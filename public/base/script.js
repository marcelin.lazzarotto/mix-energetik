(function()
{
    /**
     *  @function main
     *      Script entry point.
     */
    let main = function ()
    {
        /* First */
        MixEnergetik.LoaderManager && MixEnergetik.LoaderManager.initialize();

        MixEnergetik.MasterManager.initializeSilence();

        /* Initialize things */
        MixEnergetik.SoloManager.initialize();
        MixEnergetik.UI.ApplicationControlView.initialize();
        MixEnergetik.UI.SequencerView.initialize();
        MixEnergetik.UI.ChargeView.initialize();
        MixEnergetik.UI.EnergeticSelectionView.initialize();
        MixEnergetik.UI.VideoView.initialize();
        MixEnergetik.UI.VideoView.hide();
        MixEnergetik.UI.MapView.initialize();
        MixEnergetik.DragAndDropManager.initialize();
        MixEnergetik.UI.TradeView.initialize();
        MixEnergetik.UI.GroupDataView && MixEnergetik.UI.GroupDataView.initialize();
        MixEnergetik.GroupDataManager && MixEnergetik.GroupDataManager.initialize();
        MixEnergetik.TradeManager.initialize();
        MixEnergetik.VideoManager.initialize();
        MixEnergetik.DateManager && MixEnergetik.DateManager.initialize();
        MixEnergetik.UI.DatePickerView && MixEnergetik.UI.DatePickerView.initialize();
        MixEnergetik.ApplicationControlManager.initialize();
        MixEnergetik.GroupManager.initialize();
        MixEnergetik.SampleManager.initialize();
        MixEnergetik.SequencerManager.initialize();

        MixEnergetik.UI.CreditsView.initialize();
        MixEnergetik.UI.Preview.initialize();

        /* Initialize regions with element samples */
        MixEnergetik.RegionManager.initialize();
    };

    {
        function __loadMain()
        {
            window.removeEventListener("load", __loadMain, false);
            main();
        }
        window.addEventListener("load", __loadMain, false);
    }
})();

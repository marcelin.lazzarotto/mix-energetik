<?php
    require_once("../../server/authentication.php");

    $login = 'cezarmonik';
    $password = 'cpbase';

    authentication_tmpGetLogins('../../resources/security/base.txt', $login,
        $password);
    $res = authentication_test($login, $password);

    if ($res != ME_AUTHENTICATION_SUCCESS)
    {
        echo 'Access denied';
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="Author" content="Marcelin Lazzarotto, Fabrice Lazzarotto,
            Christian Perret">
        <meta name="Category"
            content="Prototype, Sound Design">
        <meta name="Copyright" content="Marcelin Lazzarotto">
        <meta name="Description" content="Et si on faisait de l'énergie avec
            des éléments et de la musique ?">
        <meta name="Keywords"
            content="Prototype, energiziks, Sound Design">
        <meta name="Publisher" content="Marcelin Lazzarotto">
        <meta name="Robots" content="noindex, nofollow">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="fr">

        <link rel="stylesheet"
            href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/common/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/AudioItem/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/ApplicationControlView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/LoadLayer/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/DatePicker/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/EnergeticContainer/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/MapView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Trade/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/VideoView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/SequencerPanel/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/ChargePanel/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/CreditsView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Preview/style.css">
        <link rel="stylesheet"
            href="/energiziks/public/base/style.css">

        <title>energiziks - Conso/Prod</title>
    </head>
    <body>
        <main id="Application">
            <video id="BackgroundVideo" class="noninteractable"
                src="/energiziks/resources/default/Background.mp4"
                alt="Background" preload="auto" loop autoPlay></video>
            <img id="DefaultBackground" class="background image noninteractable"
                src="/energiziks/public/base/DefaultBackground.png"
                alt="DefaultBackground" />
            <img id="ExtendedBackground" class="background image noninteractable"
                src="/energiziks/public/base/ExtendedBackground.png"
                alt="ExtendedBackground" />
            <?php
                require("../../resources/modules/LoadLayer/template.html");
                require("../../resources/modules/ApplicationControlView/template.html");
                require("../../resources/modules/DatePicker/template.html");
                require("../../resources/modules/EnergeticContainer/template.html");
                require("../../resources/modules/MapView/template.html");
                require("../../resources/modules/Trade/template.html");
                require("../../resources/modules/VideoView/template.html");
                require("../../resources/modules/SequencerPanel/template.html");
                require("../../resources/modules/ChargePanel/template.html");
                require("../../resources/modules/CreditsView/template.html");
                require("../../resources/modules/Preview/template.html");
            ?>
        </main>

        <!-- Scripts -->
        <script type="text/javascript"
            src="https://code.jquery.com/jquery-1.12.4.js">
        </script>
        <script type="text/javascript"
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/libraries/Pizzicato/Pizzicato.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Check.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Configuration.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Event.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/SoundBuilder.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/WavAudioEncoder.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/KeyControl.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/UI.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/ApplicationControlView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/AudioItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/SampleItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/GroupItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/DatePicker/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/MapView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/BinItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/BatteryItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/SequencerPanel/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/EnergeticContainer/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/Trade/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/VideoView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/ChargePanel/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectHandler.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/SoundData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/SoundWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/GroupData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/GroupWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/AudioManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Data/RegionData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Data/ImportData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/ApplicationControlManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/DragAndDropManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SoloManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/GroupManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SampleManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SequencerManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/RegionManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/DateManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/TradeManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/VideoManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/LoaderManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/MasterManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/CreditsView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/Preview/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/base/script.js">
        </script>
    </body>
</html>

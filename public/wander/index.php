<?php
    require_once("../../server/authentication.php");

    $login = '';
    $password = '';

    authentication_tmpGetLogins('../../resources/security/wander.txt', $login,
        $password);
    $res = authentication_test($login, $password);

    if ($res != ME_AUTHENTICATION_SUCCESS)
    {
        echo 'Access denied';
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="Author" content="Marcelin Lazzarotto, Fabrice Lazzarotto,
            Christian Perret">
        <meta name="Category"
            content="Prototype, Sound Design">
        <meta name="Copyright" content="Marcelin Lazzarotto">
        <meta name="Description" content="Et si on partait dans l'infiniment
            grand, ou petit ?">
        <meta name="Keywords"
            content="Prototype, energiziks, Sound Design">
        <meta name="Publisher" content="Marcelin Lazzarotto">
        <meta name="Robots" content="noindex, nofollow">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="fr">

        <meta charset="utf-8">

        <link rel="stylesheet"
            href="/energiziks/resources/modules/common/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/AudioItem/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/LoadLayer/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/ApplicationControlView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/VideoSelection/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/TagSelection/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/VideoView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/SampleListView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/SequencerPanel/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/ChargePanel/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/PresetPanelView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/PersistenceView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/CreditsView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Preview/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Exploration/style.css">
        <link rel="stylesheet" href="/energiziks/public/wander/style.css">

        <title>energiziks - Errance</title>
    </head>
    <body>
        <main id="Application">
            <!-- <video id="BackgroundVideo" class="noninteractable"
                src="/energiziks/resources/default/Background.mp4"
                alt="Background" preload="auto" loop autoPlay></video> -->
            <img id="BackgroundVideo" class="noninteractable"
                src="/energiziks/public/wander/Background.jpg"
                alt="Background" />
            <img id="DefaultBackground" class="noninteractable background image"
                src="/energiziks/public/wander/DefaultBackground.png"
                alt="Background" />
            <img id="ExtendedBackground" class="background image noninteractable"
                src="/energiziks/public/wander/ExtendedBackground.png"
                alt="Background" />
            <?php
                require("../../resources/modules/LoadLayer/template.html");
                require("../../resources/modules/ApplicationControlView/template.html");
                require("../../resources/modules/VideoSelection/template.html");
                require("../../resources/modules/TagSelection/template.html");
                require("../../resources/modules/VideoView/template.html");
                require("../../resources/modules/SampleListView/template.html");
                require("../../resources/modules/SequencerPanel/template.html");
                require("../../resources/modules/ChargePanel/template.html");
                require("../../resources/modules/PresetPanelView/template.html");
                require("../../resources/modules/PersistenceView/template.html");
                require("../../resources/modules/CreditsView/template.html");
                require("../../resources/modules/Preview/template.html");
                require("../../resources/modules/Exploration/template.html");
            ?>
        </main>

        <script type="text/javascript"
            src="/energiziks/script/libraries/Pizzicato/Pizzicato.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Check.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Configuration.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Event.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/SoundBuilder.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/WavAudioEncoder.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/KeyControl.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Helpers.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectHandler.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/SoundData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/SoundWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/GroupData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/GroupWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/AudioManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/UI.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/ApplicationControlView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/AudioItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/SampleItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/GroupItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/SequencerPanel/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/VideoItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/VideoSelection/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/TagItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/TagSelection/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/VideoView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/BatteryItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/BinItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/ChargePanel/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/SampleListView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/PresetPanelView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/PersistenceView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/ApplicationControlManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/DragAndDropManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SoloManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/GroupManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SampleManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SequencerManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/VideoManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/LoaderManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/PersistenceManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/MasterManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/CreditsView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/Preview/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/Exploration/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/wander/script.js">
        </script>
    </body>
</html>

/*
 *  File:   wander.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-10
 */

"use strict";

(function()
{
    MixEnergetik.UI = MixEnergetik.UI || {};

    /**
     *  @namespace Wander
     *      Special namespace for the page.
     */
    MixEnergetik.UI.Wander = (function()
    {
        let Wander = {};

        return Wander;
    })();

    /*
     *  Initialize
     */
    let main = function main()
    {
        MixEnergetik.LoaderManager.initialize();

        /* Initialize silence */
        MixEnergetik.MasterManager.initializeSilence();

        MixEnergetik.SoloManager.initialize();
        MixEnergetik.UI.ApplicationControlView.initialize();
        MixEnergetik.ApplicationControlManager.initialize();
        MixEnergetik.UI.SequencerView.initialize();
        MixEnergetik.UI.VideoView.initialize();
        MixEnergetik.UI.TagSelectionView.initialize();
        MixEnergetik.UI.VideoSelectionView.initialize();
        MixEnergetik.UI.SampleListView.initialize();
        MixEnergetik.DragAndDropManager.initialize();
        MixEnergetik.VideoManager.initialize();
        MixEnergetik.UI.ChargeView.initialize();
        MixEnergetik.UI.PresetPanelView.initialize();
        MixEnergetik.UI.PersistenceView.initialize();
        MixEnergetik.PersistenceManager.initialize();
        MixEnergetik.GroupManager.initialize();
        MixEnergetik.SampleManager.initialize();
        MixEnergetik.SequencerManager.initialize();

        MixEnergetik.UI.CreditsView.initialize();
        MixEnergetik.UI.Preview.initialize();
        MixEnergetik.UI.Exploration.initialize();

        document.getElementById("ExtendedBackground").classList.add("hidden");
    };

    /* Loader */
    let __load = function __load()
    {
        window.removeEventListener("load", __load, false);
        main();
    };
    window.addEventListener("load", __load, false);
})();

/*
 *  File:   vie-mer/script/Pointer.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2019-04-25
 *
 *  Brief:  For vie-mer page, describes a pointer on the map.
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.VieMer
 *      Subnamespace for VieMer related elements.
 */
MixEnergetik.VieMer = MixEnergetik.VieMer || {};

/**
 *  @constructor Pointer
 *      Construct a pointer object, containing the element on screen
 *      as well as samples and video url.
 *
 *  @param {string} zone
 *      The zone where the "location" pointed by the target belongs.
 *  @param {number} depth
 *      The depth of the "location" poited by the pointer.
 *  @param {number} identifier
 *      A number used to identify the pointer.
 */
MixEnergetik.VieMer.Pointer = (function()
{
    /**
     *  @property {MixEnergetik.VieMer.Pointer} currentSelected
     *      Keeps track of the active pointer.
     *      Can be null if none are.
     */
    let Pointer_currentSelected = null;

    let Pointer = function Pointer(submap, zone, depth, identifier)
    {
        MixEnergetik.Utilitary.Check.native(zone, "string",
            "MixEnergetik.VieMer.Pointer()", "zone");
        MixEnergetik.Utilitary.Check.native(submap, "string",
            "MixEnergetik.VieMer.Pointer()", "submap");
        MixEnergetik.Utilitary.Check.native(depth, "number",
            "MixEnergetik.VieMer.Pointer()", "depth");
        MixEnergetik.Utilitary.Check.native(identifier, "number",
            "MixEnergetik.VieMer.Pointer()", "identifier");

        /*
         * Declarations
         */
        /**
         *  @property {string} submap
         *      The submap to which the pointer belongs.
         */
        this.submap = "";
        /**
         *  @property {string} zone
         *      The zone where the "location" pointed by the target belongs.
         */
        this.zone = "";
        /**
         *  @property {number} depth
         *      The depth of the "location" poited by the pointer.
         */
        this.depth = 0;
        /**
         *  @property {number} identifier
         *      A number used to identify the pointer.
         */
        this.identifier = 0;

        /**
         *  @property {MixEnergetik.UI.SampleItem[]} samplePaths
         *      Paths of the sample items.
         */
        this.samplePaths = [];

        /**
         *  @property {MixEnergetik.UI.SampleItem[]} sampleItems
         *      Sample items linked to the pointer.
         */
        this.sampleItems = [];

        /**
         *  @property {string} videoUrl
         *      The url of the video linked to the pointer.
         */
        this.videoUrl = "";

        /**
         *  @property {string} videoName
         *      The name of the video linked to the pointer.
         */
        this.videoName = "";

        /**
         *  @property {HTMLDivElement} container
         *      The pointer DOM element.
         */
        this.container = null;

        /**
         *  @property {MixEnergetik.Utilitary.Event} event
         *      An event handler for our pointer. Possible events are:
         *      - "samplefetch", when samples are currently being downloaded
         *      - "sampleload", when samples have been loaded.
         */
        this.event = new MixEnergetik.Utilitary.Event();

        /* Helpers */
        let self = this;

        const DEPTH_KEY = MixEnergetik.VieMer.DEPTH_PREFIX + depth;

        let clickCallback = function clickCallback(event)
        {
            const VideoV = MixEnergetik.UI.VideoView;
            const SmpLstV = MixEnergetik.UI.SampleListView;

            let oldSelected = Pointer_currentSelected;

            if (Pointer_currentSelected !== null)
            {
                Pointer_currentSelected.unselect();
            }

            if (oldSelected === self)
            {
                return;
            }

            if (VideoV.getVideoURL() !== self.videoUrl)
            {
                VideoV.setVideoURL(self.videoUrl, self.videoName);
            }
            VideoV.show();
            VideoV.videoElement.currentTime = 0;
            VideoV.videoElement.play();

            SmpLstV.clear();

            if (!self.isLoaded())
            {
                /* Delayed download */
                self.loadSamples();
            }

            SmpLstV.setSampleItems(self.sampleItems);
            SmpLstV.show();

            let informationPath = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/images/vie-mer/"
                + MixEnergetik.VieMer.SUBMAPS_DIRNAME + "/" + self.submap
                + "/informations/" + self.zone;
            if (self.zone !== "coast")
            {
                informationPath += "/" + self.depth
            }
            informationPath += "/" + self.identifier + ".jpg";

            MixEnergetik.VieMer.showInformation(informationPath);

            Pointer_currentSelected = self;

            Pointer_currentSelected.container.classList.add("selected");
        };

        /*
         *  Construction
         */

        this.submap = submap;
        this.zone = zone;
        this.depth = depth;
        this.identifier = identifier;

        this.videoUrl = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/resources/videos/vie-mer/" + MixEnergetik.VieMer.SUBMAPS_DIRNAME
            + "/" + self.submap + "/";
        if (this.zone !== "coast")
        {
            this.videoUrl += self.zone + "/" + self.depth;
            this.videoName = self.zone + "_" + self.depth + "_"
                + self.identifier;
        }
        else
        {
            this.videoUrl += "cote";
            this.videoName = "Côte " + self.identifier;
        }
        this.videoUrl += "/" + self.identifier + ".mp4";

        this.container = document.createElement("div");

        this.container.classList.add("pointer");
        this.container.classList.add(DEPTH_KEY);
        this.container.id = this.zone + "_" + this.depth + "_"
            + this.identifier;

        {
            let label = document.createElement("div");

            if (this.zone !== "coast")
            {
                label.innerHTML = "(" + this.zone + "," + this.depth + ","
                    + this.identifier + ")";
            }
            else
            {
                label.innerHTML = this.identifier;
            }

            label.classList.add("label");
            label.classList.add(DEPTH_KEY);

            this.container.appendChild(label);
        }

        this.container.addEventListener("click", clickCallback, false);
    };

    Pointer.prototype.isLoaded = function isLoaded()
    {
        return (this.sampleItems.length >= this.samplePaths.length);
    };

    Pointer.prototype.loadSamples = function loadSamples()
    {
        /* Actually request 4 samples, and don't care if several are
         * missing, since we can have between 1 and 4. */

        const SndBldr = MixEnergetik.Utilitary.SoundBuilder;
        const AudioM = MixEnergetik.AudioManager;

        this.sampleItems = [];

        for (let i = 0; i < this.samplePaths.length; i++)
        {
            let samplePath = this.samplePaths[i];
            let sampleName = samplePath.substring(samplePath.lastIndexOf("/") + 1);

            let soundWrapper = new SndBldr.createSoundWrapper(samplePath);
            let soundIndex = AudioM.provideSound(soundWrapper);
            let sampleItem = new MixEnergetik.UI.SampleItem(soundIndex,
                sampleName);

            this.sampleItems.push(sampleItem);
        }
    };

    Pointer.prototype.unselect = function unselect()
    {
        if (this !== Pointer_currentSelected)
        {
            return;
        }

        MixEnergetik.VieMer.hideInformation();

        MixEnergetik.UI.VideoView.videoElement.pause();
        MixEnergetik.UI.VideoView.hide();
        MixEnergetik.UI.SampleListView.hide();

        Pointer_currentSelected = null;

        this.container.classList.remove("selected");
    };

    return Pointer;
})();

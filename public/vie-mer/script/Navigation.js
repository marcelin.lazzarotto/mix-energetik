/*
 *  File:   vie-mer/script/Filter.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2019-04-28
 *
 *  Brief:  File dealing with navigation inside energiziks/vie-mer/, due to the
 *      the submaps.
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.VieMer
 *      Subnamespace for VieMer related elements.
 */
MixEnergetik.VieMer = MixEnergetik.VieMer || {};

/**
 *  @namespace Navigation
 *      Namespace to deal with "page" navigation inside
 *      energiziks/vie-mer/.
 */
MixEnergetik.VieMer.Navigation = (function()
{
    let Navigation = {};

    /**
     *  @property {string} currentMap
     *      Indicates the current selected map.
     */
    Navigation.currentMap = "";

    Navigation.initialize = function initialize()
    {
        Navigation_container = document.getElementById(
            "VieMerNavigation");
        Navigation_map = document.getElementById(
            "VieMerNavigationMap");
        Navigation_button1 = document.getElementById(
            "VieMerNavigationButtonSubMap1");
        Navigation_button2 = document.getElementById(
            "VieMerNavigationButtonSubMap2");
        Navigation_button3 = document.getElementById(
            "VieMerNavigationButtonSubMap3");
        Navigation_buttonBack = document.getElementById(
            "VieMerNavigationButtonBack");

        Navigation_container.classList.add("introductionView");

        Navigation_button1.addEventListener("click", Navigation_button1OnClick,
            false);
        Navigation_button2.addEventListener("click", Navigation_button2OnClick,
            false);
        Navigation_button3.addEventListener("click", Navigation_button3OnClick,
            false);
        Navigation_buttonBack.addEventListener("click",
            Navigation_buttonBackOnClick, false);
    };

    let Navigation_container = null;
    let Navigation_map = null;
    let Navigation_button1 = null;
    let Navigation_button2 = null;
    let Navigation_button3 = null;
    let Navigation_buttonBack = null;

    let Navigation_toSubmapView = function toSubmapView(submap)
    {
        Navigation_container.classList.remove("introductionView");
        Navigation_container.classList.add("submapView");

        MixEnergetik.VieMer.resetView();

        Navigation.currentMap = submap;

        MixEnergetik.VieMer.loadView();
    };

    let Navigation_button1OnClick = function button1OnClick(event)
    {
        Navigation_toSubmapView("1");
    };

    let Navigation_button2OnClick = function button2OnClick(event)
    {
        Navigation_toSubmapView("2");
    };

    let Navigation_button3OnClick = function button3OnClick(event)
    {
        Navigation_toSubmapView("3");
    };

    let Navigation_buttonBackOnClick = function buttonBackOnClick(event)
    {
        Navigation_container.classList.remove("submapView");
        Navigation_container.classList.add("introductionView");
    };

    return Navigation;
})();

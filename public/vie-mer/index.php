<?php
    require_once("../../server/authentication.php");

    $login = '';
    $password = '';

    // $res = authentication_test($login, $password);
    //
    // if ($res != ME_AUTHENTICATION_SUCCESS)
    // {
    //     echo 'Access denied';
    //     exit;
    // }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="Author" content="Marcelin Lazzarotto, Fabrice Lazzarotto,
            Christian Perret">
        <meta name="Category"
            content="Prototype, Sound Design">
        <meta name="Copyright" content="Marcelin Lazzarotto">
        <meta name="Description" content="Et si on faisait de l'énergie avec
            des éléments et de la musique ?">
        <meta name="Keywords"
            content="Prototype, energiziks, Sound Design">
        <meta name="Publisher" content="Marcelin Lazzarotto">
        <meta name="Robots" content="noindex, nofollow">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="fr">

        <link rel="stylesheet"
            href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/common/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/AudioItem/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/LoadLayer/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/VideoView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/SequencerPanel/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/ChargePanel/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/SampleListView/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/PresetPanelView/style.css">
        <!-- <link rel="stylesheet"
            href="/energiziks/resources/modules/CreditsView/style.css"> -->
        <link rel="stylesheet"
            href="/energiziks/public/vie-mer/style.css">

        <title>energiziks - La vie de ma mer</title>
    </head>
    <body>
        <main id="Application">
            <!-- <video id="BackgroundVideo" class="noninteractable"
                src="/energiziks/resources/default/Background.mp4"
                alt="Background" preload="auto" loop autoPlay></video> -->
            <img id="BackgroundVideo" class="noninteractable"
                src="/energiziks/public/vie-mer/Background.png"
                alt="Background" />
            <img id="DefaultBackground" class="background image noninteractable"
                src="/energiziks/public/vie-mer/BackgroundDefault.png"
                alt="DefaultBackground" />
            <img id="ExtendedBackground" class="background image noninteractable"
                src="/energiziks/public/vie-mer/BackgroundExtended.png"
                alt="ExtendedBackground" />
            <?php
                require("../../resources/modules/LoadLayer/template.html");
                require("../../resources/modules/VideoView/template.html");
                require("../../resources/modules/SequencerPanel/template.html");
                require("../../resources/modules/ChargePanel/template.html");
                require("../../resources/modules/SampleListView/template.html");
                require("../../resources/modules/PresetPanelView/template.html");
                // require("../../resources/modules/CreditsView/template.html");
            ?>
            <!-- Specifics -->
            <nav id="VieMerNavigation">
                <img id="VieMerNavigationMap"
                    class="nonselectable noninteractable"
                    src="/energiziks/public/vie-mer/MapIntroduction.png"
                    alt="IntroductionMap" />
                <div id="VieMerNavigationButtonSubMap1"
                    class="button introduction">1</div>
                <div id="VieMerNavigationButtonSubMap2"
                    class="button introduction">2</div>
                <div id="VieMerNavigationButtonSubMap3"
                    class="button introduction">3</div>
                <div id="VieMerNavigationButtonBack"
                    class="button submap">
                    <img id="VieMerNavigationButtonBackImage"
                        src="/energiziks/public/vie-mer/IconBack.png"
                        alt="Retour" />
                </div>
            </nav>
            <section id="VieMerSubScreen">
                <div id="VieMerLoadButton" class="button">
                    <img id="VieMerLoadButtonImage"
                        src="/energiziks/public/vie-mer/IconDownload.png"
                        alt="Download them all!">
                    </img>
                    <div id="VieMerLoadButtonDescription">
                        Téléchargez toutes les ressources... mais prenez garde
                        au raz-de-marée&nbsp;!
                    </div>
                </div>
                <div id="VieMerFilterContainer">
                </div>
                <div id="VieMerMapContainer">
                    <div id="VieMerMapContent">
                        <img id="VieMerMapImage"
                            class="noninteractable nonselectable"
                            src="/energiziks/public/vie-mer/Map1.png" alt="VieMerMap">
                        <div id="VieMerCoastContainer"></div>
                    </div>
                </div>
                <img id="VieMerSubScreenInformationImage"
                    alt="Informations" />
                <div id="VieMerSubScreenInformationButton"
                    class="button information">
                    <img id="VieMerSubScreenInformationButtonIcon"
                        src="/energiziks/public/vie-mer/IconInfo.png"
                        alt="i" />
                </div>
                <div id="VieMerDroneButton" class="button">
                    <audio id="VieMerDroneAudio" loop></audio>
                </div>
                <div id="VieMerSequencerIconSpeaker" class="nonselectable noninteractable"></div>
                <div id="VieMerButtonToggleExtend" class="button"></div>
            </section>
        </main>

        <!-- Scripts -->
        <script type="text/javascript"
            src="/energiziks/script/libraries/Pizzicato/Pizzicato.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Check.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Configuration.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Event.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/SoundBuilder.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/WavAudioEncoder.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/KeyControl.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Utilitary/Helpers.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/UI.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/AudioItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/SampleItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/GroupItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/BinItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/UI/BatteryItem.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/EffectHandler.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/SoundData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/SoundWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/GroupData.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/GroupWrapper.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Audio/AudioManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/SequencerPanel/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/VideoView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/ChargePanel/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/SampleListView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/PresetPanelView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/ApplicationControlManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/DragAndDropManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SoloManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/GroupManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SampleManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/SequencerManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/VideoManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/LoaderManager.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/script/source/Manager/MasterManager.js">
        </script>
        <!-- <script type="text/javascript"
            src="/energiziks/resources/modules/CreditsView/script.js">
        </script> -->
        <script type="text/javascript"
            src="/energiziks/public/vie-mer/script/Navigation.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/vie-mer/script/Pointer.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/vie-mer/script/Filter.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/vie-mer/script.js">
        </script>
    </body>
</html>

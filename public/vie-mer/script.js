/*
 *  File:   vie-mer/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2019-04-04
 *
 *  Brief:  Main js file for vie-mer page.
 */

"use strict";

(function()
{
    /**
     *  @namespace VieMer
     *      Specific namespace for the page.
     */
    MixEnergetik.VieMer = MixEnergetik.VieMer || {};

    (function()
    {
        let VieMer = MixEnergetik.VieMer;

        /**
         *  @property {string const} DEPTH_PREFIX
         *      String prefix for all depth related identification (mainly used
         *      for CSS classnames).
         */
        VieMer.DEPTH_PREFIX = "depth";

        /**
         *  @property {string const} SUBMAPS_DIRNAME
         *      The directory name for the submaps directory on the server.
         */
        VieMer.SUBMAPS_DIRNAME = "submaps";

        /**
         *  @property {MixEnergetik.VieMer.Pointer[][][] objectmap} pointers
         *      Contains all the pointers info for the map.
         *
         *      Hierarchy:
         *          pointers[map] = {};
         *          pointers[map][zone] = [];
         *          pointers[map][zone][depth] = [];
         *          pointers[map][zone][depth][identifier] = pointer;
         */
        VieMer.pointers = {};

        /**
         *  @function initialize
         *      Initialize the page.
         */
        VieMer.initialize = function initialize()
        {
            MixEnergetik.VieMer.Navigation.initialize();

            VieMer_backgroundDefault = document.getElementById("DefaultBackground");
            VieMer_backgroundExtended = document.getElementById("ExtendedBackground");
            VieMer_container = document.getElementById("VieMerSubScreen");
            VieMer_loadButton = document.getElementById("VieMerLoadButton");
            VieMer_loadButtonDescription = document.getElementById(
                "VieMerLoadButtonDescription");
            VieMer_filterContainer = document.getElementById("VieMerFilterContainer");
            VieMer_filterContainer = document.getElementById("VieMerFilterContainer");
            VieMer_mapContainer = document.getElementById("VieMerMapContainer");
            VieMer_mapContent = document.getElementById("VieMerMapContent");
            VieMer_mapImage = document.getElementById("VieMerMapImage");
            VieMer_coastContainer = document.getElementById("VieMerCoastContainer");
            VieMer_informationImage = document.getElementById(
                "VieMerSubScreenInformationImage");
            VieMer_informationButton = document.getElementById(
                "VieMerSubScreenInformationButton");
            VieMer_droneContainer = document.getElementById("VieMerDroneButton");
            VieMer_droneAudioContainer = document.getElementById("VieMerDroneAudio");
            VieMer_buttonToggleExtend = document.getElementById("VieMerButtonToggleExtend");

            VieMer_retract();

            VieMer_loadButton.addEventListener("click",
                VieMer_onLoadButtonClick, false);
            VieMer_droneContainer.addEventListener("click", VieMer_onDroneClick,
                false);
            VieMer_buttonToggleExtend.addEventListener("click",
                VieMer_onButtonToggleExtendClick, false);

            VieMer_informationImage.addEventListener("error",
                VieMer_onInformationImageError, false);
            VieMer_informationButton.addEventListener("click",
                VieMer_onInformationButtonClick, false);

            MixEnergetik.VieMer.hideInformation();

            let testChargeSampleToSea = function testChargeSampleToSea()
            {
                let dragged = MixEnergetik.DragAndDropManager.draggedItem;
                let target = MixEnergetik.DragAndDropManager.targetItem;

                return ((dragged instanceof MixEnergetik.UI.SampleItem)
                    && (target === VieMer_mapContent)
                    && (dragged.parent)
                    && (dragged.parent instanceof MixEnergetik.UI.BatteryItem));
            };

            /* Add possibility of removing charge sample to sea */
            /* Drag over is fired on drop zone element. */
            VieMer_mapContent.addEventListener("dragover", function(event)
            {
                MixEnergetik.DragAndDropManager.targetItem = VieMer_mapContent;

                if (testChargeSampleToSea())
                {
                    /* To allow drop */
                    event.preventDefault();

                    event.dataTransfer.dropEffect = "link";
                }
            }, false);

            /* Drop is fired on drop zone element. */
            VieMer_mapContent.addEventListener("drop", function(event)
            {
                if (testChargeSampleToSea())
                {
                    let dragged = MixEnergetik.DragAndDropManager.draggedItem;

                    /* To allow drop */
                    event.preventDefault();

                    dragged.control.retract();
                    dragged.stop();
                    dragged.parent.clear();
                    dragged.parent = null;
                }
            }, false);

            /*
             * Page-specific modifications
             */
            /* Seconds */
            MixEnergetik.Utilitary.Configuration.Audio.sampleDuration = 6.874;

            /* Keys */
            MixEnergetik.UI.SampleListView.keyControl.setKeys(
                VieMer_SAMPLE_KEYS);
            MixEnergetik.UI.ChargeView.keyControl.setKeys(VieMer_CHARGE_KEYS);
            MixEnergetik.UI.PresetPanelView.keyControl.setKeys(
                VieMer_PRESET_KEYS);

            /* Sequencer */
            MixEnergetik.UI.SequencerView.specializeForVieMer();

            /* Init pointer structure */

            const MAP_NAMES = ["1", "2", "3"];
            const DEPTH_COUNT = 3;
            const COAST_COUNT = 8;
            const POINTERS_STRUCTURE =
            [
                {
                    zone: "A",
                    pointersCount: [11, 0, 0]
                },
                {
                    zone: "B",
                    pointersCount: [7, 5, 0]
                },
                {
                    zone: "C",
                    pointersCount: [6, 5, 4]
                }
            ];

            for (let i = 0; i < MAP_NAMES.length; i++)
            {
                let map = MAP_NAMES[i];

                MixEnergetik.VieMer.pointers[map] = {};

                for (let j = 0; j < POINTERS_STRUCTURE.length; j++)
                {
                    let zone = POINTERS_STRUCTURE[j].zone;
                    let pointersCount = POINTERS_STRUCTURE[j].pointersCount;

                    MixEnergetik.VieMer.pointers[map][zone] = [];

                    for (let depth = 0; depth < pointersCount.length; depth++)
                    {
                        MixEnergetik.VieMer.pointers[map][zone][depth] = [];

                        for (let identifier = 0; identifier < pointersCount[depth];
                            identifier++)
                        {
                            let pointer = new MixEnergetik.VieMer.Pointer(map,
                                zone, depth, identifier);

                            MixEnergetik.VieMer.pointers[map][zone][depth].push(pointer);
                        }
                    }
                }
            }

            /* Also add filters for depth */
            for (let depth = 0; depth < DEPTH_COUNT; depth++)
            {
                let filter = new MixEnergetik.VieMer.Filter(depth);

                VieMer_filterContainer.appendChild(filter.container);
                VieMer_filters.push(filter);
            }

            /* Also prepare drones */
            for (let i = 0; i < MAP_NAMES.length; i++)
            {
                VieMer_dronePaths[MAP_NAMES[i]] = "";
            }

            /* Also add coast pointers */
            for (let i = 0; i < MAP_NAMES.length; i++)
            {
                let map = MAP_NAMES[i];

                VieMer_coastPointers[map] = [];

                for (let j = 0; j < COAST_COUNT; j++)
                {
                    let coastPointer = new MixEnergetik.VieMer.Pointer(map,
                        "coast", -1, j);

                    VieMer_coastPointers[map].push(coastPointer);
                }
            }

            /* Map */
            MixEnergetik.Utilitary.Helpers.makeElementSlideable(
                VieMer_mapContent,
                VieMer_mapContainer);

            /* Retrieve samples name */
            (function()
            {
                let successHandler = function successHandler(fileObjectTree)
                {
                    /* Helpers, to not have infinite indent */
                    let processFO = function processFO(fileObject,
                        expectedFOType, expectedFOCategory, path)
                    {
                        if (fileObject.type !== expectedFOType)
                        {
                            console.warn("[MixEnergetik.VieMer.initialize()]"
                                + " Found a " + fileObject.type
                                + " instead of a " + expectedFOCategory
                                + " " + expectedFOType + ".");

                            return "";
                        }

                        let subPath = path + "/" + fileObject.name;

                        return subPath;
                    }; /* processFO() */

                    let processDrone = function processDrone(fileObject, path, submap)
                    {
                        /* Only one drone for now */
                        if (VieMer_dronePaths[submap] !== "")
                        {
                            return;
                        }

                        let dronePath = processFO(fileObject, "file", "drone",
                            path);

                        if (dronePath === "")
                        {
                            return;
                        }

                        VieMer_dronePaths[submap] = dronePath;
                    }; /* processDrone() */

                    let processSample = function processSample(fileObject,
                        path, pointer)
                    {
                        let filePath = processFO(fileObject, "file", "coast",
                            path);

                        if (filePath === "")
                        {
                            return;
                        }

                        pointer.samplePaths.push(filePath);
                    }; /* processSample() */

                    let processCoast = function processCoast(fileObject, path,
                        submap)
                    {
                        let coastPath = processFO(fileObject, "directory",
                            "coast", path);

                        if (coastPath === "")
                        {
                            return;
                        }

                        let coastPointer = VieMer_coastPointers[submap][+fileObject.name];

                        if (!coastPointer)
                        {
                            console.warn("[MixEnergetik.VieMer.initialize()]"
                                + " Found a directory that doesn't"
                                + " match any coast pointer.");

                            return;
                        }

                        for (let i = 0; i < fileObject.content.length; i++)
                        {
                            processSample(fileObject.content[i], coastPath,
                                coastPointer);
                        }
                    }; /* processCoast() */

                    let processPointer = function processPointer(fileObject,
                        path, submap, zone, depth)
                    {
                        let pointerPath = processFO(fileObject, "directory",
                            "pointer", path);

                        if (pointerPath === "")
                        {
                            return;
                        }

                        let identifier = +fileObject.name;
                        let pointer = MixEnergetik.VieMer.pointers[submap][
                            zone][depth][identifier];

                        if (!pointer)
                        {
                            console.warn("[MixEnergetik.VieMer.initialize()]"
                                + " Directory does not correspond to a"
                                + " specified pointer.");

                            return;
                        }

                        for (let i = 0; i < fileObject.content.length; i++)
                        {
                            processSample(fileObject.content[i], pointerPath,
                                pointer);
                        }
                    }; /* processPointer() */

                    let processDepth = function processZone(fileObject, path,
                        submap, zone)
                    {
                        let depthPath = processFO(fileObject, "directory",
                            "depth", path);
                        let depth = +fileObject.name;
                        if (depthPath === "")
                        {
                            return;
                        }

                        if (!MixEnergetik.VieMer.pointers[submap][zone].hasOwnProperty(depth))
                        {
                            console.warn("[MixEnergetik.VieMer.initialize()]"
                                + " Directory does not correspond to a"
                                + " specified depth.");

                            return;
                        }

                        for (let i = 0; i < fileObject.content.length; i++)
                        {
                            processPointer(fileObject.content[i], depthPath,
                                submap, zone, depth);
                        }
                    }; /* processDepth() */

                    let processZone = function processZone(fileObject, path,
                        submap)
                    {
                        let zonePath = processFO(fileObject, "directory",
                            "zone", path);
                        let zone = fileObject.name;

                        if (zonePath === "")
                        {
                            return;
                        }

                        switch(zone)
                        {
                            case "drone":
                                if (!VieMer_dronePaths.hasOwnProperty(submap))
                                {
                                    console.warn("[MixEnergetik.VieMer.initialize()]"
                                        + " Directory does not correspond to a"
                                        + " specified submap.");

                                    return;
                                }

                                for (let i = 0;
                                    i < fileObject.content.length;
                                    i++)
                                {
                                    processDrone(fileObject.content[i],
                                        zonePath, submap);
                                }

                                break;
                            case "cote":
                                if (!VieMer_coastPointers.hasOwnProperty(submap))
                                {
                                    console.warn("[MixEnergetik.VieMer.initialize()]"
                                        + " Directory does not correspond to a"
                                        + " specified submap.");

                                    return;
                                }

                                for (let i = 0;
                                    i < fileObject.content.length;
                                    i++)
                                {
                                    processCoast(fileObject.content[i],
                                        zonePath, submap);
                                }

                                break;
                            default:
                                if (!MixEnergetik.VieMer.pointers.hasOwnProperty(submap))
                                {
                                    console.warn("[MixEnergetik.VieMer.initialize()]"
                                        + " Directory does not correspond to a"
                                        + " specified submap.");

                                    return;
                                }

                                if (!MixEnergetik.VieMer.pointers[submap].hasOwnProperty(zone))
                                {
                                    console.warn("[MixEnergetik.VieMer.initialize()]"
                                        + " Directory does not correspond to a"
                                        + " specified zone, the coast or the"
                                        + " drone.");

                                    return;
                                }

                                for (let i = 0;
                                    i < fileObject.content.length;
                                    i++)
                                {
                                    processDepth(fileObject.content[i],
                                        zonePath, submap, zone);
                                }

                                break;
                        }
                    }; /* processZone() */

                    let processSubmap = function processSubmap(fileObject, path)
                    {
                        let submapPath = processFO(fileObject, "directory",
                            "submap", path);
                        let submap = fileObject.name;

                        if (submapPath === "")
                        {
                            return;
                        }

                        for (let i = 0; i < fileObject.content.length; i++)
                        {
                            processZone(fileObject.content[i],
                                submapPath, submap);
                        }
                    }; /* processSubmap() */

                    /* Execution */
                    const BASE_PATH = "../resources/samples/vie-mer";
                    const TRUE_BASE_PATH = MixEnergetik.MasterManager.PROJECT_ROOT
                        + "/resources/samples/vie-mer";

                    if ((fileObjectTree.name !== BASE_PATH)
                        || (fileObjectTree.type !== "directory"))
                    {
                        throw new Error("[MixEnergetik.VieMer.initialize()]"
                            + " Can't use MasterManager.retrievePaths()"
                            + " returned data.");
                    }

                    for (let i = 0; i < fileObjectTree.content.length; i++)
                    {
                        let fileObject = fileObjectTree.content[i];
                        let path = processFO(fileObject, "directory",
                            "base", TRUE_BASE_PATH);

                        if (path === "")
                        {
                            continue;
                        }

                        if (fileObject.name === VieMer.SUBMAPS_DIRNAME)
                        {
                            for (let j = 0; j < fileObject.content.length; j++)
                            {
                                processSubmap(fileObject.content[j], path);
                            }
                        }
                        /* Do nothing for the rest for now */
                    }
                };

                let errorHandler = function errorHandler(status, errorString)
                {
                    throw new Error("[MixEnergetik.EnergeticSelectionView.initialize()]"
                        + " Couldn't retrieve sample paths\n"
                        + errorString + " with status " + status);
                };

                MixEnergetik.MasterManager.retrievePaths("samples", "vie-mer",
                    successHandler, errorHandler);
            })(); /* retrieving paths */

            /* Specialize video */
            MixEnergetik.UI.VideoView.videoElement.loop = true;
        };

        /**
         *  @function resetView
         *      Resets the view. Basically:
         *      - all filters off
         *      - empty presets
         *      - empty sequencer
         *      - all coast pointers unselected and removed from
         *          VieMer_coastContainer
         *      - all pointers unselected and removed from VieMer_mapContent
         *      - drone off
         */
        VieMer.resetView = function resetView()
        {
            if (VieMer_state === "UNSET")
            {
                return;
            }

            const submap = MixEnergetik.VieMer.Navigation.currentMap;

            for (let i = 0; i < VieMer_filters.length; i++)
            {
                VieMer_filters[i].unselect();
            }

            MixEnergetik.UI.PresetPanelView.clear();
            MixEnergetik.UI.SequencerView.clear();

            for (let i = 0; i < VieMer_coastPointers[submap].length; i++)
            {
                let pointer = VieMer_coastPointers[submap][i];

                pointer.unselect();
                VieMer_coastContainer.removeChild(pointer.container);
            }

            let allPointers = MixEnergetik.VieMer.pointers[submap];

            for (let zone in allPointers)
            {
                for (let depth = 0; depth < allPointers[zone].length; depth++)
                {
                    for(let id = 0; id < allPointers[zone][depth].length; id++)
                    {
                        let pointer = allPointers[zone][depth][id];

                        pointer.unselect();
                        VieMer_mapContent.removeChild(pointer.container);
                    }
                }
            }

            VieMer_droneOff();

            VieMer_state = "UNSET";
        };

        /**
         *  @function loadView
         *      Loads the view based on
         *      MixEnergetik.VieMer.Navigation.currentMap. Basically:
         *      - submap
         *      - all coast pointers of the submap added to
         *          VieMer_coastContainer
         *      - all pointers of the submap added to VieMer_mapContent
         *      - loadall button presence
         */
        VieMer.loadView = function loadView()
        {
            if (VieMer_state === "LOADED")
            {
                return;
            }

            const submap = MixEnergetik.VieMer.Navigation.currentMap;

            VieMer_mapImage.src = "/energiziks/public/vie-mer/Map" + submap + ".png";

            VieMer_droneAudioContainer.src = VieMer_dronePaths[submap];

            for (let i = 0; i < VieMer_coastPointers[submap].length; i++)
            {
                let pointer = VieMer_coastPointers[submap][i];

                VieMer_coastContainer.appendChild(pointer.container);
            }

            let allPointers = MixEnergetik.VieMer.pointers[submap];

            for (let zone in allPointers)
            {
                for (let depth = 0; depth < allPointers[zone].length; depth++)
                {
                    for(let id = 0; id < allPointers[zone][depth].length; id++)
                    {
                        let pointer = allPointers[zone][depth][id];

                        VieMer_mapContent.appendChild(pointer.container);
                    }
                }
            }

            VieMer_state = "LOADED";
        };

        /**
         *  @function showInformation
         *      Show the information button.
         *      If the information images fails to load the image from the
         *      given path, the button will be hidden again thanks to the
         *      callback on the element.
         */
        VieMer.showInformation = function showInformation(path)
        {
            VieMer_informationImage.src = path;

            VieMer_informationButton.classList.remove("hidden");
        };

        /**
         *  @functoin hideInformation
         *      Hides the information button and image.
         */
        VieMer.hideInformation = function hideInformation()
        {
            VieMer_informationImage.classList.add("hidden");
            VieMer_informationButton.classList.add("hidden");
        };

        const VieMer_SAMPLE_KEYS = ["a", "z", "e", "r"];
        const VieMer_CHARGE_KEYS = ["q", "s", "d", "f", "j", "k", "l", "m"];
        const VieMer_PRESET_KEYS = ["w", "x", "c", "v", "b", "n", ",", ";"];

        let VieMer_backgroundDefault = null;
        let VieMer_backgroundExtended = null;
        let VieMer_container = null;
        let VieMer_loadButton = null;
        let VieMer_loadButtonDescription = null;
        let VieMer_filterContainer = null;
        let VieMer_mapContainer = null;
        let VieMer_mapContent = null;
        let VieMer_mapImage = null;
        let VieMer_coastContainer = null;
        let VieMer_informationImage = null;
        let VieMer_informationButton = null;
        let VieMer_droneContainer = null;
        let VieMer_droneAudioContainer = null;
        let VieMer_buttonToggleExtend = null;

        let VieMer_display = "DEFAULT";
        let VieMer_state = "UNSET";

        /**
         *  @property {string objectmap} dronePaths
         *      Contains a path for each drone in each submaps.
         */
        let VieMer_dronePaths = {};

        /**
         *  @property {MixEnergetik.VieMer.Filter[]} filters
         *      Filters for pointers.
         */
        let VieMer_filters = [];

        /**
         *  @property {MixEnergetik.VieMer.Pointer[] objectmap} coastPointers
         *      Aside from the pointers in the sea, those are pointers located
         *      on the coast. They are always visible.
         *      One array per map.
         */
        let VieMer_coastPointers = {};

        /**
         *  @property {boolean objectmap} loaded
         *      Keeps in mind if a map has been fully loaded or not.
         */
        let VieMer_loaded = {};

        /**
         *  @function retract
         *      Routine to retract the view.
         */
        let VieMer_retract = function retract()
        {
            VieMer_backgroundDefault.classList.remove("hidden");
            VieMer_backgroundExtended.classList.add("hidden");

            document.getElementById("VieMerSequencerIconSpeaker").classList.add("hidden");

            MixEnergetik.UI.SequencerView.hide();
            MixEnergetik.UI.PresetPanelView.hide();

            VieMer_display = "DEFAULT";
        };

        /**
         *  @function droneOff
         *      Helper to turn the drone off.
         */
        let VieMer_droneOff = function droneOff()
        {
            if (VieMer_droneAudioContainer.paused)
            {
                return;
            }

            VieMer_droneAudioContainer.pause();
            VieMer_droneContainer.classList.remove("on");
        };

        /**
         *  @function onDroneClick
         *      Callback for the drone button click.
         *      If available, toggle the drone playing.
         *
         *  @param {MouseEvent} event
         *      The event descriptor. Unused.
         */
        let VieMer_onDroneClick = function onDroneClick(event)
        {
            if (!VieMer_droneAudioContainer.src
                || (VieMer_dronePaths[MixEnergetik.VieMer.Navigation.currentMap] === ""))
            {
                return;
            }

            if (VieMer_droneAudioContainer.paused)
            {
                VieMer_droneAudioContainer.play();
                VieMer_droneContainer.classList.add("on");
            }
            else
            {
                VieMer_droneOff();
            }
        };

        /**
         *  @function onLoadButtonClick
         *      Callback for the load button click.
         *      Load all of the pages resources at once. Also delete the button,
         *      since it wouldn't make any sense to keep it on the page
         *      afterward.
         *
         *  @param {MouseEvent} event
         *      The event descriptor. Unused.
         */
        let VieMer_onLoadButtonClick = function VieMer_onLoadButtonClick(event)
        {
            const submap = MixEnergetik.VieMer.Navigation.currentMap;

            if (VieMer_loaded[submap])
            {
                return;
            }

            /* Coast */
            for (let i = 0; i < VieMer_coastPointers.length; i++)
            {
                let pointer = VieMer_coastPointers[submap][i];

                if (!pointer.isLoaded())
                {
                    pointer.loadSamples();
                }
            }

            /* Sea */
            for (let zone in MixEnergetik.VieMer.pointers[submap])
            {
                for (let depth = 0;
                    depth < MixEnergetik.VieMer.pointers[submap][zone].length;
                    depth++)
                {
                    for (let identifier = 0;
                        identifier < MixEnergetik.VieMer.pointers[submap][zone][depth].length;
                        identifier++)
                    {
                        let pointer = MixEnergetik.VieMer.pointers[submap][
                            zone][depth][identifier];

                        if (!pointer.isLoaded())
                        {
                            pointer.loadSamples();
                        }
                    }
                }
            }

            VieMer_loaded[submap] = true;

            /* Remove button */
            VieMer_loadButton.parentNode.removeChild(VieMer_loadButton);
        }; /* onLoadButtonClick */

        /**
         *  @function onButtonToggleExtendClick
         *      Callback when the toggle extend button has been clicked on.
         *      Hide or show other views accordingly.
         *
         *  @param {MouseEvent} event
         *      The event descriptor. Unused.
         */
        let VieMer_onButtonToggleExtendClick = function onButtonToggleExtendClick(event)
        {
            if (VieMer_display === "DEFAULT")
            {
                /* Extend */
                VieMer_backgroundDefault.classList.add("hidden");
                VieMer_backgroundExtended.classList.remove("hidden");

                document.getElementById("VieMerSequencerIconSpeaker").classList.remove("hidden");

                MixEnergetik.UI.SequencerView.show();
                MixEnergetik.UI.PresetPanelView.show();

                VieMer_display = "EXTENDED";
            }
            else
            {
                VieMer_retract();
            }
        };

        let VieMer_onInformationImageError = function onInformationImageError(
            event)
        {
            MixEnergetik.VieMer.hideInformation();
        };

        let VieMer_onInformationButtonClick = function onInformationButtonClick(
            event)
        {
            VieMer_informationImage.classList.toggle("hidden");
        };

        return VieMer;
    })();

    /**
     *  @function main
     *      Script entry point.
     */
    let main = function ()
    {
        /* First */
        MixEnergetik.LoaderManager && MixEnergetik.LoaderManager.initialize();

        /* Init silence, custom one */
        {
            let silencePath = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/default/Silence_6_874.ogg";
            let silenceWrapper = MixEnergetik.Utilitary.SoundBuilder.createSoundWrapper(silencePath);

            MixEnergetik.AudioManager.provideSilence(silenceWrapper);
        }

        /* Initialize things */
        MixEnergetik.SoloManager.initialize();

        MixEnergetik.UI.SequencerView.initialize();
        MixEnergetik.UI.ChargeView.initialize();
        MixEnergetik.UI.VideoView.initialize();
        MixEnergetik.UI.VideoView.hide();
        MixEnergetik.UI.PresetPanelView.initialize();
        MixEnergetik.UI.SampleListView.initialize();
        MixEnergetik.ApplicationControlManager.initialize();
        MixEnergetik.DragAndDropManager.initialize();
        MixEnergetik.VideoManager.initialize();
        MixEnergetik.GroupManager.initialize();
        MixEnergetik.SampleManager.initialize();
        MixEnergetik.SequencerManager.initialize();
        // MixEnergetik.UI.CreditsView.initialize();

        /* Create an additional bin on this page */
        MixEnergetik.UI.ChargeView.createBin();
        /* Create 3 additional presets on this page */
        for (let i = 0; i < 3; i++)
        {
            MixEnergetik.UI.PresetPanelView.addPreset();
        }

        /* Specific init */
        MixEnergetik.VieMer.initialize();
    };

    {
        function __loadMain()
        {
            window.removeEventListener("load", __loadMain, false);
            main();
        }
        window.addEventListener("load", __loadMain, false);
    }
})();

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="Author" content="Marcelin Lazzarotto, Fabrice Lazzarotto,
            Christian Perret">
        <meta name="Category"
            content="Prototype, Sound Design">
        <meta name="Copyright" content="Marcelin Lazzarotto">
        <meta name="Description" content="Et si on faisait de l'énergie avec
            des éléments et de la musique ?">
        <meta name="Keywords"
            content="Prototype, energiziks, Sound Design">
        <meta name="Publisher" content="Marcelin Lazzarotto">
        <meta name="Robots" content="noindex, nofollow">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="fr">

        <link rel="stylesheet"
            href="/energiziks/resources/modules/common/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Menu/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Menu/Introduction/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Menu/Main/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/Menu/EnergyFrance/style.css">
        <link rel="stylesheet"
            href="/energiziks/resources/modules/CreditsView/style.css">
        <link rel="stylesheet" href="/energiziks/public/menu/style.css">

        <title>energiziks - Menu principal</title>
    </head>
    <body>
        <main id="Application">
            <div id="Background"></div>
            <img id="LegalImage" class="noninteractable"
                src="/energiziks/resources/modules/Menu/Mentions.png"
                alt="Mentions" />
            <?php
                require("../../resources/modules/Menu/Introduction/template.html");
                require("../../resources/modules/Menu/Main/template.html");
                require("../../resources/modules/Menu/EnergyFrance/template.html");
                require("../../resources/modules/CreditsView/template.html");
            ?>
        </main>

        <script type="text/javascript"
            src="/energiziks/script/libraries/Pizzicato/Pizzicato.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/resources/modules/CreditsView/script.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/menu/menuIntroduction.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/menu/menuMain.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/menu/menuEnergyFrance.js">
        </script>
        <script type="text/javascript"
            src="/energiziks/public/menu/script.js">
        </script>
    </body>
</html>

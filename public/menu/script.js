/**
 *  File:   menu.js
 *  Author: Marcelin lazzarotto
 *  Date:   2018-05-16
 */

"use strict";

var Menu = Menu || {};

(function()
{
    let Menu_background = null;

    let Menu_resizeCallback = function resizeCallback()
    {
        const ratio = (16 / 9);
        let width = Menu_background.clientWidth;
        let height = width / ratio;

        Menu_background.style.height = height + "px";
    };

    /**
     *  @function main
     *      Script entry point.
     */
    let main = function main()
    {
        Menu_background = document.getElementById("Background");

        window.addEventListener("resize", Menu_resizeCallback, false);

        Menu.Introduction.initialize();
        Menu.Main.initialize();
        Menu.EnergyFrance.initialize();

        MixEnergetik.UI.CreditsView.initialize();

        Menu_resizeCallback();
    };

    {
        function __loadMain()
        {
            window.removeEventListener("load", __loadMain, false);
            main();
        }
        window.addEventListener("load", __loadMain, false);
    }
})();

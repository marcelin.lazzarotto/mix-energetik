/*
 *  File:   SampleManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-07-03
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module SampleManager
 *      The manager dealing with things that loads, to lock the app while it
 *      loads.
 */
MixEnergetik.SampleManager = (function()
{
    let SampleManager = {};

    /**
     *  @function add
     *      A given sample to the playing samples.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The sample item we extract the sound from.
     */
    SampleManager.add = function add(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem, "MixEnergetik.SampleManager.add()",
            "sampleItem", "MixEnergetik.UI.SampleItem");

        MixEnergetik.AudioManager.addSound(sampleItem.soundData);

        sampleItem.use();
    };

    /**
     *  @function remove
     *      A given sample to the playing samples.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The sample item we extract the sound from.
     */
    SampleManager.remove = function remove(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem, "MixEnergetik.SampleManager.remove()",
            "sampleItem", "MixEnergetik.UI.SampleItem");

        const index = SampleManager_temporaryItems.indexOf(sampleItem);

        if (index > -1)
        {
            /* In case we already did it, it means the user reclicked before
             * end, and just want to deactivate it.
             * (Gosh this is complicated)
             */
            SampleManager_temporaryItems.splice(index, 1);

            MixEnergetik.AudioManager.removeSound(sampleItem.soundData);

            sampleItem.unuse();

            sampleItem.playLonger = false;
        }
        else if (sampleItem.playLonger)
        {
            /* In case we were not held by a key, we must remove the temporary
             * thing. */
            SampleManager_temporaryItems.push(sampleItem);

            sampleItem.playLonger = false;
        }
        else
        {
            MixEnergetik.AudioManager.removeSound(sampleItem.soundData);

            sampleItem.unuse();
        }
    };

    /**
     *  @function isActive
     *      Checks if the manager is active.
     *      Basically, we got items in the temporaryItems array.
     */
    SampleManager.isActive = function isActive()
    {
        return (SampleManager_temporaryItems.length > 0);
    };

    SampleManager.initialize = function initialize()
    {
        MixEnergetik.AudioManager.event.on("stop", SampleManager_cleanTemporaryItems);
        MixEnergetik.AudioManager.event.on("end", SampleManager_cleanTemporaryItems);
        MixEnergetik.GroupManager.event.on("stop", SampleManager_cleanTemporaryItems);
    };

    /* Stop clicked samples */
    let SampleManager_temporaryItems = [];

    let SampleManager_cleanTemporaryItems = function cleanTemporaryItems()
    {
        for (let i = SampleManager_temporaryItems.length - 1; i >= 0; i--)
        {
            const sampleItem = SampleManager_temporaryItems.splice(i, 1)[0];

            MixEnergetik.AudioManager.removeSound(sampleItem.soundData);

            sampleItem.unuse();
        }
    };

    return SampleManager;
})();

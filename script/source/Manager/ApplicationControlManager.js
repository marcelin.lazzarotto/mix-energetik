/*
 *  File:   ApplicationControlManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-01

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module ApplicationControlManager
 *      Deals with application control (in-menu, solo...).
 */
MixEnergetik.ApplicationControlManager = (function()
{
    let ApplicationControlManager = {};

    /**
     *  @function initialize
     *      Initialize the manager with a callback.
     */
    ApplicationControlManager.initialize = function initialize()
    {
    };

    return ApplicationControlManager;
})();

/*
 *  File:   GroupManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module GroupManager
 *      The controller dealing with group logic.
 *      The application may activate a group, with let us mix a small group of
 *      sample together as helpers.
 *      One group may be active at the same time, and the main sequencer must
 *      not be active at the same time.
 */
MixEnergetik.GroupManager = (function()
{
    let GroupManager = {};

    /**
     *  @property {object} status
     *      Object to keep track of additional status information.
     */
    GroupManager.status = {};
    GroupManager.status.activity = "off";
    GroupManager.status.activeChanged = false;

    /**
     *  @function isActive
     *      Returns whether a group is currently active or not.
     *
     *  @returns {bool}
     *      true if activeGroup !== null
     *      false otherwise
     */
    GroupManager.isActive = function isActive()
    {
        return GroupManager_playList.length > 0;
    };

    /**
     *  @function getActive
     *      Retrieves the current active group.
     *
     *  @returns {MixEnergetik.UI.GroupItem}
     *      A GroupItem object if activeGroup !== null
     *      null otherwise
     */
    GroupManager.getActive = function getActive()
    {
        return GroupManager_playList[0] || null;
    };

    /**
     *  @function deactivate
     *      Deactivate the GroupItem object contained in activeGroup.
     *      This re-enables all samples previously deactivated.
     *      Do nothing if activeGroup === null.
     */
    GroupManager.deactivate = function deactivate()
    {
        if(!GroupManager.isActive())
        {
            return;
        }

        for (let i = GroupManager_playList.length - 1; i >= 0; i--)
        {
            MixEnergetik.GroupManager.remove(GroupManager_playList[i]);
        }
    };

    /**
     *  @function indexOf
     *      Returns the index of the GroupItem object inside the waiting list.
     *
     *  @param {MixEnergetik.UI.GroupItem} groupItem
     *      The item to check for.
     *
     *  @returns {integer}
     *      A positive index if the object is present.
     *      -1 otherwise.
     */
    GroupManager.indexOf = function indexOf(groupItem)
    {
        MixEnergetik.Utilitary.Check.object(groupItem,
            MixEnergetik.UI.GroupItem,
            "MixEnergetik.GroupManager.indexOf()",
            "groupItem",
            "MixEnergetik.UI.GroupItem");

        return GroupManager_playList.indexOf(groupItem);
    };

    /**
     *  @function push
     *      Add a GroupItem object inside the waiting list, so that the
     *      currently active group is followed by those in the list.
     *      Do nothing if the item is already in the list.
     *
     *  @param {MixEnergetik.UI.GroupItem} groupItem
     *      The group to add.
     *
     *  @throws {Error}
     *      If groupItem is undefined, null or not a valid GroupItem instance.
     */
    GroupManager.push = function push(groupItem)
    {
        MixEnergetik.Utilitary.Check.object(groupItem,
            MixEnergetik.UI.GroupItem,
            "MixEnergetik.GroupManager.push()",
            "groupItem",
            "MixEnergetik.UI.GroupItem");

        if (MixEnergetik.GroupManager.indexOf(groupItem) > -1)
        {
            return;
        }

        if (MixEnergetik.SequencerManager.isActive())
        {
            MixEnergetik.SequencerManager.deactivate();
        }

        let isFirst = (GroupManager_playList.length === 0);

        GroupManager_playList.push(groupItem);

        if (isFirst)
        {
            MixEnergetik.AudioManager.addGroup(groupItem.groupData);
            groupItem.use();

            MixEnergetik.GroupManager.status.activeChanged = true;
            MixEnergetik.GroupManager.status.activity = "on";

            MixEnergetik.GroupManager.event.trigger("play");
        }
        else
        {
            groupItem.setStyle("waiting");
        }
    };

    /**
     *  @function remove
     *      Removes a GroupItem object inside the waiting list. Do nothing if
     *      the item is not in.
     *
     *  @param {MixEnergetik.UI.GroupItem} groupItem
     *      The group to remove.
     *
     *  @throws {Error}
     *      If groupItem is undefined, null or not a valid GroupItem instance.
     */
    GroupManager.remove = function remove(groupItem)
    {
        MixEnergetik.Utilitary.Check.object(groupItem,
            MixEnergetik.UI.GroupItem,
            "MixEnergetik.GroupManager.remove()",
            "groupItem",
            "MixEnergetik.UI.GroupItem");

        /* Safety */
        if (!MixEnergetik.GroupManager.isActive())
        {
            throw new Error("Wtf?");
        }

        let index = MixEnergetik.GroupManager.indexOf(groupItem);

        /* Safety 2 */
        if (index === -1)
        {
            return;
        }

        if (index === 0)
        {
            /* We stopped this group */
            GroupManager_next = GroupManager_playList[1];

            MixEnergetik.GroupManager.event.trigger("stop");
        }

        GroupManager_playList.splice(index, 1);

        if (index === 0) /* first */
        {
            /* Switch */
            MixEnergetik.AudioManager.removeGroup(groupItem.groupData);

            MixEnergetik.GroupManager.status.activeChanged = false;

            if (GroupManager_playList.length !== 0)
            {
                /* Check we have not finished */
                MixEnergetik.AudioManager.addGroup(GroupManager_playList[0].groupData);
                GroupManager_playList[0].setStyle("standard");
                GroupManager_playList[0].use();

                MixEnergetik.GroupManager.status.activeChanged = true;

                MixEnergetik.GroupManager.event.trigger("play");
            }

            groupItem.unuse();
        }
        else
        {
            groupItem.setStyle("standard");
        }
    };

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      An object dealings with event the GroupManager will eventually
     *      send to given callbacks.
     *
     *      Current implemented for groups events are:
     *      - "play", when a group has just started playing;
     *      - "end", when a group has just naturally finished playing;
     *      - "stop", when a group has been stopped while playing.
     */
    GroupManager.event = new MixEnergetik.Utilitary.Event();

    /**
     *  @function pause
     *      Pause the group list from playing, but still keep track of
     *      information.
     *      Used when solo mode if activated for instance.
     */
    GroupManager.pause = function pause()
    {
        const GM = MixEnergetik.GroupManager;

        if (!GM.isActive() || (GM.status.activity === "paused"))
        {
            return;
        }

        let active = GM.getActive();

        /* Switch */
        MixEnergetik.AudioManager.removeGroup(active.groupData);
        active.unuse();

        GM.status.activity = "paused";
    };

    /**
     *  @function resume
     *      Resume play for the group list.
     *      Used when solo mode if activated for instance.
     *
     *  @param {number} offset
     *      The offset at which the group should restart.
     */
    GroupManager.resume = function resume(offset)
    {
        const GM = MixEnergetik.GroupManager;

        if (!GM.isActive() || (GM.status.activity !== "paused"))
        {
            return;
        }

        let active = GM.getActive();

        /* Switch */
        MixEnergetik.AudioManager.addGroup(active.groupData, offset);
        active.use();

        GM.status.activity = "on";
    };

    let GroupManager_fancyIfSequence = function fancyIfSequence()
    {
        const GroupM = MixEnergetik.GroupManager;
        const group = GroupM.getActive();

        if (MixEnergetik.UI.SequencerView.indexOf(group) !== -1)
        {
            /* Group is sequence, fancy this. */
            group.sequencer_fancy.iconLoop.classList.remove("hidden");
        }
    };

    let GroupManager_unfancyIfSequence = function unfancyIfSequence()
    {
        const GroupM = MixEnergetik.GroupManager;
        const group = GroupM.getActive();

        if (MixEnergetik.UI.SequencerView.indexOf(group) !== -1)
        {
            /* Group is sequence, fancy this. */
            group.sequencer_fancy.iconLoop.classList.add("hidden");
        }
    };

    GroupManager.event.on("play", GroupManager_fancyIfSequence);
    GroupManager.event.on("end", GroupManager_unfancyIfSequence);
    GroupManager.event.on("stop", GroupManager_unfancyIfSequence);

    /**
     *  @function onAudioEnd
     *      Callback on the audio manager. If the audio end while the
     *      GroupManager is active, it means we want to play the next group.
     */
    let GroupManager_onAudioEnd = function onAudioEnd()
    {
        const AudioM = MixEnergetik.AudioManager;
        const GroupM = MixEnergetik.GroupManager;

        if (!GroupM.isActive())
        {
            /* Safety */
            return;
        }

        GroupManager_next = (GroupManager_playList[1] || GroupManager_playList[0]);

        GroupM.event.trigger("end");

        if (MixEnergetik.SoloManager.soloItem !== null)
        {
            /* Playing solo, we need to stick with the same group, except,
             * doing nothing except triggering group end and play. */
        }
        else
        {
            AudioM.removeGroup(GroupM.getActive().groupData);

            GroupM.status.activeChanged = false;

            /* If there is only one, special case: we loop. More we shift */
            if (GroupManager_playList.length > 1)
            {
                GroupManager_playList[0].unuse();
                GroupManager_playList.shift();
                GroupManager_playList[0].setStyle("standard");

                GroupM.status.activeChanged = true;
            }

            /* Play the new (or same) group in succession */
            AudioM.addGroup(GroupManager_playList[0].groupData);

            GroupManager_playList[0].use();
        }

        GroupM.event.trigger("play");
    };

    /**
     *  @function getNext
     *      Returns the next group to play.
     *      Can be undefined or null if the GroupManager is or is going off.
     */
    GroupManager.getNext = function getNext()
    {
        return GroupManager_next;
    };

    /**
     *  @function initialize
     *      Initialize the group manager.
     */
    GroupManager.initialize = function initialize()
    {
        MixEnergetik.AudioManager.event.on("end", GroupManager_onAudioEnd);
    };

    /**
     *  @property {MixEnergetik.UI.GroupItem[]} GroupManager_playList
     *      Array of the group in waiting to play after the current active
     *      one.
     */
    let GroupManager_playList = [];

    /**
     *  @property {MixEnergetik.UI.GroupItem} next
     *      Keep track of the next group to play for trade.
     */
    let GroupManager_next = null;

    return GroupManager;
})();

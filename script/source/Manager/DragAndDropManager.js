/*
 *  File:   DragAndDropManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module DragAndDropManager
 *      The kinda big interface between everything.
 */
MixEnergetik.DragAndDropManager = (function()
{
    let isDropSuccessful = function isDropSuccessful(event)
    {
        return event.dataTransfer.dropEffect !== "none";
    };

    /**
     *  @object DragAndDropManager
     *      Deals with drag and drop effects.
     */
    let DragAndDropManager = Object.create(null,
    {
        /**
         *  @property {UI.AudioSource} draggedItem
         *      A reference to the wrapper of the HTML element being dragged.
         *      Handy, no? No?
         *
         *      The item being null means nothing is currently dragged.
         */
        _draggedItem:
        {
            configurable: false,
            enumerable: false,
            writable: true,

            value: null
        },

        /**
         *  @property {UI.AudioSource} _targetItem
         *      The potential target item of the drop.
         */
        _targetItem:
        {
            configurable: false,
            enumerable: false,
            writable: true,

            value: null
        },

        /**
         *  @accessors draggedItem
         *      Wrapper around the drag element.
         *
         *  @field this._draggedItem
         */
        draggedItem:
        {
            configurable: false,
            enumerable: true,

            get: function()
            {
                return this._draggedItem;
            },
            set: function(draggedItem)
            {
                if (draggedItem)
                {
                    this._draggedItem = draggedItem;
                }
                else
                {
                    this._draggedItem = null;
                    this.targetItem = null;
                }
            }
        },

        /**
         *  @accessors targetItem
         *      Wrapper around the drop target.
         *
         *  @field this._targetItem
         */
        targetItem:
        {
            configurable: false,
            enumerable: true,

            get: function()
            {
                return this._targetItem;
            },
            set: function(targetItem)
            {
                if (targetItem === this._targetItem)
                {
                    return;
                }

                if (this._targetItem && this._targetItem.dropTargetOff)
                {
                    this._targetItem.dropTargetOff();
                }

                if (targetItem && targetItem.dropTargetOn)
                {
                    targetItem.dropTargetOn();
                }

                this._targetItem = targetItem;
            }
        }
    });

    /**
     *  @function dragCallback
     *      Deals with drag event:
     *      - fired on the draggable target
     *      - occurs when an element is being dragged
     */
    DragAndDropManager.dragCallback = function dragCallback(event)
    {
    };

    /**
     *  @function dragstartCallback
     *      Deals with dragstart event:
     *      - fired on the draggable target
     *      - occurs when the user starts to drag an element
     */
    DragAndDropManager.dragstartCallback = function dragstartCallback(event)
    {
        let dragged = MixEnergetik.DragAndDropManager.draggedItem;

        if (dragged === null)
        {
            return;
        }

        if (dragged.disabled)
        {
            return;
        }

        if (dragged.source.draggable)
        {
            event.dataTransfer.effectAllowed = "link";

            event.dataTransfer.setData("text/plain", null);

            dragged.source.classList.add("dragged");
        }
    };

    /**
     *  @function dragendCallback
     *      Deals with dragend event:
     *      - fired on the draggable target
     *      - occurs when the user has finished dragging the element
     */
    DragAndDropManager.dragendCallback = function dragendCallback(event)
    {
        let dragged = MixEnergetik.DragAndDropManager.draggedItem;
        let target  = MixEnergetik.DragAndDropManager.targetItem;

        if (dragged === null)
        {
            return;
        }

        if (!isDropSuccessful(event))
        {
            /* This means a cancel. */
            if (dragged instanceof MixEnergetik.UI.AudioItem)
            {
                /* WARNING: side effect (but cool) removes a sample item when
                 * dragged out when in active preset due to call to stop
                 * 2018-04-20: I don't remember this. */
                dragged.stop();
            }

            if (dragged instanceof MixEnergetik.UI.GroupItem)
            {
                if ((MixEnergetik.UI.MapView === undefined)
                    || (MixEnergetik.UI.MapView === null)
                    || !MixEnergetik.UI.MapView.isRegion(dragged))
                {
                    if (!target || (target instanceof MixEnergetik.UI.GroupItem))
                    {
                        /* Remove samples if we are not a region and we ended
                         * somewhere free. */
                        dragged.clear();

                        if (MixEnergetik.UI.SequencerView.indexOf(dragged) !== -1)
                        {
                            /* If we belongs to sequencer, add a silence
                            * TODO ugly */
                            dragged.addSound(MixEnergetik.AudioManager.silence);
                        }
                    }
                }
            }
        }
        else
        {
            if (dragged instanceof MixEnergetik.UI.SampleItem)
            {
                if (MixEnergetik.GroupManager.isActive()
                    && (target instanceof MixEnergetik.UI.GroupItem)
                    && (target !== MixEnergetik.GroupManager.getActive()))
                {
                    dragged.stop();
                }
                else if ((MixEnergetik.UI.BatteryItem)
                    && (target instanceof MixEnergetik.UI.BatteryItem))
                {
                    dragged.stop();
                }
                // else if ((MixEnergetik.UI.MapView)
                //     && (MixEnergetik.UI.MapView.isRegion(target)))
                // {
                //     dragged.stop();
                // }
                else if (MixEnergetik.GroupManager.isActive()
                    && MixEnergetik.GroupManager.getActive() === target)
                {
                    /* If the drag is successful don't stop the sound which
                     * should be already running, but mark the item as used */
                    dragged.use();
                }
                else if (target instanceof MixEnergetik.UI.BinItem)
                {
                    if ((dragged.parent)
                        && (dragged.parent instanceof MixEnergetik.UI.BatteryItem)
                        /* && target.type === "BIN_SAMPLE" */)
                    {
                        dragged.control.retract();
                        dragged.stop();
                        dragged.parent.clear();
                        dragged.parent = null;
                    }
                }
            }
            else if ((dragged instanceof MixEnergetik.UI.GroupItem)
                && (!MixEnergetik.UI.MapView
                    || !MixEnergetik.UI.MapView.isRegion(dragged)))
            {
                if ((target instanceof MixEnergetik.UI.BinItem)
                    && (target.type === "BIN_GROUP"))
                {
                    dragged.stop();
                    dragged.clear();
                }
            }
        }

        /* In any case, we are not dragged anymore */
        MixEnergetik.DragAndDropManager.targetItem = null;
        MixEnergetik.DragAndDropManager.draggedItem = null;
        dragged.source.classList.remove("dragged");
    };

    /**
     *  @function dragenterCallback
     *      Deals with dragenter event:
     *      - fired on the drop target
     *      - occurs when the dragged element enters the drop target
     */
    DragAndDropManager.dragenterCallback = function dragenterCallback(event)
    {
    };

    /**
     *  @function dragoverCallback
     *      Deals with dragover event:
     *      - fired on the drop target
     *      - occurs when the dragged element is over the drop target
     */
    DragAndDropManager.dragoverCallback = function dragoverCallback(event)
    {
        let dragged = MixEnergetik.DragAndDropManager.draggedItem;
        let target  = MixEnergetik.DragAndDropManager.targetItem;

        if (DnDM_testExportToImport()
            || DnDM_testSampleToGroup()
            || DnDM_testGroupToGroup()
            || DnDM_testSampleToBin()
            || DnDM_testGroupToBin()
            || DnDM_testExportedToBin_temp())
        {
            /* To allow drop */
            event.preventDefault();

            event.dataTransfer.dropEffect = "link";
        }
    };

    /**
     *  @function dragleaveCallback
     *      Deals with dragleave event:
     *      - fired on the drop target
     *      - occurs when the dragged element leaves the drop target
     */
    DragAndDropManager.dragleaveCallback = function dragleaveCallback(event)
    {
        /* We can remove the target from controller */
        MixEnergetik.DragAndDropManager.targetItem = null;
    };

    /**
     *  @function dropCallback
     *      Deals with drop event:
     *      - fired on the drop target
     *      - occurs when the dragged element is dropped on the drop target
     */
    DragAndDropManager.dropCallback = function dropCallback(event)
    {
        let dragged = MixEnergetik.DragAndDropManager.draggedItem;
        let target  = MixEnergetik.DragAndDropManager.targetItem;

        if (DnDM_testExportToImport())
        {
            event.preventDefault(); /* avoid opening as a link */

            dragged.stop();

            MixEnergetik.UI.TradeView.importSample(dragged);

            return;
        }

        if (DnDM_testImportToRegion())
        {
            event.preventDefault(); /* avoid opening as a link */

            dragged.stop();

            target.addImportSample(dragged.soundData);

            return;
        }

        if (DnDM_testSampleToGroup())
        {
            event.preventDefault(); /* avoid opening as a link */

            dragged.stop();

            target.addSound(dragged.soundData);

            return;
        }

        if (DnDM_testGroupToGroup())
        {
            event.preventDefault(); /* avoid opening as a link */

            if (target.isEmpty())
            {
                target.addGroup(dragged.groupData);

                if (!MixEnergetik.SequencerManager.isActive())
                {
                    target.play();
                }

                if (MixEnergetik.UI.MapView
                    && MixEnergetik.UI.MapView.isRegion(dragged))
                {
                    /* Region -> sequencer */
                    target.specialization.origin = dragged;
                }
            }

            return;
        }

        if (DnDM_testSampleToBin())
        {
            event.preventDefault();

            return;
        }

        if (DnDM_testGroupToBin())
        {
            event.preventDefault();

            return;
        }

        if (DnDM_testExportedToBin_temp())
        {
            event.preventDefault();

            dragged.stop();

            MixEnergetik.UI.TradeView.revokeSample(dragged);

            return;
        }
    };

    DragAndDropManager.initialize = function initialize()
    {
        document.addEventListener("drag", MixEnergetik.DragAndDropManager.dragCallback, false);
        document.addEventListener("dragstart", MixEnergetik.DragAndDropManager.dragstartCallback, false);
        document.addEventListener("dragend", MixEnergetik.DragAndDropManager.dragendCallback, false);
        document.addEventListener("dragenter", MixEnergetik.DragAndDropManager.dragenterCallback, false);
        document.addEventListener("dragover", MixEnergetik.DragAndDropManager.dragoverCallback, false);
        document.addEventListener("dragleave", MixEnergetik.DragAndDropManager.dragleaveCallback, false);
        document.addEventListener("drop", MixEnergetik.DragAndDropManager.dropCallback, false);
    };

    let DnDM_testExportedToBin_temp = function testExportedToBin()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        return (MixEnergetik.UI.TradeView)
            && (dragged instanceof MixEnergetik.UI.SampleItem)
            && (MixEnergetik.UI.TradeView.isImportSample(dragged))
            && (target === MixEnergetik.UI.TradeView) // TODO
            && (MixEnergetik.UI.TradeView.isExportedSample(dragged));
    };

    let DnDM_testExportToImport = function testExportToImport()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        if ((MixEnergetik.UI.TradeView)
            && (dragged instanceof MixEnergetik.UI.SampleItem)
            && (MixEnergetik.UI.TradeView.isExportSample(dragged)
            && (target === MixEnergetik.UI.TradeView)))
        {
            const regionId = MixEnergetik.TradeManager.regionItem.specialization.regionId;
            const regionData = MixEnergetik.RegionManager.regions[regionId];
            const exportedItems = MixEnergetik.TradeManager.exportedItems[regionId];
            let exportCount = 0

            if (exportedItems)
            {
                exportCount = exportedItems.length;
            }

            return ((regionData.getTradeType() === "TYPE_EXPORT")
                && (exportCount < -regionData.getTradeLevel()));
        }

        return false;
    };

    let DnDM_testImportToRegion = function testImportToRegion()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        if ((dragged instanceof MixEnergetik.UI.SampleItem)
            && (target instanceof MixEnergetik.UI.GroupItem)
            && MixEnergetik.UI.MapView
            && MixEnergetik.UI.MapView.isRegion(target)
            && MixEnergetik.UI.TradeView
            && MixEnergetik.UI.TradeView.isImportSample(dragged)
            && (target === MixEnergetik.GroupManager.getActive()))
        {
            const regionId = target.specialization.regionId;
            const regionData = MixEnergetik.RegionManager.regions[regionId];
            const importLevel = MixEnergetik.TradeManager.getImportLevel(target);

            return ((regionData.getTradeType() === "TYPE_IMPORT")
                && (importLevel < regionData.getTradeLevel()));
        }

        return false;
    };

    let DnDM_testSampleToGroup = function testSampleToGroup()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        const isSampleNonTrade = ((dragged instanceof MixEnergetik.UI.SampleItem)
            && !(MixEnergetik.UI.TradeView
                && (MixEnergetik.UI.TradeView.isImportSample(dragged)
                    || MixEnergetik.UI.TradeView.isExportSample(dragged)
                )
            )
        );
        const isGroupNonRegion = ((target instanceof MixEnergetik.UI.GroupItem)
            && !(MixEnergetik.UI.MapView
                && MixEnergetik.UI.MapView.isRegion(target)
            )
        );

        return (DnDM_testImportToRegion()
            || (isSampleNonTrade && isGroupNonRegion));
    };

    let DnDM_testGroupToGroup = function testGroupToGroup()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        return ((dragged instanceof MixEnergetik.UI.GroupItem)
            && (target instanceof MixEnergetik.UI.GroupItem)
            && !(MixEnergetik.UI.MapView
                && MixEnergetik.UI.MapView.isRegion(target)
            )
        );
    };

    let DnDM_testSampleToBin = function testSampleToBin()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        return ((dragged instanceof MixEnergetik.UI.SampleItem)
            && (target instanceof MixEnergetik.UI.BinItem)
            && (dragged.parent)
            && (target.type === "BIN_SAMPLE"));
    };

    let DnDM_testGroupToBin = function testGroupToBin()
    {
        const dragged = MixEnergetik.DragAndDropManager.draggedItem;
        const target  = MixEnergetik.DragAndDropManager.targetItem;

        return ((dragged instanceof MixEnergetik.UI.GroupItem)
            && (target instanceof MixEnergetik.UI.BinItem)
            && !(MixEnergetik.UI.MapView
                && MixEnergetik.UI.MapView.isRegion(dragged)
            )
        );
    };

    return DragAndDropManager;
})();

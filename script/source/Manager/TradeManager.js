/*
 *  File:   TradeManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-05-06
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module TradeManager
 *      The kinda big interface between everything.
 */
MixEnergetik.TradeManager = (function()
{
    let TradeManager = {};

    /**
     *  @property {objectmap} tradeItems
     *      Sample items so that we keep them in track.
     *      Trade items are stored by style and then by type (import/export)
     *      and then, for exports, by regions id.
     *
     *      Ex:
     *      tradeItems["rock on"] =
     *      {
     *          import:[...],
     *          export:
     *          {
     *              "Hauts-De-France": [...],
     *              "Occitanie": [...],
     *              ...
     *          }
     *      }
     */
    TradeManager.tradeItems = {};

    /**
     *  @property {MixEnergetik.UI.SampleItem objectmap} exportedItems
     *      Keeps track of the items that have been exported in the import
     *      sample list.
     *      Stored by region identifier.
     *      (Clarification: actually identify the newly CREATED SampleItem
     *      object).
     */
    TradeManager.exportedItems = {};

    /**
     *  @property {MixEnergetik.UI.GroupItem} regionItem
     *      Identifier of the currently playing region, to get extraction right.
     */
    TradeManager.regionItem = null;

    /**
     *  @function stop
     *      Stops the currently playing samples in the trade view.
     */
    TradeManager.stop = function stop()
    {
        const style = MixEnergetik.TradeManager.getStyle;

        if (!MixEnergetik.TradeManager.tradeItems[style])
        {
            return;
        }

        const importItems = MixEnergetik.TradeManager.tradeItems[style].import;
        const exportItems = MixEnergetik.TradeManager.tradeItems[style].export;

        for (let i = 0; i < importItems.length; i++)
        {
            importItems[i].stop();
            // importItems[i].unuse();
        }

        for (let regionId in exportItems)
        {
            for (let i = 0; i < exportItems[regionId].length; i++)
            {
                exportItems[regionId][i].stop();
                // exportItems[regionId][i].unuse();
            }
        }
    };

    /**
     *  @function clear
     *      Clears the trade logic (should only be called when updating
     *      regions).
     */
    TradeManager.clear = function clear()
    {
        MixEnergetik.TradeManager.stop();

        MixEnergetik.UI.TradeView.clear();

        MixEnergetik.TradeManager.exportedItems = {};
        MixEnergetik.TradeManager.regionItem = null;
    };

    /**
     *  @function getStyle
     *      Returns the style currently used.
     *
     *  @returns {string}
     *      The style used. Used to identify things.
     */
    TradeManager.getStyle = function getStyle()
    {
        let style = "";

        if (MixEnergetik.UI.EnergeticSelectionView)
        {
            style = MixEnergetik.UI.EnergeticSelectionView.style;
        }
        else
        {
            style = "element";
        }

        return style;
    };

    /**
     *  @function getImportLevel
     *      Retrieves the total import level of given region item.
     *
     *  @param {MixEnergetik.UI.GroupItem} regionItem
     *      The region item from which to seek the import level.
     */
    TradeManager.getImportLevel = function getImportLevel(regionItem)
    {
        MixEnergetik.Utilitary.Check.object(regionItem,
            MixEnergetik.UI.GroupItem,
            "MixEnergetik.TradeManager.getImportLevel()", "regionItem",
            "MixEnergetik.UI.GroupItem");

        if (!regionItem.importData)
        {
            return 0.0;
        }

        return (regionItem.importData.length / 2);
    };

    TradeManager.initialize = function initialize()
    {
        MixEnergetik.GroupManager.event.on("play", TradeManager_showTrade);
        MixEnergetik.GroupManager.event.on("end", TradeManager_hideTrade);
        MixEnergetik.GroupManager.event.on("stop", TradeManager_hideTrade);
    };

    let TradeManager_showTrade = function showTrade()
    {
        const groupItem = MixEnergetik.GroupManager.getActive();
        const regionId = groupItem.specialization.regionId;

        if (!regionId)
        {
            /* TradeView should be hidden, and stay like this. */
            return;
        }

        if (groupItem === MixEnergetik.TradeManager.regionItem)
        {
            /* TradeView is normally visible, and nothing should change */
            return;
        }

        const regionData = MixEnergetik.RegionManager.regions[regionId];
        const tradeType = regionData.getTradeType();

        if (tradeType === "TYPE_SELF_SUFFICIENT")
        {
            return;
        }

        if (MixEnergetik.UI.EnergeticSelectionView)
        {
            if (MixEnergetik.UI.EnergeticSelectionView.status === "consumption")
            {
                MixEnergetik.UI.TradeView.hide();
                return;
            }
        }

        /* Actual update */

        MixEnergetik.TradeManager.regionItem = groupItem;

        if (tradeType === "TYPE_IMPORT")
        {
            MixEnergetik.UI.TradeView.updateImport();
        }
        else /* if (tradeType === "TYPE_EXPORT") */
        {
            MixEnergetik.UI.TradeView.updateExport();
        }

        MixEnergetik.UI.TradeView.show();
    };

    let TradeManager_hideTrade = function hideTrade()
    {
        const nextItem = MixEnergetik.GroupManager.getNext();

        if (nextItem)
        {
            const regionId = nextItem.specialization.regionId;
            if(regionId)
            {
                const regionData = MixEnergetik.RegionManager.regions[regionId];

                if (regionData.getTradeType !== "TYPE_SELF_SUFFICIENT")
                {
                    /* Nothing to hide here */
                    return;
                }
            }
        }

        MixEnergetik.UI.TradeView.hide();
        MixEnergetik.TradeManager.regionItem = null;
    }

    return TradeManager;
})();

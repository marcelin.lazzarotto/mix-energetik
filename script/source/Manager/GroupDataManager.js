/*
 *  File:   GroupDataManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-05-06

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module GroupDataManager
 *      The kinda big interface between everything.
 */
MixEnergetik.GroupDataManager = (function()
{
    let GroupDataManager = {};

    GroupDataManager.sampleItems = {};

    let GroupDataManager_showData = function showData()
    {
        GroupDataManager_currentGroup = MixEnergetik.GroupManager.getActive();

        MixEnergetik.UI.GroupDataView.show(GroupDataManager_currentGroup);
    };

    let GroupDataManager_hideData = function hideData()
    {
        if (GroupDataManager_currentGroup.specialization.regionId)
        {
            MixEnergetik.UI.GroupDataView.hide();
        }

        GroupDataManager_currentGroup = null;
    }

    GroupDataManager.initialize = function initialize()
    {
        MixEnergetik.GroupManager.event.on("play", GroupDataManager_showData);
        MixEnergetik.GroupManager.event.on("end", GroupDataManager_hideData);
        MixEnergetik.GroupManager.event.on("stop", GroupDataManager_hideData);
    };

    let GroupDataManager_currentGroup = null;

    return GroupDataManager;
})();

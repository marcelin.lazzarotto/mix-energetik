/*
 *  File:   Configuration.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @submodule Configuration
 *      Namespace for all that'll be used to keep in mind the application's
 *      configuration.
 */
MixEnergetik.Utilitary.Configuration = (function()
{
    let Configuration = {};
    Configuration.Audio = {};

    Configuration.Audio.bpm = 137;
    /* == average on all sound.buffer.length */
    /* TODO use this for a small indication bar and for metric */
    Configuration.Audio.soundsLength = 0;
    Configuration.Audio.metre = {beatsToBar: 4, bars: 4};
    Configuration.Audio.synchroPrecision = (1 / 8);

    /* In seconds */
    /* TODO ugly */
    Configuration.Audio.sampleDuration = 3.504;
    // Configuration.Audio.sampleDuration = 3.5;

    return Configuration;
})();

/*
 *  File:   IncludeModule.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-04
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @submodule Configuration
 *      Namespace for all that'll be used to keep in mind the application's
 *      configuration.
 */
MixEnergetik.Utilitary.IncludeModule = (function()
{
    let IncludeModule = {};

    /**
     *  @function onload
     *      To be defined by the user, called when IncludeModule.include ended
     *      successfully.
     */
    IncludeModule.onload = null;
    /**
     *  @function onfail
     *      To be defined by the user, called when IncludeModule.include ended
     *      without managing to include the module.
     */
    IncludeModule.onfail = null;

    /**
     *  @function include
     *      Perform HTML inclusion over the page.
     *
     * https://www.w3schools.com/howto/howto_html_include.asp */
    IncludeModule.include = function include()
    {
        let request;

        /* loop through a collection of all HTML elements with class */
        let includeElements = document.getElementsByClassName("MixEnergetik-module");

        for (let i = 0; i < includeElements.length; i++)
        {
            let includeElement = includeElements[i];

            /* Get module name */
            let moduleName = includeElement.getAttribute("data-MixEnergetik-module");
            if ((moduleName !== undefined)
                || (moduleName !== null))
            {
                const MODULE_BASE = "../resources/modules/";

                /* Perform an HTTP request using the module name for what we
                 * want to include. */
                let request = new XMLHttpRequest();
                let fileNameView = MODULE_BASE + moduleName + "/template.html";

                request.onreadystatechange = function()
                {
                    if (this.readyState == 4)
                    {
                        if (this.status == 200)
                        {
                            includeElement.innerHTML = this.responseText;

                            /* Add CSS */
                            {
                                let fileNameStyle = MODULE_BASE + moduleName + "/style.css";
                                let headElement = document.getElementsByTagName("head")[0];
                                let linkElement = document.createElement("link");

                                linkElement.rel = "stylesheet";
                                linkElement.href = fileNameStyle;
                                headElement.appendChild(linkElement);
                            }

                            /* Callback */
                            if (IncludeModule.onload)
                            {
                                IncludeModule.onload();
                            }
                        }
                        else if (this.status == 404)
                        {
                            includeElement.innerHTML = "Page not found.";

                            if (IncludeModule.onerror)
                            {
                                IncludeModule.onerror();
                            }
                            else
                            {
                                throw new Error("[MixEnergetik.Utilitary.IncludeModule]"
                                    + " failed to include a page submodule: "
                                    + moduleName);
                            }
                        }

                        /* Remove the attributes */
                        includeElement.classList.remove("MixEnergetik-module");
                        includeElement.removeAttribute("data-MixEnergetik-module");

                        /* TODO Redo the includes in case we added another
                         * include within the newly added code, but without side
                         * effect */
                        /* IncludeModule.include(); */
                    }
                }

                request.open("GET", fileNameView, true);
                request.send();
            }
        }
    }

    return IncludeModule;
})();

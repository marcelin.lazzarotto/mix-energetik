/*
 *  File:   Check.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @submodule Check
 *      Namespace for function used to check a parameter.
 */
MixEnergetik.Utilitary.Check = (function()
{
    let Check = {};

    /**
     *  @function native
     *      Checks the given parameter is neither undefined or null and of
     *      given native type. Raise exception if any is false.
     *
     *  @param {number || string} param
     *      The param to check.
     *  @param {string} typeString
     *      The string corresponding to the type to check (typically given by
     *      typeof operator).
     *  @param {string} locationString
     *      String used to give a clue about where is the error, when an
     *      exception should be raised.
     *  @param {string} paramString
     *      String used to identify the parameter that failed.
     *
     *  @throws {Error}
     *      Throws an exception if param is either undefined, null or not of the
     *      given typeString.
     */
    Check.native = function nativeCheck(param, typeString,
        locationString, paramString)
    {
        if((param === undefined) || (param === null)
            || (typeof param !== typeString))
        {
            throw new Error ("[" + locationString + "] the " + paramString
                + " parameter must be provided and a valid " + typeString);
        }
    };

    /**
     *  @function object
     *      Checks the given parameter is neither undefined or null and an
     *      instance of given constructor. Raise exception any is false.
     *
     *  @param {number || string} param
     *      The param to check.
     *  @param {string} constructorId
     *      The type to check against.
     *  @param {string} locationString
     *      String used to give a clue about where is the error, when an
     *      exception should be raised.
     *  @param {string} paramString
     *      String used to identify the parameter that failed.
     *  @param {string} constructorString
     *      String used to identify the constructor compared to.
     *
     *  @throws {Error}
     *      Throws an exception if param is either undefined, null or not an
     *      instance of constructorId.
     */
    Check.object = function objectCheck(param, constructorId, locationString,
        paramString, constructorString)
    {
        if ((param === undefined) || (param === null)
            || !(param instanceof constructorId))
        {
            throw new Error ("[" + locationString + "] the " + paramString
                + " parameter must be provided and a valid " + constructorString
                + " object.");
        }
    };

    Check.array = function arrayCheck(param, locationString, paramString)
    {
        if ((param === undefined) || (param === null) || (!Array.isArray(param)))
        {
            throw new Error ("[" + locationString + "] the " + paramString
                + " parameter must be provided and an array");
        }
    };

    return Check;
})();

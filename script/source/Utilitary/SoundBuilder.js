/*
 *  File:   SoundBuilder.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @module SoundBuilder
 *      Is used to bypass Pizzicato.Sound() constructor, thanks to crapy firefox
 *      WebAudioAPI decodeAudioData which cannot read 32 bit float wav data.
 */
MixEnergetik.Utilitary.SoundBuilder = (function()
{
    /* TODO don't forget to make something cleanier (here we assume WAV content)
     * and we could make a better Pizzicato.Sound bypass with only 1 file
     * download. */
    /**
     *  @function fromF32ToI16
     *      Transform given float 32bit wav signal into a int 16 bit wav signal.
     *      PCM for both.
     *
     *  @param {ArrayBuffer} audioData
     *
     */
    let fromF32ToI16 = function(audioData)
    {
        /* a new wav object */
        let wavObject = {};
        wavObject.fileFormat = {};
        wavObject.audioFormat = {};
        wavObject.data = {};

        /* let's remove useless headers at the same time */
        let HEADER_SIZE = 44;

        let dataStart = 0;
        let position = 12;
        let newPosition = 0;
        let newData = null;
        let dv = new DataView(audioData);
        let newDataDV = null;

        wavObject.fileFormat.RIFF = new Uint8Array(audioData, 0, 4);
        wavObject.fileFormat.size = 0; /* for after */
        wavObject.fileFormat.WAVE = new Uint8Array(audioData, 8, 4);

        /* Seek new data length
         * We proceed like this because there might be unecessary headers
         * along the way */
        do
        {
            let header = String.fromCharCode.apply(null,
                new Uint8Array(audioData, position, 4));
            let length = dv.getUint32(position + 4, true);

            if (header.trim() === "fmt")
            {
                wavObject.audioFormat.FMT_ = new Uint8Array(audioData, position,
                    4);
                wavObject.audioFormat.size = length;
                wavObject.audioFormat.audioFormat = 1; /* PCM */
                wavObject.audioFormat.channelCount = dv.getUint16(position + 10,
                    true);
                wavObject.audioFormat.frequency = dv.getUint32(position + 12,
                    true);
                wavObject.audioFormat.bytePerSec = dv.getUint32(position + 16,
                    true) / 2;
                wavObject.audioFormat.bytePerBlock = dv.getUint16(position + 20,
                    true) / 2;
                wavObject.audioFormat.bitsPerSample = 16;
            }
            else if (header.trim() === "data")
            {
                dataStart = position + 8;

                /* since we indeed reduce size by 2 */
                wavObject.data.DATA = new Uint8Array(audioData, position, 4);
                wavObject.data.size = length / 2;
                wavObject.data.buffer = new Uint8Array(audioData, dataStart,
                    length);
            }

            position = position + 8 + length;
        } while(position < audioData.byteLength);

        wavObject.fileFormat.size = HEADER_SIZE + wavObject.data.size;

        newData = new Uint8Array(wavObject.fileFormat.size);
        newDataDV = new DataView(newData.buffer);

        /* recopy headers (and change some info) excepted uneeded junk */
        /* file type */
        newData.set(wavObject.fileFormat.RIFF, 0);
        newDataDV.setUint32(4, wavObject.fileFormat.size, true);
        newData.set(wavObject.fileFormat.WAVE, 8);
        /* audio format */
        newData.set(wavObject.audioFormat.FMT_, 12);
        newDataDV.setUint32(16, wavObject.audioFormat.size, true);
        newDataDV.setUint16(20, wavObject.audioFormat.audioFormat, true);
        newDataDV.setUint16(22, wavObject.audioFormat.channelCount, true);
        newDataDV.setUint32(24, wavObject.audioFormat.frequency, true);
        newDataDV.setUint32(28, wavObject.audioFormat.bytePerSec, true);
        newDataDV.setUint16(32, wavObject.audioFormat.bytePerBlock, true);
        newDataDV.setUint16(34, wavObject.audioFormat.bitsPerSample, true);
        /* data block */
        newData.set(wavObject.data.DATA, 36);
        newDataDV.setUint32(40, wavObject.data.size, true);

        position = dataStart;
        newPosition = 44;

        for (position = dataStart; position < audioData.byteLength;
            position += 4)
        {
            let sample = dv.getFloat32(position, true);

            sample = sample * (65536 / 2);

            newDataDV.setUint16(newPosition, sample, true);

            newPosition += 2;
        }

        return newData.buffer;
    };

    /**
     *  @function loadWave
     *      Hack function to get over firefox faulty decodeAudioData with
     *      unexpected headers.
     *
     *  @param {string} uri
     *      The file to load.
     *  @param {function} handler
     *      Called after load. Receives an error parameter if a problem occured.
     *
     *  @returns {Pizzicato.Sound}
     *      The sound object to load.
     */
    let loadWave = function loadWave(uri, handler)
    {
        /* Dummy sound. */
        let sound = new Pizzicato.Sound();

        let request = new XMLHttpRequest();

        request.open('GET', uri, true);
        request.responseType = 'arraybuffer';
        request.onload = function onload(progressEvent)
        {
            let standardBuffer = progressEvent.target.response;
            let fixedBuffer = fromF32ToI16(progressEvent.target.response);

            Pizzicato.context.decodeAudioData(standardBuffer,
                function(buffer)
                {
                    sound.getRawSourceNode = function()
                    {
                        let node = Pizzicato.context
                            .createBufferSource();
                        node.loop = sound.loop;
                        node.buffer = buffer;
                        return node;
                    };

                    console.log("Loaded " + uri);

                    if (handler)
                    {
                        handler();
                    }
                },
                function(error)
                {
                    Pizzicato.context.decodeAudioData(fixedBuffer,
                        function(buffer)
                        {
                            sound.getRawSourceNode = function()
                            {
                                let node = Pizzicato.context
                                    .createBufferSource();
                                node.loop = sound.loop;
                                node.buffer = buffer;
                                return node;
                            };

                            console.log("Ugly but fixed " + uri);

                            if (handler)
                            {
                                handler();
                            }
                        },
                        function(error)
                        {
                            console.error("Error decoding audio file " + uri);

                            console.log(error);

                            if (handler)
                            {
                                handler(error);
                            }
                        }
                    );
                }
            );
        };
        request.onreadystatechange = function(event)
        {
            if (request.readyState === 4 && request.status !== 200)
            {
                console.error("Error while fetching " + uri + ". "
                    + request.statusText);
            }
        };
        request.send();

        return sound;
    }

    let SoundBuilder = {};

    /**
     *  @function createSoundWrapper
     *      Creates a Pizzicato.Sound item from the given uri and return the
     *      MixEnergetik.SoundWrapper corresponding object.
     *
     *  @param {string} uri
     *      The uri of the sound file.
     *
     *  @returns {MixEnergetik.SoundWrapper}
     *      The sound wrapper item as used in the application if uri is defined,
     *      non null and valid.
     *      Returns null otherwise.
     */
    SoundBuilder.createSoundWrapper = function(uri)
    {
        MixEnergetik.Utilitary.Check.native(uri, "string",
            "MixEnergetik.Utilitary.SoundBuilder.createSoundWrapper()", "uri");

        let handler = function(error)
        {
            /* As we are finished loading, trigger the event */
            MixEnergetik.Utilitary.SoundBuilder.event.trigger("load");
        };
        // let name = file.name.replace(/\.[^/.]+$/, "");
        let uriTS = uri + "?timestamp=" + (+new Date());
        let sound = null;
        let extension = uri.substring(uri.lastIndexOf('.') + 1, uri.length) || uri;

        /* As we are seeking for a resource, trigger the event */
        MixEnergetik.Utilitary.SoundBuilder.event.trigger("fetch");

        if ((extension === "wav") || (extension === "WAV"))
        {
            /* Firefox hack */
            sound = loadWave(uriTS, handler);
        }
        else
        {
            /* mp3 is better supported and I don't know for others */
            sound = new Pizzicato.Sound({source:"file", options:{path:uriTS}}, handler);
        }

        let soundWrapper = new MixEnergetik.SoundWrapper(sound, uri);

        return soundWrapper;
    };

    SoundBuilder.event = new MixEnergetik.Utilitary.Event();

    return SoundBuilder;
})();

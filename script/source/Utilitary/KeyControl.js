/*
 *  File:   KeyControl.js
 *  Author: Marcelin Lazzaroto
 *  Date:   2018-10-22
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @constructor KeyControl
 *      Constructs objects that help dealing with key management (user
 *      interaction).
 *
 *      -MEH-
 *      KeyControl is a bad name, refactor this.
 */
MixEnergetik.Utilitary.KeyControl = (function()
{
    let KeyControl = function KeyControl()
    {
        /* Declarations */

        /**
         *  @property {MixEnergetik.UI.AudioItem objectmap} items
         *      Associative table of items.
         *      Indexed on keys.
         */
        this.items = {};

        let self = this;

        let KeyControl_keyDownCallback = function keyDownCallback(event)
        {
            let key = event.key.toLowerCase();

            if (self.items[key])
            {
                self.items[key].inputDown();
            }
        };

        let KeyControl_keyUpCallback = function keyUpCallback(event)
        {
            let key = event.key.toLowerCase();

            if (self.items[key])
            {
                self.items[key].inputUp();
            }
        };

        /* Construction */

        document.addEventListener("keydown", KeyControl_keyDownCallback, false);
        document.addEventListener("keyup", KeyControl_keyUpCallback, false);
    };

    /**
     *  @function clear
     *      Clears the key control items array.
     */
    KeyControl.prototype.clear = function clear()
    {
        for (let key in this.items)
        {
            let audioItem = this.items[key];

            if (audioItem)
            {
                if (audioItem.source)
                {
                    audioItem.source.removeChild(audioItem.keyCue);
                }
                else if (audioItem.container)
                {
                    audioItem.container.removeChild(audioItem.keyCue);
                }
                audioItem.keyCue = null;

                this.items[key] = null;
            }
        }
    };

    /**
     *  @function addItem
     *      Tries to adds an item to the KeyControl object .items array.
     *      Upon operation, also adds an indication on the item.
     *
     *      Do nothing if all the keys are already allocated.
     *
     *  @param {Object} audioItem
     *      The audioItem to add.
     *      In practice, can be anything implementing inputDown() and inputUp().
     *
     *  @returns {string}
     *      "KeyControl_SUCCESS" if the function succeeded.
     *      "KeyControl_NO_KEY_AVAILABLE" if there wasn't a key available for
     *          the item.
     */
    KeyControl.prototype.addItem = function addItem(audioItem)
    {
        MixEnergetik.Utilitary.Check.native(audioItem,
            "object",
            "MixEnergetik.Utilitary.KeyControl",
            "audioItem");
        MixEnergetik.Utilitary.Check.native(audioItem.inputDown,
            "function",
            "MixEnergetik.Utilitary.KeyControl",
            "audioItem.inputDown");
        MixEnergetik.Utilitary.Check.native(audioItem.inputUp,
            "function",
            "MixEnergetik.Utilitary.KeyControl",
            "audioItem.inputUp");

        for (let key in this.items)
        {
            if (!this.items[key])
            {
                /* We cannot add the cue if the element is not an HTML one */
                audioItem.keyCue = document.createElement("div");
                audioItem.keyCue.innerHTML = key;
                audioItem.keyCue.classList.add("item");
                audioItem.keyCue.classList.add("key");
                audioItem.keyCue.classList.add("cue");
                audioItem.keyCue.classList.add("noninteractable");

                if (audioItem.source)
                {
                    audioItem.source.appendChild(audioItem.keyCue);
                }
                else if (audioItem.container)
                {
                    audioItem.container.appendChild(audioItem.keyCue);
                }

                this.items[key] = audioItem;

                return "KeyControl_SUCCESS";
            }
        }

        return "KeyControl_NO_KEY_AVAILABLE";
    };

    /**
     *  @function setKeys
     *      Sets the key control monitored keys.
     *      Replace existing monitored keys, cleaning registered items.
     *
     *  @param {string[]} keys
     *      Array of string corresponding to the keys.
     *      An empty array effectively removes all keys.
     */
    KeyControl.prototype.setKeys = function setKeys(keys)
    {
        MixEnergetik.Utilitary.Check.array(keys,
            "MixEnergetik.Utilitary.KeyControl()",
            "keys");

        let currentItems = [];

        /* Clear previous monitoring */
        for (let key in this.items)
        {
            let audioItem = this.items[key];

            if (audioItem)
            {
                currentItems.push(this.items[key]);

                if (audioItem.source)
                {
                    audioItem.source.removeChild(audioItem.keyCue);
                }
                else if (audioItem.container)
                {
                    audioItem.container.removeChild(audioItem.keyCue);
                }
                audioItem.keyCue = null;

                this.items[key] = null;
            }
        }

        this.items = {};

        for (let i = 0; i < keys.length; i++)
        {
            this.items[keys[i]] = null;
        }

        for (let i = 0; i < currentItems.length; i++)
        {
            this.addItem(currentItems[i]);
        }
    };

    KeyControl.DEFAULT_KEYS = [
        "a", "z", "e", "r", "t", "y", "u", "i", "o", "p",
        "q", "s", "d", "f", "g", "h", "j", "k", "l", "m"
    ];

    return KeyControl;
})();

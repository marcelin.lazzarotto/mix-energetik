/*
 *  File:   AudioManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module MixEnergetik.AudioManager
 *      Here to deal with audio, with no concern for UI. This is specialy to
 *      deal with Pizzicato group and event thing which is far from perfect for
 *      our case.
 */
MixEnergetik.AudioManager = (function()
{
    /**
     *  @constructor AudioManager
     *      The object that deals with audio.
     */
    let AudioManager = Object.create(null,
    {
        /**
         *  @property {MixEnergetik.GroupWrapper} main
         *      The data for the standard running audio. Is used when no solo
         *      is running.
         */
        main:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: new MixEnergetik.GroupWrapper()
        },

        /**
         *  @property {MixEnergetik.SoundWrapper[]} soundWrappers
         *      Data bank of all the sound in the program, each wrapped by our
         *      custom object.
         */
        soundWrappers:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: []
        },

        /**
         *  @property {MixEnergetik.SoundData} silence
         *      Sound data corresponding to a silence, to fill sequencer
         *      with a sentinel.
         */
        silence:
        {
            configurable: false,
            enumerable: true,
            writable: true,

            value: null
        },

        /**
         *  @function provideSilence
         *      Provide a sound wrapper corresponding to a silent sample. Used
         *      with Sequencer for empty sequences.
         *
         *      Do nothing if silence was already provided.
         *
         *  @param {MixEnergetik.SoundWrapper} soundWrapper
         *      The soundWrapper to be provided.
         *
         *  @throws {Error}
         *      If the soundWrapper parameter is not valid.
         */
        provideSilence:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function provideSilence(soundWrapper)
            {
                MixEnergetik.Utilitary.Check.object(soundWrapper,
                    MixEnergetik.SoundWrapper,
                    "MixEnergetik.AudioManager.provideSilence()",
                    "soundWrapper",
                    "MixEnergetik.SoundWrapper");

                let index = AudioManager.provideSound(soundWrapper);
                MixEnergetik.AudioManager.silence = new MixEnergetik.SoundData(index);
            }
        },

        /**
         *  @function provideSound
         *      Provide a sound for the AudioManager data base (i.e. the
         *      soundWrappers array).
         *
         *  @param {MixEnergetik.SoundWrapper} soundWrapper
         *      The soundWrapper to be provided.
         *
         *  @returns {integer}
         *      The index of the newly added soundWrapper in the array.
         *
         *  @throws {Error}
         *      If the soundWrapper parameter is not valid.
         */
        provideSound:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundWrapper)
            {
                if((soundWrapper === null) || (soundWrapper === undefined)
                    || !(soundWrapper instanceof MixEnergetik.SoundWrapper))
                {
                    throw Error("[AudioManager.provideSound()] the soundWrapper"
                        + " parameter must be provided and a"
                        + " MixEnergetik.SoundWrapper object");
                }

                let index = (this.soundWrappers.push(soundWrapper) - 1);

                return index;
            }
        },

        /**
         *  @function retrieveEffectWrapper
         *      Shortcut function to retrieve the effect wrapper of a sound.
         *
         *  @param {integer} soundIndex
         *      Index of the sound in the soundWrappers array we want to get the
         *      effect from.
         *  @param {integer} effectIndex
         *      Index of the effect in the effect array in a sound wrapper.
         *
         *  @warning
         *      UNCHECKED // TODO
         *
         *  @returns {MixEnergetik.EffectWrapper}
         *      The wrapper for a Pizzicato.Effect object.
         */
        retrieveEffectWrapper:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function retrieveEffectWrapper(soundIndex, effectIndex)
            {
                return (MixEnergetik
                    .AudioManager
                    .soundWrappers[soundIndex]
                    .effectHandler
                    .array[effectIndex]);
            }
        },

        /**
         *  @function addSound
         *      Add a sound to the group of sound.
         *      Do nothing if the sound is in the group.
         *
         *  @param {MixEnergetik.SoundData} soundData
         *      The SoundData object used to play the sound.
         *  @param {number} offset
         *      The time at which the sound must start.
         */
        addSound:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundData, offset)
            {
                const AudioM = MixEnergetik.AudioManager;
                const wrap = AudioM.main;

                offset = offset || wrap.getCurrentTime();

                MixEnergetik.Utilitary.Check.object(soundData,
                    MixEnergetik.SoundData,
                    "MixEnergetik.AudioManager.addSound()",
                    "soundData",
                    MixEnergetik.SoundData);

                if (typeof offset !== "number")
                {
                    offset = wrap.getCurrentTime();
                }

                let soundWrapper = AudioM.soundWrappers[soundData.soundIndex];

                if(AudioM.contains(soundData.soundIndex))
                {
                    /* don't do anything unneccessary */
                    return;
                }

                /* load configuration */
                soundData.load();

                AudioM.main.addSound(soundWrapper.sound);
                AudioM.main.setCurrentTime(offset);
                AudioM.main.play();
            }
        },

        /**
         *  @function removeSound
         *      Remove a sound from the group of sound.
         *      Do nothing if the sound is not in the group.
         */
        removeSound:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundData)
            {
                let soundWrapper = this.soundWrappers[soundData.soundIndex];

                if(!this.contains(soundData.soundIndex))
                {
                    /* don't do anything unneccessary */
                    return;
                }

                /* load configuration */
                soundData.save();

                this.main.removeSound(soundWrapper.sound);

                if (this.main.isEmpty())
                {
                    MixEnergetik.AudioManager.event.trigger("stop");
                }
            }
        },

        /**
         *  @function addGroup
         *      Adds a complete group to the sounds playing.
         *
         *  @param {MixEnergetik.GroupData} groupData
         *      The group containing info for the sounds to add.
         *  @param {number} offset
         *      To start the group at given time.
         */
        addGroup:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(groupData, offset)
            {
                offset = offset || 0.0;

                if((groupData === null) || (groupData === undefined)
                    || !(groupData instanceof MixEnergetik.GroupData))
                {
                    throw new Error("[AudioManager.addGroup()] The groupData"
                        + " parameter must be provided and a valid GroupData"
                        + " object");
                }

                if ((typeof offset !== "number"))
                {
                    offset = 0.0;
                }

                let gain = 1.0;

                /* TODO MEGAMEH correct sound */
                if (MixEnergetik.SequencerManager.isActive())
                {
                    gain = MixEnergetik.SequencerManager.volume;
                }

                /* load group effects */
                for (let i = 0; i < groupData.effectDatas.length; i++)
                {
                    /* clarity */
                    let savedEffect = groupData.effectDatas[i]; /* i === .position also */
                    let effectIndex = savedEffect.index;
                    this.main.effectHandler.usedEffects[savedEffect.position] = effectIndex;

                    for (let key in savedEffect.options)
                    {
                        this.main.effectHandler.array[effectIndex].effect.options[key] = savedEffect.options[key];
                    }

                    if (savedEffect.callback)
                    {
                        this.main.effectHandler.array[effectIndex].effect.callback = savedEffect.callback;
                    }
                }

                for (let i = 0; i < groupData.soundDatas.length; i++)
                {
                    this.addSound(groupData.soundDatas[i], offset);
                }

                this.setGroupVolume(groupData.volume * gain);
            }
        },

        /**
         *  @function removeGroup
         *      Removes the group currently playing.
         *
         *      TODO doc
         */
        removeGroup:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function(groupData)
            {
                if((groupData === null) || (groupData === undefined)
                    || !(groupData instanceof MixEnergetik.GroupData))
                {
                    throw new Error("[AudioManager.removeGroup()] The groupData"
                        + " parameter must be provided and a valid GroupData"
                        + " object");
                }

                groupData.effectDatas = [];

                /* save group effects */
                for (let i = 0; i < this.main.effectHandler.usedEffects.length; i++)
                {
                    /* clarity */
                    let effectIndex = this.main.effectHandler.effectHandler.usedEffect[i];
                    let effectWrapper = this.main.effectHandler.effectHandler.array[effectIndex];

                    /* redundant safety check but well */
                    if(effectWrapper.position !== -1)
                    {
                        let save = new EffectData(effectIndex,
                            effectWrapper.position);

                        save.copyFrom(effectWrapper.effect);

                        groupData.effectDatas.push(save);
                    }
                }

                for (let i = 0; i < groupData.soundDatas.length; i++)
                {
                    this.removeSound(groupData.soundDatas[i]);
                }

                // groupData.volume = this.main.group.volume;
            }
        },

        /**
         *  @function setSampleVolume
         *      Sets the volume of currently given playing sample (works anyway)
         *      to given volume.
         *
         *  @param {MixEnergetik.SoundData} soundData
         *      The sound data from which we can get the sample and the volume.
         */
        setSampleVolume:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundData)
            {
                const AudioM = MixEnergetik.AudioManager;
                let sound = AudioM.soundWrappers[soundData.soundIndex].sound;

                sound.volume = soundData.volume;
            }
        },

        /**
         *  @function getSampleVolume
         *      Gets the current playing sample volume.
         *
         *  @param {number} soundIndex
         *      The soundIndex of the sound from which we want the volume.
         *
         *  @returns {number}
         *      0 if soundIndex does not correspond to any sound;
         *      the volume of the asked sample otherwise.
         */
        getSampleVolume:
        {
            value: function (soundIndex)
            {
                const AudioM = MixEnergetik.AudioManager;
                const sound = AudioM.soundWrappers[soundIndex].sound;

                if (sound)
                {
                    return sound.volume;
                }
                else
                {
                    return 0;
                }
            }
        },

        /**
         *  @function setGroupVolume
         *      Sets the volume of the whole group to given volume.
         *
         *  @param {number} volume
         *      As only one group plays at a time, here's the volume.
         */
        setGroupVolume:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(volume)
            {
                this.main.group.volume = volume;
            }
        },

        /**
         *  @function contains
         *      Checks if a sound is inside the group playing.
         *
         *  @param {number} soundIndex
         *      The index of the sound to check presence.
         *
         *  @returns {bool}
         *      true if sound is inside this.main.group
         *      false otherwise
         */
        contains:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundIndex)
            {
                MixEnergetik.Utilitary.Check.native(soundIndex, "number",
                    "MixEnergetik.AudioManager.contains()", "soundIndex");

                return this.main.contains(MixEnergetik.AudioManager.soundWrappers[soundIndex].sound);
            }
        },

        /**
         *  @function isEmpty
         *      Checks if the main audio group is empty.
         *
         *  @returns {bool}
         *      true if the group contains no sound
         *      false otherwise
         */
        isEmpty:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                return this.main.isEmpty();
            }
        },

        /**
         *  @function bounce
         *      Okay, real magic starts here.
         *
         *  @param {GroupItem[]} allSounds
         *      An array of array of sound. All sound in the same sub array are
         *      meant to play together.
         *
         *  https://developer.mozilla.org/fr/docs/Web/API/AudioContext/createMediaStreamDestination
         */
        bounce:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(allSounds)
            {
                /* Guard */
                if (!allSounds)
                {
                    return;
                }

                let groupWrapper = new MixEnergetik.GroupWrapper();
                /* Encode the sound retrieved from workaround into a proper WAV
                format (TODO: for now). */
                let encoder = new MixEnergetik.Utilitary.WavAudioEncoder(Pizzicato.context.sampleRate,
                    2);
                /* Create a ScriptProcessorNode with a bufferSize of 4096 and
                input / output channel set to 2.
                This allows us to retrieve processed audio data in the form
                of a buffer
                TODO FIXME NOTE: VERY IMPORTANT: This feature is deprecied,
                soon (but not yet) to be replaced by Audio Worklets (and it's
                really worklets not workers!) */
                let scriptNode = Pizzicato.context.createScriptProcessor(4096,
                    2, 2);

                /* Preemptive definitions */

                /*
                 *  @function processAudio
                 *      Process audio that lands in scriptNode, so that it get
                 *      encoded in proper wav format ('cause screw opus which
                 *      cannot be used anywhere).
                 *
                 *  @param {AudioProcessingEvent} event
                 *      The audio processing event containing the node datas.
                 */
                let processAudio = function processAudio(event)
                {
                    /* A sound buffer is https://developer.mozilla.org/en-US/docs/Web/API/AudioBuffer */
                    /* The input buffer is the group loaded beforehand */
                    let inputBuffer = event.inputBuffer;

                    /* The output buffer will be unused (may need to discharge
                    samples into a dummy destination) */
                    //  let outputBuffer = event.outputBuffer;

                    let buffer = [];
                    buffer[0] = inputBuffer.getChannelData(0).slice(0,
                        inputBuffer.length);
                    buffer[1] = inputBuffer.getChannelData(1).slice(0,
                        inputBuffer.length);

                    encoder.encode(buffer);
                };

                /*
                 *  @function bounceEnd
                 *      Called when the bounce is considered finished. This
                 *      means we can make the user "download" the result.
                 */
                let bounceEnd = function bounceEnd()
                {
                    let a = document.createElement('a');
                    // Make blob out of our blobs, and open it.
                    let blob = encoder.finish();
                    let url = window.URL.createObjectURL(blob);

                    /* first, cleanup */
                    groupWrapper.group.disconnect(scriptNode);

                    a.style.display = "none";
                    a.href = url;
                    a.download = "bounce_result.wav";
                    document.body.appendChild(a);
                    a.click();

                    setTimeout(function()
                    {
                        document.body.removeChild(a);
                        window.URL.revokeObjectURL(url);
                    }, 100);
                }

                /*
                 *  @function bounceContinue
                 *      Called at the end of a bar so that it can make the next
                 *      play.
                 */
                let i = 0;
                let bounceContinue = function()
                {
                    groupWrapper.clear();

                    if (i >= allSounds.length)
                    {
                        /* finito */
                        bounceEnd();
                        return;
                    }

                    for (let j = 0; j < allSounds[i].groupData.soundDatas.length; j++)
                    {
                        let soundWrapper = MixEnergetik.AudioManager.soundWrappers[allSounds[i].groupData.soundDatas[j].soundIndex];

                        /* load configuration */
                        allSounds[i].groupData.soundDatas[j].load();

                        groupWrapper.addSound(soundWrapper.sound);
                    }

                    i++;
                }

                scriptNode.onaudioprocess = processAudio;

                groupWrapper.event.on("end", bounceContinue);

                groupWrapper.group.connect(scriptNode);

                /* Actually bounceStart() here, but whatever */
                bounceContinue();
            }
        }
    });

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      Event manager for the AudioManager.
     *      Supported events are:
     *      - "play", when the group has started playing.
     *      - "end", when the group has stopped playing.
     */
    AudioManager.event = new MixEnergetik.Utilitary.Event();

    /**
     *  @function getCurrentTime
     *      Returns the current elapsed time of AudioManager.main.
     */
    AudioManager.getCurrentTime = function getCurrentTime()
    {
        return MixEnergetik.AudioManager.main.getCurrentTime();
    };

    /**
     *  @function retrieveSoundIndexByUrl
     *      Seeks in the SoundWrapper array index for the one corresponding to
     *      the url.
     *
     *  @param {string} url
     *      The url of the sound we seek.
     *
     *  @returns {number}
     *      The index of the SoundWrapper object if found.
     *      -1 otherwise
     */
    AudioManager.retrieveSoundIndexByUrl = function retrieveSoundIndexByUrl(url)
    {
        MixEnergetik.Utilitary.Check.native(url, "string",
            "MixEnergetik.AudioManager.retrieveSoundIndexByUrl()", "url");

        const soundWrappers = MixEnergetik.AudioManager.soundWrappers;

        for (let i = 0; i < soundWrappers.length; i++)
        {
            if (soundWrappers[i].path === url)
            {
                return i;
            }
        }

        return -1;
    };

    AudioManager.main.event.on("play", function()
    {
        AudioManager.event.trigger("play");
    });
    AudioManager.main.event.on("end", function()
    {
        AudioManager.event.trigger("end");
    });

    /**
     *  @property {AnalyserNode} analyser
     *      A Web Audio API analyser to work with the sound.
     */
    AudioManager.analyser = Pizzicato.context.createAnalyser();
    AudioManager.main.group.connect(AudioManager.analyser);

    /**
     *  @function getCurrentAudioData
     *      Get data linked to the currently audio playing audio.
     *
     *  @returns {object}
     *      An object with following property:
     *      {number} minimum
     *          The minimum value of the signal data.
     *      {number} maximum
     *          The maximum value of the signal data.
     *      {number} average
     *          The average value of the signal data.
     */
    AudioManager.getCurrentAudioData = function getCurrentAudioData()
    {
        const analyser = MixEnergetik.AudioManager.analyser;

        let audioInfo = {};
        audioInfo.minimum = Number.POSITIVE_INFINITY;
        audioInfo.maximum = 0;
        audioInfo.average = 0;

        let audioData = new Uint8Array(analyser.fftSize);
        analyser.getByteTimeDomainData(audioData);

        for (let i = 0; i < audioData.length; i++)
        {
            let a = Math.abs(audioData[i] - 128);

            audioInfo.average += a;

            audioInfo.minimum = Math.min(audioInfo.minimum, a);
            audioInfo.maximum = Math.max(audioInfo.maximum, a);
        }

        audioInfo.average /= audioData.length;

        return audioInfo;
    };

    return AudioManager;
})();

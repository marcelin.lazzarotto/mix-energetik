/*
 *  File:   SoundWrapper.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.SoundWrapper
 *      Wrapper for Pizzicato.Sound object, so that we also keep track of
 *      effects and can easily manipulate them and save/load them.
 *
 *  @param {Pizzicato.Sound} sound
 *      The basic pizzicato sound we need to wrap around.
 *  @param {string} path
 *      The string of the path from where the sample can be found.
 */
MixEnergetik.SoundWrapper = (function()
{
    SoundWrapper = function SoundWrapper(sound, path)
    {
        /* Declaration */
        this.sound = null;
        this.effectHandler = new MixEnergetik.EffectHandler();

        if (!sound || !(sound instanceof Pizzicato.Sound))
        {
            throw new Error("[SoundWrapper()] The sound parameter should be"
                + " provided and a Pizzicato.Sound object");
        }

        if ((path === undefined)
            || (path === null)
            || (typeof path !== "string")
            || (path === ""))
        {
            throw new Error("[SoundWrapper()] The parameter path should be"
                + " provided and a correct string");
        }
        /* TODO Better string check for a path due to us reusing the thing with
         * save load eventually, to avoid malice. */

        /* Construction */
        this.sound = sound;
        this.path = path;
    }; /* constructor MixEnergetik.SoundWrapper */

    return SoundWrapper;
})();

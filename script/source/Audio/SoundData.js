/*
 *  File:   SoundData.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.SoundData
 *      Create a wrapper for sounds so that we keep data depending on contexts.
 *
 *  @param {integer} soundIndex
 *      Index of the sound linked with this data (easier save/load, at many
 *      levels).
 */
MixEnergetik.SoundData = (function()
{
    let SoundData = function SoundData(soundIndex)
    {
        if ((soundIndex === undefined)
            || (soundIndex === null)
            || (soundIndex < 0)
            || (soundIndex >= MixEnergetik.AudioManager.soundWrappers.length))
        {
            throw new Error("[SoundData()] soundIndex parameter must be provided"
                + " and a valid index.");
        }

        this.soundIndex = soundIndex;
        this.volume = 1; /* Range 0..1 */
        this.usedEffects = []; /* MixEnergetik.EffectData */
    }; /* MixEnergetik.SoundData */

    /**
     *  @function save
     *      Save the volume and effect in memory. Should be called when the
     *      sound playing switches context (e.g. when we stop playing).
     */
    SoundData.prototype.save = function save()
    {
        /* clarity */
        let soundWrapper = MixEnergetik.AudioManager.soundWrappers[this.soundIndex];

        this.volume = soundWrapper.sound.volume;

        /* Save USED effects configuration */
        for (let i = 0; i < soundWrapper.effectHandler.usedEffects.length; i++)
        {
            /* clarity */
            let effectIndex = soundWrapper.effectHandler.usedEffects[i];
            let effectWrapper = soundWrapper.array[effectIndex];

            /* redundant safety check but well */
            if (effectWrapper.position !== -1)
            {
                let save = new EffectData(effectIndex,
                    effectWrapper.position);

                save.copyFrom(effectWrapper.effect);

                this.usedEffects.push(save);
            }
        }
    };

    /**
     *  @function load
     *      Load the saved volume and effects. Should be called when the sound
     *      playing switches context back (e.g. when we will start playing).
     *      Modifies soundWrapper, so I hope you saved beforehand.
     */
    SoundData.prototype.load = function load()
    {
        /* clarity */
        let soundWrapper = MixEnergetik.AudioManager.soundWrappers[this.soundIndex];
        let effectHandler = soundWrapper.effectHandler;

        soundWrapper.sound.volume = this.volume;
        effectHandler.usedEffects.length = this.usedEffects.length;

        for (let i = 0; i < this.usedEffects.length; i++)
        {
            /* clarity */
            let savedEffect = this.usedEffects[i]; /* i === .position also */
            let effectIndex = savedEffect.index;
            effectHandler.usedEffects[savedEffect.position] = effectIndex;

            savedEffect.copyTo(effectHandler.array[effectIndex].effect);
        }
    };

    /**
     *  @function clone
     *      Creates a new SoundData object with same parameters but dissociated
     *      memory.
     *
     *  @returns {MixEnergetik.SoundData}
     *      A newly created SoundData object based on our object.
     */
    SoundData.prototype.clone = function clone()
    {
        let clone = new MixEnergetik.SoundData(this.soundIndex);

        clone.volume = this.volume;

        for (let i = 0; i < this.usedEffects.length; i++)
        {
            let savedEffectData = this.usedEffects[i];

            let cloneEffect = savedEffectData.clone();

            clone.usedEffects.push(cloneEffect);
        }

        return clone;
    };

    return SoundData;
})();

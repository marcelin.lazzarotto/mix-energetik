/*
 *  File:   EffectWrapper.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.EffectWrapper
 *      Wrapper for Pizzicato.Effect sub objects so that we contain additional
 *      information.
 *      Just here to contain information, to be used by a SoundWrapper object.
 */
MixEnergetik.EffectWrapper = (function()
{
    let EffectWrapper = function EffectWrapper()
    {
        /**
        *  @property {Pizzicato.Effect} pzEffect
        *      The base effect we wrap around.
        */
        this.effect = null;
        /**
        *  @property {integer} position
        *      The position in the side chain of effect.
        */
        this.position = -1;
    }; /* constructor MixEnergetik.EffectWrapper */

    return EffectWrapper;
})();

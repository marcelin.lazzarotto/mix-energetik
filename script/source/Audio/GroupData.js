/*
 *  File:   GroupData.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-02-08
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.GroupData
 *      Contains information about several SoundData objects. We are keeping
 *      data depending on context.
 *      A GroupData object also have its own EffectData array and volume.
 */
MixEnergetik.GroupData = (function()
{
    let GroupData = function GroupData()
    {
        /**
         *  @property {SoundData[]} soundDatas
         *      Array of SoundData objects, clones of soundData contained in
         *      SampleItem objects, themselves added to this group.
         */
        this.soundDatas = [];

        /**
         *  @property {EffectData[]} effectDatas
         *      Saved data of the effects of the group. Saved when stopping a
         *      play and loaded when starting.
         */
        this.effectDatas = [];

        /**
         *  @property {float} volume
         *      Volume of the group, in range 0 to 1. Is saved same time as the
         *      effects (TODO?).
         */
        this.volume = 1;
    }; /* MixEnergetik.GroupData */

    /**
     *  @function indexOfSoundData
     *      Returns the index of the given sound data.
     *      Since we base ourselves on sound identifier due to cloned items,
     *      this function is O(n).
     *
     *  @param {integer} soundIndex
     *      The index of the Pizzicato.Sound object from which the seeked
     *      soundData is derived.
     *
     *  @returns {integer}
     *      -1 if the item could not be found.
     *      An integer corresponding to the index inside the soundDatas array
     *      of the GroupData object this function was called on.
     */
    GroupData.prototype.indexOfSoundData = function indexOfSoundData(soundIndex)
    {
        if ((soundIndex === null)
            || (soundIndex === undefined)
            || (typeof soundIndex !== "number")
            || (soundIndex < 0)
            || (soundIndex >= MixEnergetik.AudioManager.soundWrappers.length))
        {
            throw new Error("[GroupData.indexOfSoundData()] soundIndex"
                + " parameter must be provided and a valid integer");
        }

        for (let i = 0; i < this.soundDatas.length; i++)
        {
            if(this.soundDatas[i].soundIndex === soundIndex)
            {
                return i;
            }
        }

        return -1;
    };

    /**
     *  @function addSoundData
     *      Adds the given soundData to the list of soundDatas array.
     *      Nothing happens if the soundData object is already present.
     *
     *  @param {MixEnergetik.SoundData} soundData
     *      The SoundData object to add to the soundDatas array.
     *      The object itself is added, cloning must appear beforehand.
     */
    GroupData.prototype.addSoundData = function addSoundData(soundData)
    {
        if ((soundData === null) || (soundData === undefined)
            || !(soundData instanceof MixEnergetik.SoundData))
        {
            throw new Error("[GroupData.addSoundData()] soundData parameter"
                + " must be provided and a valid MixEnergetik.SoundData"
                + " object");
        }

        if(this.indexOfSoundData(soundData.soundIndex) > -1)
        {
            console.debug("[GroupData.addSoundData()] given soundData is"
                + " already present");
            return;
        }

        this.soundDatas.push(soundData);
    };

    /**
     *  @function removeSoundData
     *      Remove a SoundData object from soundDatas array.
     *      Nothing happens if the item is not present.
     *
     *  @param {integer} soundIndex
     *      The index of the soundWrapper, from which our soundData is based.
     *
     *  @throws {Error}
     *      If soundIndex is not valid.
     */
    GroupData.prototype.removeSoundData = function removeSoundData(soundIndex)
    {
        if ((soundIndex === undefined)
            || (soundIndex === null)
            || (typeof soundIndex !== "number")
            || (soundIndex < 0)
            || (soundIndex >= MixEnergetik.AudioManager.soundWrappers.length))
        {
            throw new Error("[GroupData.removeSoundData()] The parameter"
                + " soundIndex must be provided and a valid index.");
        }

        let dataIndex = this.indexOf(soundIndex);

        if(dataIndex === -1)
        {
            console.debug("[GroupData.removeSoundData()] the corresponding"
                + " SoundData is not present, and thus can't be removed.");
        }
        else
        {
            this.soundDatas.splice(dataIndex, 1);
        }
    };

    /**
     *  @function save
     *      Save the volume and effect in memory. Should be called when the
     *      sound playing switches context (e.g. when we stop playing).
     */
    GroupData.prototype.save = function save()
    {
        /* TODO safety check (also changes how AudioManager handles groups) */
        /* if (AudioManager.group !=== this) return; */

        /* clarity */
        let globalEffectHandler = MixEnergetik.AudioManager.main.effectHandler;

        this.effectDatas.length = globalEffectHandler.usedEffects.length;

        /* Save effects */
        for (let i = 0; i < globalEffectHandler.usedEffects.length; i++)
        {
            /* clarity */
            let effectIndex = globalEffectHandler.usedEffects[i];
            let effectWrapper = globalEffectHandler.array[effectIndex];

            /* redundant safety check but well */
            if (effectWrapper.position !== -1)
            {
                let save = new EffectData(effectIndex,
                    effectWrapper.position);

                save.copyFrom(effectWrapper.effect);

                this.effectDatas[i] = save;
            }
        }

        /* Save each sounds */
        for (let i = 0; i < this.soundDatas.length; i++)
        {
            this.soundDatas[i].save();
        };
    };

    /**
     *  @function load
     *      Load the saved volume and effects. Should be called when the sound
     *      playing switches context back (e.g. when we will start playing).
     *      Modifies soundWrapper, so I hope you saved beforehand.
     */
    GroupData.prototype.load = function load()
    {
        /* Clarity */
        let globalEffectHandler = MixEnergetik.AudioManager.main.effectHandler;

        /* TODO Load volume */
        // MixEnergetik.AudioManager.main.volume =

        /* load group effects */
        for (let i = 0; i < this.effectDatas.length; i++)
        {
            /* clarity */
            let savedEffect = this.effectDatas[i]; /* i === .position also */
            globalEffectHandler.usedEffects[savedEffect.position] = savedEffect.index;

            savedEffect.copyTo(globalEffectHandler.array[savedEffect.index].effect);
            // TODO set position
        }

        /* Load each sound data */
        for (let i = 0; i < this.soundDatas.length; i++)
        {
            this.soundDatas[i].load();
        }
    };

    /**
     *  @function copyTo
     *      Copies a group data into another one.
     *      Act like clone, except that the destination is already given.
     *      Careful, as this will override the destination.
     *
     *  @param {MixEnergetik.GroupData} destination
     *      The destination to copy ourselves into.
     *
     *  @throws {Error}
     *      If destination is invalid.
     */
    GroupData.prototype.copyTo = function copyTo(destination)
    {
        if ((destination === undefined)
            || (destination === null)
            || !(destination instanceof MixEnergetik.GroupData))
        {
            throw new Error("[GroupData.copyTo()] the destination parameter"
                + " must be provided and a valid GroupData object");
        }

        /* Reset if not reseted */
        destination.soundDatas = [];
        destination.effectDatas = [];
        destination.volume = this.volume;

        /* Copy sounds */
        for (let i = 0; i < this.soundDatas.length; i++)
        {
            destination.addSoundData(this.soundDatas[i].clone());
        }

        /* Copy effects */
        for (let i = 0; i < this.effectDatas.length; i++)
        {
            /* clarity */
            let savedEffect = this.effectDatas[i]; /* i === .position also */
            let copied = new MixEnergetik.EffectData(savedEffect.index,
                savedEffect.position);

            savedEffect.copyTo(copied);

            /* TODO add effect function maybe? */
            destination.effectDatas.push(copied);
        }
    };

    /**
    *  @function clone
    *      Creates a new GroupData object with same parameters but dissociated
    *      memory.
    *
    *  @returns {MixEnergetik.GroupData}
    *      A newly created GroupData object based on our object.
    */
    GroupData.prototype.clone = function clone()
    {
        let clone = new GroupData();

        this.copyTo(clone);

        return clone;
    };

    return GroupData;
})();

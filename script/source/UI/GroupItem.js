/*
 *  File:   GroupItem.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @constructor MixEnergetik.UI.GroupItem
 *      A group item represent either a preset (right) or a sequence inside
 *      the sequencer.
 *      Group are built using clones of SoundData from SampleItem objects. This
 *      both helps with saving things (TODO) and having several configuration
 *      for the same sound, depending on the context (=== the group in which
 *      the sample plays).
 */
MixEnergetik.UI.GroupItem = (function()
{
    let GroupItem = function GroupItem()
    {
        /**
         *  @property {MixEnergetik.GroupData[]} groupData
         *      Contains all the group information, as well as routine for
         *      cloning, saving and loading.
         */
        this.groupData = new MixEnergetik.GroupData();

        /**
         *  @property {MixEnergetik.ImportData} importData
         *      Data following trades in regions. Only valable if the GroupItem
         *      object represents a region.
         */
        this.importData = null;

        /**
         *  @property {object} specialization
         *      OVERMEH. Used for... speciality in several pages.
         *
         */
        this.specialization = {};
        /*
         *  Construction
         */

        /* Call super() */
        MixEnergetik.UI.AudioItem.call(this);
        /* now we should have the things set up */

        /* Make it so by default it has a silence sound */
        /* Temporary workaround */
        /* Temporary my ass */
        this.addSound(MixEnergetik.AudioManager.silence);
    }; /* MixEnergetik.UI.GroupItem */

    /*
     *  Clean way to update prototype.
     */
    GroupItem.prototype = Object.create(MixEnergetik.UI.AudioItem.prototype,
    {
        /**
         *  @function play
         *      Function called when the user main-clicks on a PresetItem.
         *      Depending on the state of the application, the effects may vary
         *      a lot (see exhaustive specification in UI.js).
         */
        play:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                /* check */
                if (this.disabled)
                {
                    return;
                }

                if (MixEnergetik.GroupManager.indexOf(this) === -1)
                {
                    /* on top of it, if the clicked one (us) is not the
                     * same, we activate ourselves */
                    MixEnergetik.GroupManager.push(this);
                }
            }
        },

        /**
         *  @function stop
         *      Changes the group state.
         */
        stop:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                /* check */
                if (this.disabled)
                {
                    return;
                }

                if (MixEnergetik.GroupManager.indexOf(this) > -1)
                {
                    /* Else lets just remove ourselves. */
                    MixEnergetik.GroupManager.remove(this);
                }
            }
        },

        /**
         *  @function addSound
         *      Adds a new sound to this group item.
         *      This in practice creates a new sound data, clone of the given
         *      item's sound data. This will thus keep both effect configuration
         *      and volume configuration while adding this to the group; but
         *      can thereafter be modified without side effects.
         *      Nothing happens if the sound is already present.
         *
         *  @param {MixEnergetik.SoundData} soundData
         *      The sound data to add.
         *
         *  @throws {Error}
         *      If soundData is null or not MixEnergetik.SoundData object
         */
        addSound:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundData)
            {
                if((soundData === undefined) || (soundData === null)
                    || !(soundData instanceof MixEnergetik.SoundData))
                {
                    throw new Error("[GroupItem.addSound()] soundData"
                        + " parameter must be provided and a correct"
                        + " MixEnergetik.SoundData object");
                }


                if(this.indexOf(soundData.soundIndex) > -1)
                {
                    return;
                }

                let silenceIndex = MixEnergetik.AudioManager.silence.soundIndex;

                if (this.isEmpty() && soundData.soundIndex !== silenceIndex)
                {
                    /* if we are adding the first, we need to change the look of
                     * the group item */
                    this.source.classList.remove("empty");
                }

                let ownSoundData = soundData.clone(); /* Because duh! */
                this.groupData.soundDatas.push(ownSoundData);

                /* in case we are the currently running group, add it to the
                 * sounds played */
                if(MixEnergetik.GroupManager.getActive() === this)
                {
                    /* kind of a bypass... no? */
                    MixEnergetik.AudioManager.addSound(ownSoundData);
                    this.use(); /* Ugly but adds the added one to us */
                }
                // TODO else MixEnergetik.GroupManager.push(this);
            }
        },

        /**
         *  @function removeSound
         *      Remove the given item from the list of sound of this group.
         *      Nothing happens if the sound is not in the group.
         *
         *  @param {integer} soundIndex
         *      The index hold by the SoundData object that we want to remove.
         *
         *  @throws {Error}
         *      If soundIndex is undefined/null or not a proper sound index.
         */
        removeSound:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundIndex)
            {
                if((soundIndex === undefined) || (soundIndex === null)
                    || (typeof soundIndex !== "number"))
                {
                    throw new Error("[GroupItem.removeSound()] soundIndex"
                        + " parameter must be provided and a correct index");
                }

                let toRemoveIndex = this.indexOf(soundIndex);

                if (toRemoveIndex === -1)
                {
                    return;
                }

                let soundData = this.groupData.soundDatas.splice(toRemoveIndex, 1);

                if (!soundData)
                {
                    return;
                }

                /* in case we are the currently running preset, remove the sound
                 * from the sounds played */
                if(MixEnergetik.GroupManager.getActive() === this)
                {
                    MixEnergetik.AudioManager.removeSound(soundData[0]);
                }

                if (this.isEmpty())
                {
                    /* if we removed the last, we need to change the look of the
                     * preset */
                    this.source.classList.add("empty");
                }
            }
        },

        /**
         *  @function indexOf
         *      Returns the index of the sound inside the group's array,
         *      identified by the given soundIndex.
         *
         *  @param {integer} soundIndex
         *      The sound index to compare to.
         *
         *  @returns {integer}
         *      The index of the soundData if a corresponding soundData to the
         *      soundIndex is present;
         *      -1 if no corresponding sound could be found.
         */
        indexOf:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(soundIndex)
            {
                /* Since we got clones everywhere, we need to check in O(n) if
                 * we got things */
                for (let i = 0; i < this.groupData.soundDatas.length; i++)
                {
                    if(this.groupData.soundDatas[i].soundIndex === soundIndex)
                    {
                        return i;
                    }
                }

                return -1;
            }
        },

        /**
         *  @function clear
         *      Removes all sounds from the group.
         *
         *  @param {string} tradeAction
         *      Action for the trade samples. Values are:
         *      "TRADE_RESET" (default) where import samples are reset;
         *      "TRADE_UNCHANGED" where import samples are not modified.
         */
        clear:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(tradeAction)
            {
                tradeAction = tradeAction || "TRADE_RESET";

                for (let i = this.groupData.soundDatas.length - 1; i >= 0; i--)
                {
                    this.removeSound(this.groupData.soundDatas[i].soundIndex);
                }

                if (MixEnergetik.TradeManager)
                {
                    if (MixEnergetik.TradeManager.regionItem === this)
                    {
                        MixEnergetik.TradeManager.stop();
                    }
                }

                if (MixEnergetik.UI.MapView && this.importData
                    && (tradeAction === "TRADE_RESET"))
                {
                    this.importData.clear();
                }

                if ((this.specialization.origin !== undefined)
                    && (this.specialization.origin !== null))
                {
                    this.specialization.origin.container.classList.remove("origin");
                    delete this.specialization.origin;
                }

                this.addSound(MixEnergetik.AudioManager.silence);
            }
        },

        /**
         *  @function use
         *      Called when returning from a solo play, to change user response.
         */
        use:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                /* Call super() */
                MixEnergetik.UI.AudioItem.prototype.use.call(this);

                const AudioM = MixEnergetik.AudioManager;
                const TradeV = MixEnergetik.UI.TradeView;
                const SmpLstV = MixEnergetik.UI.SampleListView;
                const GrpDtV = MixEnergetik.UI.GroupDataView;
                const ChargeV = MixEnergetik.UI.ChargeView;

                let soundIndices = [];

                for (let i = 0; i < this.groupData.soundDatas.length; i++)
                {
                    soundIndices.push(this.groupData.soundDatas[i].soundIndex);
                }

                for (let i = 0; i < soundIndices.length; i++)
                {
                    const index = soundIndices[i];
                    let sampleItem = null;

                    /* Seek in SampleListView and in Accumulator */
                    if (SmpLstV)
                    {
                        sampleItem = SmpLstV.sampleItems[index];
                        if (sampleItem)
                        {
                            sampleItem.setVolume(AudioM.getSampleVolume(index));
                            sampleItem.use();
                        }
                    }

                    if (ChargeV)
                    {
                        sampleItem = ChargeV.retrieveSampleItem(index);
                        if (sampleItem)
                        {
                            sampleItem.setVolume(AudioM.getSampleVolume(index));
                            sampleItem.use();
                        }
                    }

                    if (GrpDtV)
                    {
                        sampleItem = GrpDtV.elementItems[index];
                        if (sampleItem)
                        {
                            sampleItem.setVolume(AudioM.getSampleVolume(index));
                            sampleItem.use();
                        }
                    }
                }

                if (TradeV && this.importData)
                {
                    let soundIndices = [];

                    for (let soundIndex in this.importData.soundDatas)
                    {
                        soundIndices.push(+soundIndex);
                    }

                    TradeV.use(soundIndices);
                }

                if ((this.specialization.origin !== undefined)
                    && (this.specialization.origin !== null))
                {
                    this.specialization.origin.container.classList.add("origin");
                }
            }
        },

        /**
         *  @function unuse
         *      Called when something else plays in solo, for user response.
         */
        unuse:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                /* Call super() */
                MixEnergetik.UI.AudioItem.prototype.unuse.call(this);

                const TradeV = MixEnergetik.UI.TradeView;
                const SmpLstV = MixEnergetik.UI.SampleListView;
                const ChargeV = MixEnergetik.UI.ChargeView;
                const GrpDtV = MixEnergetik.UI.GroupDataView;

                let soundIndices = [];

                for (let i = 0; i < this.groupData.soundDatas.length; i++)
                {
                    soundIndices.push(this.groupData.soundDatas[i].soundIndex);
                }

                for (let i = 0; i < soundIndices.length; i++)
                {
                    const index = soundIndices[i]
                    let sampleItem = null;

                    /* Seek in SampleListView and in Accumulator */
                    if (SmpLstV)
                    {
                        sampleItem = SmpLstV.sampleItems[index];
                        if (sampleItem)
                        {
                            sampleItem.unuse();
                        }
                    }

                    if (ChargeV)
                    {
                        sampleItem = ChargeV.retrieveSampleItem(index);
                        if (sampleItem)
                        {
                            sampleItem.unuse();
                        }
                    }

                    if (GrpDtV)
                    {
                        sampleItem = GrpDtV.elementItems[index];
                        if (sampleItem)
                        {
                            sampleItem.unuse();
                        }
                    }
                }

                if (TradeV)
                {
                    TradeV.unuse();
                }

                if ((this.specialization.origin !== undefined)
                    && (this.specialization.origin !== null))
                {
                    this.specialization.origin.container.classList.remove("origin");
                }
            }
        },

        /**
         *  @function setStyle
         *      Called when we need to change the style in special cases.
         *      Do nothing if the specified case is not detected (still prints a
         *      log though (you never know)).
         *
         *  @param {string} styleId
         *      The string equivalent to enumeration to identify the type to
         *      give.
         *
         *  @throws {Error}
         *      If styleId is undefined or null or not a string.
         */
        setStyle:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(styleId)
            {
                MixEnergetik.Utilitary.Check.native(styleId, "string",
                    "MixEnergetik.GroupItem.setStyle()", "styleId");

                switch (styleId)
                {
                    case "waiting":
                        /* A group is in a waiting list */
                        this.container.classList.add("waiting");
                        break;
                    case "standard":
                        this.container.classList.remove("waiting");
                        break;
                    default:
                        console.log(styleId + " was not recognized");
                        return;
                }
            }
        },

        /**
         *  @function setVolume
         *      The the volume for the given audio item.
         *      It can be the volume of the sample if the item concerns only
         *      one sample or the master volume of a group.
         *
         *  @param {float} volume
         *      Volume value between 0 and 1. 1 equals 100% and 0 is mute.
         *      The sound cannot go beyond 0 and 1.
         *      Nothing happens if volume is not valid (i.e. null, undefined or
         *      not a number).
         */
        setVolume:
        {
            configurable: true,
            enumerable: true,
            writable: false,

            value: function(volume)
            {
                if ((volume === null) || (volume === undefined)
                    || (typeof volume !== "number"))
                {
                    return;
                }

                let boundVolume = 1.0;

                if (volume < 0.0)
                {
                    boundVolume = 0.0;
                }
                else if (volume > 1.0)
                {
                    boundVolume = 1.0;
                }
                else
                {
                    boundVolume = volume;
                }

                this.groupData.volume = boundVolume;

                if (this.on)
                {
                    /* change sound volume if this sound is playing */
                    MixEnergetik.AudioManager.setGroupVolume(this.groupData.volume);
                }
            }
        },

        /**
         *  @function dropTargetOn
         *      Activate user feedback on drop target.
         */
        dropTargetOn:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.source.classList.add("dropTarget");
            }
        },

        /**
         *  @function dropTargetOff
         *      Remove user feedback on drop target.
         */
        dropTargetOff:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.source.classList.remove("dropTarget");
            }
        },

        /**
         *  @function setContainer
         *      Function to set the container for this group item, since we will
         *      use either a provided svg polygon (TODO for now, will prob.
         *      change) or an api-created standard div.
         *
         *  @param {Element} container
         *      The element to use as a container for the item. Should be an
         *      SVG polygon or an HTML div, but can be any valid element in
         *      practice.
         *  @param {string} control
         *      Indicates if the item is controlable (volume...) or not.
         *      Possible values are: "CONTROL_ENABLE" (default) or
         *      "CONTROL_DISABLE".
         *  @param {string} drag
         *      Indicates if the item is draggable or not.
         *      Possible values are: "DRAG_ENABLE" (default) or "DRAG_DISABLE".
         *
         *  @override {MixEnergetik.UI.AudioItem.prototype.setContainer}
         */
        setContainer:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(container, control, drag)
            {
                /* Still call super */
                MixEnergetik.UI.AudioItem.prototype.setContainer.call(this, container, control, drag);

                let self = this;

                let clickCallback = function clickCallback(event)
                {
                    const AppCtrlM = MixEnergetik.ApplicationControlManager;
                    const SoloM = MixEnergetik.SoloManager;

                    if (AppCtrlM.soloModeOn)
                    {
                        let otherItem = SoloM.soloItem;

                        if (otherItem !== null)
                        {
                            otherItem.stopSolo();
                        }
                        if (otherItem !== self)
                        {
                            self.playSolo();
                        }
                    }
                    else
                    {
                        if (MixEnergetik.GroupManager.indexOf(self) === -1)
                        {
                            /*  We have to play, but first we need to deactivate
                             *  things that may be currently playing.
                             */
                            if (MixEnergetik.SequencerManager.isActive())
                            {
                                /* TODO click on group in sequencer !!!!! */
                                /* if the mixer is activated, deactivate it completelly in favor
                                * of the preset */
                                MixEnergetik.SequencerManager.deactivate();
                            }
                            else if ((MixEnergetik.GroupManager.isActive())
                                && (MixEnergetik.GroupManager.getActive() !== this) /* Dumb safety */
                                && (MixEnergetik.GroupManager.getActive().isEmpty()
                                    || self.isEmpty()))
                            {
                                MixEnergetik.GroupManager.deactivate();
                            }

                            self.play();
                        }
                        else /* if (self.on) */
                        {
                            self.stop();
                        }
                    }
                };

                /* Add the correct class to html element */
                this.source.classList.add("empty");

                this.source.addEventListener("click", clickCallback, false);

                /* Drag over is fired on drop zone element. */
                this.source.addEventListener("dragover", function(event)
                {
                    MixEnergetik.DragAndDropManager.targetItem = self;
                }, false);
            }
        }
    }); /* Update GroupItem prototype */

    /**
     *  @function isEmpty
     *      Checks if the GroupItem object contains no SoundData object or
     *      silence.
     *
     *  @returns {boolean}
     *      true if there is no SoundData object or if the only SoundData object
     *      contained in .groupData corresponds to the silent sample.
     *      false otherwise
     */
    GroupItem.prototype.isEmpty = function isEmpty()
    {
        let silenceIndex = MixEnergetik.AudioManager.silence.soundIndex;

        return ((this.groupData.soundDatas.length === 0)
            || ((this.groupData.soundDatas.length === 1)
                && (this.indexOf(silenceIndex) >= 0)));
    };

    /**
     *  @function addGroup
     *      Add a complete group to this group item (=== copy). Does not work
     *      if this group is not empty.
     *
     *  @param {MixEnergetik.GroupData} groupData
     *      The group to copy.
     */
    GroupItem.prototype.addGroup = function addGroup(groupData)
    {
        MixEnergetik.Utilitary.Check.object(groupData, MixEnergetik.GroupData,
            "MixEnergetik.UI.GroupItem.addGroup", "groupData",
            "MixEnergetik.GroupData");

        if (!this.isEmpty())
        {
            return;
        }

        for (let i = 0; i < groupData.soundDatas.length; i++)
        {
            this.addSound(groupData.soundDatas[i]);
        }

        this.setVolume(groupData.volume);

        for (let i = 0; i < groupData.effectDatas.length; i++)
        {
            let otherEffectData = groupData.effectDatas[i];

            let thisEffectData = new EffectData(otherEffectData.index,
                otherEffectData.position)

            thisEffectData.copyFrom(otherEffectData);

            this.groupData.effectDatas.push(thisEffectData);
        }
    };

    /**
     *  @function addImportSample
     *      Add a trade sample to the group item.
     *      Do nothing if the trade capacity is already maxed out.
     *
     *  @param {MixEnergetik.SoundData} soundData
     *      The trade sample to add.
     */
    GroupItem.prototype.addImportSample = function addImportSample(soundData)
    {
        MixEnergetik.Utilitary.Check.object(soundData, MixEnergetik.SoundData,
            "MixEnergetik.UI.GroupItem.addImportSample()", "soundData",
            "MixEnergetik.SoundData");

        if (this.importData === null)
        {
            console.log("[MixEnergetik.UI.GroupItem.addImportSample()] Can't"
                + " import on a group with no import data.");
            return;
        }

        const regionId = this.specialization.regionId;

        if (!regionId)
        {
            console.log("[MixEnergetik.UI.GroupItem.addImportSample()] The"
                + " group is not a region.");
            return;
        }

        const regionData = MixEnergetik.RegionManager.regions[regionId];

        if (regionData.getTradeType() !== "TYPE_IMPORT")
        {
            console.log("[MixEnergetik.UI.GroupItem.addImportSample()] The"
                + " region is not importing.");
            return;
        }

        const importLevel = MixEnergetik.TradeManager.getImportLevel(this);

        if (importLevel >= regionData.getTradeLevel())
        {
            console.log("[MixEnergetik.UI.GroupItem.addImportSample()] The"
                + " region is full.");
            return;
        }

        this.importData.addSound(soundData.clone());

        MixEnergetik.UI.TradeView.updateImportLevel();

        this.addSound(soundData);
    };

    /**
     *  @function removeImportSample
     *      Remove a trade sample from the group item.
     *      Do nothing if the trade sample does not belong to the GroupItem
     *      object.
     *
     *  @param {number} soundIndex
     *      The trade sample index to remove.
     */
    GroupItem.prototype.removeImportSample = function removeImportSample(soundIndex)
    {
        MixEnergetik.Utilitary.Check.native(soundIndex, "number",
            "MixEnergetik.UI.GroupItem.removeImportSample()", "soundIndex");

        if (!this.importData)
        {
            return;
        }

        this.importData.removeSound(soundIndex);
        this.removeSound(soundIndex);

        if (MixEnergetik.TradeManager.regionItem === this)
        {
            MixEnergetik.UI.TradeView.updateImportLevel();
        }
    };

    /* Repair the inherited constructor */
    GroupItem.prototype.constructor = GroupItem;

    /**
     *  @function inputDown
     *      Wrapper around the fact that an input corresponding to the sample
     *      is down. Mostly used with mousedown, but can be with key down.
     */
    GroupItem.prototype.inputDown = function inputDown()
    {
        /* On main click, either:
         * - execute standard play (vary according to application's
         *   state)
         */
        if (MixEnergetik.SoloManager.soloItem === null)
        {
            if (!this.on)
            {
                /* This is a bad thing and should have a fuckton of things to
                 * check */
                this.use();

                MixEnergetik.AudioManager.addGroup(this.groupData);
            }
        }
    };

    /**
     *  @function inputUp
     *      Wrapper around the fact that an input corresponding to the sample
     *      is up. Mostly used with mouseup, but can be with key down.
     */
    GroupItem.prototype.inputUp = function inputUp()
    {
        /* On main click, either:
         * - execute standard stop (vary according to application's state)
         */
        if (MixEnergetik.SoloManager.soloItem === null)
        {
            if (this.on)
            {
                /* We have to not remove any sound that is preset in another
                 * thing playing. */
                let usedSoundIndices = [];

                /* Check groups */
                {
                    const presets = MixEnergetik.UI.PresetPanelView.presets;
                    for (let i = 0; i < presets.length; i++)
                    {
                        let preset = presets[i];

                        if (preset.on
                            && (preset !== this))
                        {
                            let soundDatas = preset.groupData.soundDatas;

                            for (let j = 0; j < soundDatas.length; j++)
                            {
                                let index = soundDatas[j].soundIndex;

                                if (usedSoundIndices.indexOf(index) === -1)
                                {
                                    usedSoundIndices.push(index);
                                }
                            }
                        }
                    }
                }

                /* Check sequence */
                {
                    {
                        const sequences = MixEnergetik.UI.SequencerView.sequences;
                        for (let i = 0; i < sequences.length; i++)
                        {
                            let sequence = sequences[i];

                            if (sequence.on
                                && (sequence !== this))
                            {
                                let soundDatas = sequence.groupData.soundDatas;

                                for (let j = 0; j < soundDatas.length; j++)
                                {
                                    let index = soundDatas[j].soundIndex;

                                    if (usedSoundIndices.indexOf(index) === -1)
                                    {
                                        usedSoundIndices.push(index);
                                    }
                                }
                            }
                        }
                    }
                }

                /* Actually remove unique sounds */
                {
                    const soundDatas = this.groupData.soundDatas;

                    for (let i = 0; i < soundDatas.length; i++)
                    {
                        let soundData = soundDatas[i];

                        if (usedSoundIndices.indexOf(soundData.soundIndex) === -1)
                        {
                            MixEnergetik.AudioManager.removeSound(soundData);
                        }
                    }
                }

                this.unuse();
            }
        }
    };

    /**
     *  @function muteBaseSound
     *      For map only, will mute the base sound if possible.
     */
    GroupItem.prototype.muteBaseSound = function muteBaseSound()
    {
        let regionBaseSound = this.specialization.regionBaseSound;

        if ((regionBaseSound === undefined)
            || (regionBaseSound === null))
        {
            return;
        }

        for (let i = 0; i < regionBaseSound.length; i++)
        {
            let soundDatas = this.groupData.soundDatas;

            for (let j = 0; j < soundDatas.length; j++)
            {
                if (soundDatas[j].soundIndex === regionBaseSound[i])
                {
                    soundDatas[j].volume = 0;

                    if (this.on)
                    {
                        /* In case sample is playing */
                        MixEnergetik.AudioManager.setSampleVolume(soundDatas[j]);
                    }
                }
            }
        }
    };

    /**
     *  @function unmuteBaseSound
     *      For map only, will unmute the base sound if possible.
     */
    GroupItem.prototype.unmuteBaseSound = function muteBaseSound()
    {
        let regionBaseSound = this.specialization.regionBaseSound;

        if ((regionBaseSound === undefined)
            || (regionBaseSound === null))
        {
            return;
        }

        for (let i = 0; i < regionBaseSound.length; i++)
        {
            let soundDatas = this.groupData.soundDatas;

            for (let j = 0; j < soundDatas.length; j++)
            {
                if (soundDatas[j].soundIndex === regionBaseSound[i])
                {
                    soundDatas[j].volume = 1;

                    if (this.on)
                    {
                        /* In case sample is playing */
                        MixEnergetik.AudioManager.setSampleVolume(soundDatas[j]);
                    }
                }
            }
        }
    };

    return GroupItem;
})();

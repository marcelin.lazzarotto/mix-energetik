/*
 *  File:   SampleItem.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";


/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @constructor MixEnergetik.UI.SampleItem
 *      Basic UI item to represent a sound. Actually a position on the map,
 *      but for now we stay on a small shitty thing.
 *
 *  @inheritFrom {MixEnergetik.UI.AudioItem}
 *
 *  @param {integer} soundIndex
 *      An index to a sound inside MixEnergetik.AudioManager.sounds array.
 *  @param {string} name
 *      A name to give to the sample, so that it is identified in the
 *      application.
 *  @param {string} nature
 *      The nature of the sample. Is used for specializing the interface.
 *      Possible values are:
 *      - "NATURE_UNSPECIFIED" (default) for default behavior
 *      - "NATURE_ELEMENT" for element sample, unconfigurable and undraggable.
 *      - "NATURE_EXPORT" for export sample, configurable and draggable.
 */
MixEnergetik.UI.SampleItem = (function()
{
    let SampleItem = function SampleItem(soundIndex, name, nature)
    {
        /*
         *  Declarations
         */
        /**
         *  @property {MixEnergetik.SoundData} soundData
         *      A sound object with additionnal data.
         */
        this.soundData = null;

        /**
         *  @property {string} name
         *      The name of the sample, so that we know.
         */
        this.name = name;

        /**
         *  @property {MixEnergetik.UI.BatteryItem <for now>} parent
         *      A parent, to be sure to remove only what's in the accumulator.
         */
        this.parent = null;

        /**
         *  @property {boolean} playLonger
         *      Keep track of the inputUp thing, for playing sample until the
         *      end.
         */
        this.playLonger = false;

        /**
         *  @property {MixEnergetik.UI.SampleItem} self
         *      Reference to this.
         */
        let self = this;

        /*
         *  Helpers
         */
        /*
         *  Used with mouse event functions.
         */
        const Button =
        {
            /* Main button pressed, usually the left button or the
             * un-initialized state */
            MAIN: 0,
            // /* Auxiliary button pressed, usually the wheel button or the
            //  * middle button (if present) */
            // AUXILIARY: 1,
            // /* Secondary button pressed, usually the right button */
            // SECONDARY: 2,
            // /* Fourth button, typically the Browser Back button */
            // FOURTH: 3,
            // /* Fifth button, typically the Browser Forward button */
            // FIFTH: 4,
        };

        /**
         *  @function mouseDownCallback
         *      Function to check which button is used to click on the sample.
         *      Then calls the proper function to deal with the desired
         *      reaction.
         */
        let mouseDownCallback = function mouseDownCallback(event)
        {
            /* Different event model thing... */
            event = event || window.event;
            switch (event.button)
            {
                case Button.MAIN:
                    self.inputDown();

                    break;
                default:
                    break;
            }
        };

        /**
         *  @function mouseUpCallback
         *      Function to check which button is used to click on the sample.
         *      Then calls the proper function to deal with the desired
         *      reaction.
         */
        let mouseUpCallback = function mouseUpCallback(event)
        {
            /* Different event model thing... */
            event = event || window.event;

            switch(event.button)
            {
                case Button.MAIN:
                    self.playLonger = true;
                    self.inputUp();

                    break;
                default:
                    break;
            }
        };


        /*
         *  Construction
         */

        nature = nature || "NATURE_UNSPECIFIED";

        if ((soundIndex === undefined)
            || (soundIndex === null)
            || (soundIndex < 0)
            || (soundIndex > MixEnergetik.AudioManager.soundWrappers.length))
        {
            throw new Error("[SampleItem()] soundIndex parameter must be"
                + " provided and a valid index.");
        }

        /* Call super() */
        MixEnergetik.UI.AudioItem.call(this);

        /* now we should have the things set up, set up the container */
        let container = document.createElement("div");
        switch (nature)
        {
            case "NATURE_ELEMENT":
                this.setContainer(container, "CONTROL_ENABLE", "DRAG_DISABLE");
                break;
            case "NATURE_EXPORT":
                this.setContainer(container, "CONTROL_ENABLE", "DRAG_ENABLE");
                break;
            case "NATURE_UNSPECIFIED":
            default:
                this.setContainer(container, "CONTROL_ENABLE", "DRAG_ENABLE");
                break;
        }

        this.soundData = new MixEnergetik.SoundData(soundIndex);

        /* Interactions */
        if (nature === "NATURE_ELEMENT")
        {
            this.source.addEventListener("click",
                function()
                {
                    const SoloM = MixEnergetik.SoloManager;

                    if (SoloM.soloItem === self)
                    {
                        SoloM.clear();
                    }
                    else
                    {
                        SoloM.setSoloItem(self);
                    }
                }, false);
        }
        else
        {
            this.source.addEventListener("mouseup", mouseUpCallback, false);
            this.source.addEventListener("mousedown", mouseDownCallback, false);
        }

        let nameNode = document.createElement("span");
        nameNode.innerHTML = name;
        this.control.container.insertAdjacentElement("afterbegin", nameNode);

        if (!MixEnergetik.UI.MapView)
        {
            /* In /wander, make the samples 50% volume by default */
            this.soundData.volume = 0.5;
        }
        else
        {
            /* In /base or /extraction, make export samples 75% volume by
             * default */
            if (nature === "NATURE_EXPORT")
            {
                this.soundData.volume = 0.75;
            }
        }

        this.control.event.on("extend", function()
        {
            self.control.volumeSlider.value = self.soundData.volume;
        });

        /* Add the correct class to html element */
        this.container.classList.add("sample");
    }; /* MixEnergetik.UI.SampleItem */

    /*
     *  Clean way to update prototype.
     */
    SampleItem.prototype = Object.create(MixEnergetik.UI.AudioItem.prototype,
    {
        /**
         *  @function play
         *      Function called when the user main-clicks on a SampleItem.
         *      Depending on the state of the application, the effects may vary
         *      a lot.
         *
         *  @inheritedFrom {MixEnergetik.UI.AudioItem}
         */
        play:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                /* check */
                if (this.disabled)
                {
                    return;
                }

                if (this.on)
                {
                    return;
                }

                if (MixEnergetik.GroupManager.isActive())
                {

                    // TODO
                    /* Add the preset to the list. */
                    /* Make so it is only temporary since adding is drag and
                     * drop */
                    let preset = MixEnergetik.GroupManager.getActive();

                    if (preset.indexOf(this.soundData.soundIndex) > -1)
                    {
                        console.log("[MixEnergetik.UI.SampleItem.play()]"
                            + " WTF?");
                        return;
                    }

                    MixEnergetik.SampleManager.add(this);
                }
                else if(MixEnergetik.SequencerManager.isActive())
                {
                    /* Same shit here, we allow for playing when the sequencer
                     * is playing, but only as temporary sample. */
                    let sequence = MixEnergetik.SequencerManager.getCurrent();

                    if (sequence.indexOf(this.soundData.soundIndex) > -1)
                    {
                        // Do nothing
                        return;
                    }

                    MixEnergetik.SampleManager.add(this);
                }
                else /* if (!MixEnergetik.GroupManager.isActive() && !MixEnergetik.SequencerManager.isActive()) */
                {
                    /* since nothing is running, we can go on */
                    MixEnergetik.SampleManager.add(this);
                }
            }
        },

        /**
         *  @function stop
         *      Stops the sample currently playing and reset previous state.
         *
         *  @inheritedFrom {MixEnergetik.UI.AudioItem}
         */
        stop:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                /* check */
                if (this.disabled)
                {
                    return;
                }

                if (!this.on)
                {
                    return;
                }

                if (MixEnergetik.GroupManager.isActive())
                {
                    /* add or remove sound from active preset */
                    let preset = MixEnergetik.GroupManager.getActive();
                    let soundIndex = this.soundData.soundIndex;

                    if (preset.indexOf(soundIndex) > -1)
                    {
                        if (preset.importData
                            && preset.importData.soundDatas[soundIndex])
                        {
                            preset.removeImportSample(soundIndex);
                        }
                        else
                        {
                            preset.removeSound(soundIndex);
                        }
                        this.unuse();
                    }
                    else if (MixEnergetik.AudioManager.contains(this.soundData.soundIndex))
                    {
                        MixEnergetik.SampleManager.remove(this);
                    }
                }
                else if(MixEnergetik.SequencerManager.isActive())
                {
                    /* Same shit here, we allow for stop when the sequencer
                     * is playing, but only as temporary sample. */

                    /* add or remove sound from active preset */
                    let soundIndex = this.soundData.soundIndex;

                    if (MixEnergetik.AudioManager.contains(this.soundData.soundIndex))
                    {
                        MixEnergetik.SampleManager.remove(this);
                    }
                }
                else /* if (!MixEnergetik.GroupManager.isActive() && !MixEnergetik.SequencerManager.isActive()) */
                {
                    /* since nothing is running, we can stop manually */
                    MixEnergetik.SampleManager.remove(this);
                }
            }
        },

        /**
         *  @function setVolume
         *      The the volume for the given audio item.
         *      It can be the volume of the sample if the item concerns only
         *      one sample or the master volume of a group.
         *
         *  @param {float} volume
         *      Volume value between 0 and 1. 1 equals 100% and 0 is mute.
         *      The sound cannot go beyond 0 and 1.
         *      Nothing happens if volume is not valid (i.e. null, undefined or
         *      not a number).
         *
         *  @warning
         *      Base prototype method that does nothing.
         */
        setVolume:
        {
            configurable: true,
            enumerable: true,
            writable: false,

            value: function(volume)
            {
                if ((volume === null) || (volume === undefined)
                    || (typeof volume !== "number"))
                {
                    return;
                }

                let boundVolume = 1.0;

                if (volume < 0.0)
                {
                    boundVolume = 0.0;
                }
                else if (volume > 1.0)
                {
                    boundVolume = 1.0;
                }
                else
                {
                    boundVolume = volume;
                }

                this.soundData.volume = boundVolume;
                this.control.volumeSlider.value = boundVolume;

                if (this.on)
                {
                    /* change sound volume if this sound is playing */
                    MixEnergetik.AudioManager.setSampleVolume(this.soundData);
                }
            }
        },
    }); /* MixEnergetik.UI.SampleItem update */

    /* repair the inherited constructor */
    SampleItem.prototype.constructor = SampleItem;

    /**
     *  @function inputDown
     *      Wrapper around the fact that an input corresponding to the sample
     *      is down. Mostly used with mousedown, but can be with key down.
     */
    SampleItem.prototype.inputDown = function inputDown()
    {
        /* On main click, either:
         * - execute standard play (vary according to application's
         *   state)
         */
        if (MixEnergetik.SoloManager.soloItem === null)
        {
            if (!this.on)
            {
                this.play();
            }
        }
    };

    /**
     *  @function inputUp
     *      Wrapper around the fact that an input corresponding to the sample
     *      is up. Mostly used with mouseup, but can be with key down.
     */
    SampleItem.prototype.inputUp = function inputUp()
    {
        /* On main click, either:
         * - execute standard stop (vary according to application's state)
         */
        if (MixEnergetik.SoloManager.soloItem === null)
        {
            if (this.on)
            {
                this.stop();
            }
        }
    };

    return SampleItem;
})();

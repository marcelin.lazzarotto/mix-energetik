/*
 *  File:   TagItem.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module TagItem
 *      The manager dealing with video filtering by tags.
 */
MixEnergetik.UI.TagItem = (function()
{
    /**
     *  @constructor TagItem
     *      An UI item to select a tag.
     *
     *  @param {string} name
     *      Name of the tag. Mandatory.
     *  @param {string} interaction
     *      Type of the interaction of this item.
     *      Values are "TAG_CLICKABLE" or "TAG_UNCLICKABLE".
     */
    let TagItem = function TagItem(name, interaction)
    {
        /* Declarations */
        /**
         *  @property {HTMLElement} container
         *      Container of the video.
         */
        this.container = null;
        /**
         *  @property {string} name
         *      The name of the video.
         */
        this.name = "";
        /**
         *  @property {boolean} selected
         */
        this.selected = false;

        let self = this;
        let clickCallback = function clickCallback(event)
        {
            if (self.selected)
            {
                self.unselect();
            }
            else
            {
                self.select();
            }
        };

        /* Construction */
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.UI.TagItem()", "name");
        MixEnergetik.Utilitary.Check.native(interaction, "string",
            "MixEnergetik.UI.TagItem()", "interaction");

        this.name = name;

        this.container = document.createElement("span");
        this.container.classList.add("video");
        this.container.classList.add("tag");

        this.container.innerHTML = this.name;

        if (interaction === "TAG_CLICKABLE")
        {
            this.container.addEventListener("click", clickCallback, false);
        }
    };

    TagItem.prototype.select = function select()
    {
        if (this.selected)
        {
            return;
        }

        const TagSelV = MixEnergetik.UI.TagSelectionView;
        const globalTag = TagSelV.tags[this.name];
        const relTag = TagSelV.relatedTags[this.name];

        /* 2018-07-02 Only one active tag */
        if (MixEnergetik.UI.TagSelectionView.selected !== null)
        {
            const name = MixEnergetik.UI.TagSelectionView.selected.name;
            MixEnergetik.UI.TagSelectionView.selected.unselect();

            if (name === this.name)
            {
                return;
            }
        }

        MixEnergetik.UI.VideoSelectionView.addFilter(this.name);

        globalTag.container.classList.add("selected");
        globalTag.selected = true;
        if (relTag)
        {
            relTag.container.classList.add("selected");
            relTag.selected = true;
        }

        MixEnergetik.UI.TagSelectionView.selected = this;
    };

    TagItem.prototype.unselect = function unselect()
    {
        if (!this.selected)
        {
            return;
        }

        const TagSelV = MixEnergetik.UI.TagSelectionView;
        const globalTag = TagSelV.tags[this.name];
        const relTag = TagSelV.relatedTags[this.name];

        MixEnergetik.UI.VideoSelectionView.removeFilter(this.name);

        globalTag.container.classList.remove("selected");
        globalTag.selected = false;
        if (relTag)
        {
            relTag.container.classList.remove("selected");
            relTag.selected = false;
        }

        MixEnergetik.UI.TagSelectionView.selected = null;
    };

    return TagItem;
})();

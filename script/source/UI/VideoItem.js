/*
 *  File:   VideoItem.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-07-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module VideoItem
 *      The items in the video selection view.
 */
MixEnergetik.UI.VideoItem = (function()
{
    let VideoItem = function VideoItem(url, name)
    {
        /* Declarations */
        /**
         *  @property {HTMLElement} container
         *      Container of the video.
         */
        this.container = null;
        /**
         *  @property {string} name
         *      The name of the video.
         */
        this.name = "";
        /**
         *  @property {object[]} sampleDatas
         *      Data of each samples, for delayed download.
         *      Fields:
         *          .path: the complete path to get the sample;
         *          .name: the name of the sample.
         */
        this.sampleDatas = []
        /**
         *  @property {MixEnergetik.UI.SampleItem[]} sampleItems
         *      List of SampleItem object corresponding to sample linked to the
         *      video.
         */
        this.sampleItems = [];
        /**
         *  @property {string} url
         *      Url of the video.
         */
        this.url = url;
        /**
         *  @property {string[]} tags
         *      List of tags related to this video.
         */
        this.tags = [];
        /**
         *  @property {string} status
         *      Status of the item. Possible values are:
         *      - "STATUS_LOADING_TAGS" when tags are fetching
         *      - "STATUS_DEFAULT" when object is perfectly usable
         */
        this.status;
        /**
         *  @property {MixEnergetik.UI.Event} event
         *      Event manager for a video item.
         *      Possible events are:
         *      - "tagfetch": when tags are fetched over the server;
         *      - "tagload": when tags have been loaded from the server;
         *      - "samplefetch": when samples are being retrieved from the
         *      server;
         *      - "sampleload": when samples have finally been loaded.
         */
        this.event = new MixEnergetik.Utilitary.Event();

        let self = this;

        /**
         *  @function clickCallback
         *      Called when a video selector has been clicked.
         *      Select the video so that we can mix under it.
         */
        let clickCallback = function clickCallback(event)
        {
            MixEnergetik.UI.VideoView.setVideoURL(url, name);
        };

        /**
         *  @function loadTags
         *      Load tags corresponding to video.
         */
        let loadTags = function loadTags()
        {
            const TAGS_PATH = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/tags";

            let xhr = new XMLHttpRequest();
            let url = TAGS_PATH + "/" + self.name + ".txt";

            xhr.open("GET", url, true);
            // Send the proper header information along with the request
            xhr.responseType = "text";

            xhr.onreadystatechange = function()
            {
                if(xhr.readyState == XMLHttpRequest.DONE)
                {
                    if (xhr.status == 200)
                    {
                        let allTagsString = xhr.response || null;

                        if (allTagsString === null)
                        {
                            throw new Error("Debug:Something weird happened.");
                        }

                        let allTags = allTagsString.split("\n");

                        for (let i = 0; i < allTags.length; i++)
                        {
                            const tagName = allTags[i];

                            /* Avoid empty string things */
                            if ((tagName !== "") && (tagName !== "\n")
                                && (tagName !== "\r") && (tagName !== "\t"))
                            {
                                MixEnergetik.UI.TagSelectionView.addTag(tagName);
                                self.tags.push(tagName);
                            }
                        }
                    }
                    else
                    {
                        throw new Error("[MixEnergetik.UI.VideoItem_loadTags()]"
                            + " Couldn't retrieve tags");
                    }

                    self.status = "STATUS_DEFAULT";
                    self.event.trigger("tagload");
                }
            };

            self.status = "STATUS_LOADING_TAGS";
            self.event.trigger("tagfetch");
            xhr.send();
        };

        /* Construction */
        MixEnergetik.Utilitary.Check.native(url, "string",
            "MixEnergetik.UI.VideoItem.constructor()", "url");
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.UI.VideoItem.constructor()", "name");

        this.name = name;
        this.url  = url;

        this.container = document.createElement("div");
        this.container.classList.add("video");
        this.container.classList.add("selector");
        this.container.classList.add("container");
        {
            let videoElement = document.createElement("video");
            videoElement.src = url;
            videoElement.preload = "metadata";
            videoElement.title = name;
            this.container.appendChild(videoElement);
        }
        this.container.addEventListener("click", clickCallback, false);

        loadTags();
    }; /* VideoItem constructor */

    VideoItem.prototype.loadSamples = function loadSamples()
    {
        this.event.trigger("samplefetch");

        const SndBldr = MixEnergetik.Utilitary.SoundBuilder;
        const AudioM = MixEnergetik.AudioManager;

        this.sampleItems = [];

        for (let i = 0; i < this.sampleDatas.length; i++)
        {
            const samplePath = this.sampleDatas[i].path;
            const sampleName = this.sampleDatas[i].name;
            const soundWrapper = new SndBldr.createSoundWrapper(samplePath);
            const soundIndex = AudioM.provideSound(soundWrapper);
            const sampleItem = new MixEnergetik.UI.SampleItem(soundIndex,
                sampleName);

            this.sampleItems.push(sampleItem);
        }

        this.event.trigger("sampleload");
    };

    return VideoItem;
})();

/*
 *  File:   ChargeItems.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-20
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @constructor MixEnergetik.UI.BatteryItem
 *      Deals with the accumulator samples.
 */
MixEnergetik.UI.BatteryItem = (function()
{
    let BatteryItem = function BatteryItem()
    {
        /* Declaration */
        /**
         *  @property {HTMLElement} container
         *      The battery item HTMLElement.
         */
        this.container = null;
        /**
         *  @property {MixEnergetik.UI.SampleItem} sampleItem
         *      The SampleItem object inside the Battery.
         */
        this.sampleItem = null;

        /**
         *  @property {HTMLDivElement} keyCue
         *      The div element containing the key cue.
         */
        this.keyCue = null;

        let self = this;

        let dragOverCallback = function dragOverCallback(event)
        {
            let dragged = MixEnergetik.DragAndDropManager.draggedItem;

            /* To allow drop */
            event.preventDefault();

            if ((self.sampleItem === null)
                && (dragged instanceof MixEnergetik.UI.SampleItem))
            {
                event.dataTransfer.dropEffect = "link";

                MixEnergetik.DragAndDropManager.targetItem = self;
            }
        };

        let dropCallback = function dropCallback(event)
        {
            let dragged = MixEnergetik.DragAndDropManager.draggedItem;

            event.preventDefault(); /* avoid opening as a link */

            if (self.sampleItem !== null)
            {
                return;
            }
            if (!(dragged instanceof MixEnergetik.UI.SampleItem))
            {
                return;
            }

            self.fillWith(dragged.soundData);
        };

        /* Construction */

        this.container = document.createElement("div");
        this.container.classList.add("sample");
        this.container.classList.add("container");
        this.container.classList.add("battery");

        /* Drag hovers over a valid drop target */
        this.container.addEventListener("dragover", dragOverCallback, false);
        /* Drag event has succesfully droped */
        this.container.addEventListener("drop", dropCallback, false);
    };

    /**
     *  @function fillWith
     *      Fill the sample item, using the given sound data.
     */
    BatteryItem.prototype.fillWith = function(soundData)
    {
        MixEnergetik.Utilitary.Check.object(soundData,
            MixEnergetik.SoundData,
            "MixEnergetik.UI.BatteryItem.fillWith()", "soundData",
            "MixEnergetik.SoundData");

        let soundWrapper = MixEnergetik.AudioManager.soundWrappers[soundData.soundIndex];
        let splitPath = soundWrapper.path.split("/");
        let name = splitPath[splitPath.length - 1];

        this.sampleItem = new MixEnergetik.UI.SampleItem(soundData.soundIndex,
            name);
        this.sampleItem.soundData = soundData.clone();
        this.sampleItem.parent = this;
        this.container.appendChild(this.sampleItem.container);
    }

    /**
     *  @function clear
     *      Extern call to delete proper the sampleItem.
     *      Nothing happens if there is no item inside.
     */
    BatteryItem.prototype.clear = function clear()
    {
        if (this.sampleItem === null)
        {
            return;
        }

        if (this.sampleItem.control.extended)
        {
            /* Remove item control if needed. */
            this.sampleItem.control.retract();
        }

        this.container.removeChild(this.sampleItem.container);
        this.sampleItem = null;
    };

    /**
     *  @function dropTargetOn
     *      Activate the drop target indication.
     */
    BatteryItem.prototype.dropTargetOn = function dropTargetOn()
    {
        this.container.classList.add("dropTarget");
    };

    /**
     *  @function dropTargetOff
     *      Activate the drop target indication.
     */
    BatteryItem.prototype.dropTargetOff = function dropTargetOff()
    {
        this.container.classList.remove("dropTarget");
    };

    /**
     *  @function inputDown
     *      Input forwarding.
     */
    BatteryItem.prototype.inputDown = function inputDown()
    {
        if (this.sampleItem)
        {
            this.sampleItem.inputDown();
        }
    };

    /**
     *  @function inputUp
     *      Input forwarding.
     */
    BatteryItem.prototype.inputUp = function inputUp()
    {
        if (this.sampleItem)
        {
            this.sampleItem.inputUp();
        }
    };

    return BatteryItem;
})();

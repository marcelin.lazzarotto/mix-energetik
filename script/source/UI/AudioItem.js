/*
 *  File:   AudioItem.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @constructor MixEnergetik.UI.ItemControl
 *      Adds advanced controls for AudioItem objects.
 */
MixEnergetik.UI.ItemControl = (function()
{
    const ItemControl_CONTAINER_XOFFSET = -80;
    const ItemControl_CONTAINER_YOFFSET = -100;

    let ItemControl = function ItemControl()
    {
        /* Declarations */
        /**
         *  @property {HTMLElement} container
         *      The item control DOM element.
         */
        this.container = null;
        /**
         *  @property {HTMLElement} button
         *      The button to show content.
         */
        this.button = null;
        /**
         *  @property {HTMLInput} volumeSlider
         *      The range input for the sound volume.
         */
        this.volumeSlider = null;
        /**
         *  @property {HTMLElement} soloButton
         *      The range input for the sound volume.
         */
        this.soloButton = null;
        /**
         *  @property {boolean} extended
         *      Keeps track of the state.
         */
        this.extended = false;
        /**
         *  @property {HTMLElement} parentContainer
         *      The parent container, used only for positioning.
         */
        this.parentContainer = null;
        /**
         *  @property {MixEnergetik.Utilitary.Event} event
         *      Event manager for an item control object.
         *      Possible events are:
         *      - "extend": when the item control interface appears
         *      - "retract": when the item control interface disappears
         */
        this.event = null;

        let buttonClose = null;

        let self = this;

        let buttonShowControlCallback = function buttonShowControlCallback(event)
        {
            if (!self.extended)
            {
                self.extend();
            }
            else
            {
                self.retract();
            }
        };

        /*
         * Construction
         */
        this.container = document.createElement("div");
        this.button    = document.createElement("div");
        buttonClose = document.createElement("div");
        this.volumeSlider = document.createElement("input");

        this.container.classList.add("item");
        this.container.classList.add("control");
        this.container.classList.add("container");

        this.button.classList.add("item");
        this.button.classList.add("control");
        this.button.classList.add("button");
        // {
        //     let gearImage = document.createElement("img");
        //     gearImage.src = "https://upload.wikimedia.org/wikipedia/commons/0/0b/Gear_icon_svg.svg";
        //     this.button.appendChild(gearImage);
        // }

        buttonClose.innerHTML = "X";
        buttonClose.classList.add("item");
        buttonClose.classList.add("control");
        buttonClose.classList.add("button");
        buttonClose.classList.add("close");

        this.volumeSlider = document.createElement("input");
        this.volumeSlider.type = "range";
        this.volumeSlider.min = 0;
        this.volumeSlider.max = 1;
        this.volumeSlider.value = 1;
        this.volumeSlider.step = 0.01;

        this.soloButton = document.createElement("div");
        this.soloButton.classList.add("item");
        this.soloButton.classList.add("control");
        this.soloButton.classList.add("button");
        this.soloButton.classList.add("solo");
        this.soloButton.innerHTML = "Solo";

        this.button.addEventListener("click", buttonShowControlCallback, false);
        buttonClose.addEventListener("click", function(){self.retract();}, false);

        this.container.appendChild(this.volumeSlider);
        this.container.appendChild(this.soloButton);
        this.container.appendChild(buttonClose);

        this.event = new MixEnergetik.Utilitary.Event();
    };

    let resizeCallback = function resizeCallback(event, self)
    {
        self.computePositions();
    };

    /**
     *  @function computePositions
     *      Compute the position of the control window.
     */
    ItemControl.prototype.computePositions = function computePositions()
    {
        let rect = this.parentContainer.getBoundingClientRect();
        let xpos = (rect.left + pageXOffset + ItemControl_CONTAINER_XOFFSET);
        let ypos = (rect.top  + pageYOffset + ItemControl_CONTAINER_YOFFSET);

        /* Avoid moving out of the page */
        if (xpos < 0)
        {
            xpos = 0;
        }
        if (ypos < 0)
        {
            ypos = 0;
        }

        this.container.style.left = xpos + "px";
        this.container.style.top  = ypos + "px";
    };

    /**
     *  @function extend
     *      Extends the UI of the ItemControl to show additional information.
     */
    ItemControl.prototype.extend = function extend()
    {
        if (this.extended)
        {
            return;
        }

        let self = this;

        /* Ugly but no other choice */
        const APPLICATION = document.getElementById("Application");

        this.computePositions();

        window.addEventListener("resize", function(event)
        {
            resizeCallback(event, self);
        }, false);
        APPLICATION.appendChild(this.container);

        this.extended = true;

        this.event.trigger("extend");
    };

    /**
     *  @function retract
     *      Retract the UI to normal state to hide additional information.
     */
    ItemControl.prototype.retract = function retract()
    {
        if (!this.extended)
        {
            return;
        }

        /* Ugly but no other choice */
        const APPLICATION = document.getElementById("Application");

        APPLICATION.removeChild(this.container);
        // window.removeEventListener("resize", resizeCallback, false);

        this.extended = false;

        this.event.trigger("retract");
    };

    return ItemControl;
})();

/**
 *  @constructor MixEnergetik.UI.AudioItem
 *      Basic item to represent both Sample and Group item.
 */
MixEnergetik.UI.AudioItem = (function()
{
    /**
     *  @constructor AudioItem
     *      Base AudioItem constructor for audio container on which the user can
     *      interact.
     */
    let AudioItem = function AudioItem()
    {
        /*
         *  Definitions
         */
        /**
         *  @property {HTMLDivElement} container
         *      The div with which contains all the user can interact.
         */
        this.container = null;
        /**
         *  @property {HTMLDivElement} animation
         *      A div that is used to perform an animation when linked audio
         *      plays.
         */
        this.animation = null;
        /**
         *  @property {HTMLDivElement} source
         *      Indicates the clickable element in itself. May be the same as
         *      container if there is no control to add to the element.
         */
        this.source = null;
        /**
         *  @property {bool} disabled
         *      Keeps in mind if the audio source is disabled due to
         *      incompatibility with another audio source currently playing.
         */
        this.disabled = false;
        /**
         *  @property {bool} on
         *      Keeps in mind if the audio source is on or off, which will ease
         *      some bits of logic.
         */
        this.on = false;
        /**
         *  @property {MixEnergetik.UI.ItemControl} control
         *      The more advanced UI for sample control.
         */
        this.control = null;
        /**
         *  @property {HTMLDivElement} keyCue
         *      Div node of the key.
         */
        this.keyCue = null;

        /*
         *  Construction
         */
        /* Create this, even if not used */
        this.control = new MixEnergetik.UI.ItemControl();
    };

    /*
     *  Clean way to update prototype.
     */
    Object.defineProperties(AudioItem.prototype,
    {
        /**
         *  @function play
         *      Function called when the user main-clicks on a AudioItem.
         *      Depending on the state of the application, the effects may vary
         *      a lot.
         */
        play:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
            }
        },

        /**
         *  @function stop
         *      Stops the sample currently playing and reset previous state.
         */
        stop:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
            }
        },

        /**
         *  @function soloOn
         *      User response to solo mode on for item.
         */
        soloOn:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function ()
            {
                this.control.soloButton.classList.add("checked");
                this.use();
            }
        },

        /**
         *  @function soloOff
         *      User response to solo mode off for item.
         */
        soloOff:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function ()
            {
                this.control.soloButton.classList.remove("checked");
                this.unuse();
            }
        },

        /**
         *  @function disable
         *      Disable the audio source so that it cannot be clicked and used
         *      until reenabled.
         */
        disable:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.container.classList.add("disabled");
                this.disabled = true;
            }
        },

        /**
         *  @function enable
         *      Enable the audio source so that it can be clicked and used
         *      normally.
         */
        enable:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.container.classList.remove("disabled");
                this.disabled = false;
            }
        },

        /**
         *  @function use
         *      Mark the environmentItem object as on, which changes its
         *      display.
         */
        use:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                let self = this;
                const DURATION = MixEnergetik.Utilitary.Configuration.Audio.sampleDuration;
                const MAX_WIDTH = 100;

                const renderAnimation = function()
                {
                    if (!self.on)
                    {
                        return;
                    }

                    const elapsedTime = MixEnergetik.AudioManager.getCurrentTime();
                    const ratio = elapsedTime / DURATION;
                    let width = ratio * MAX_WIDTH;
                    width = (width <= 100.0) ? width : 100.0;

                    self.animation.style.width = width + "%";

                    window.requestAnimationFrame(renderAnimation);
                }

                this.on = true;
                this.container.classList.add("on");

                if (this.animation)
                {
                    window.requestAnimationFrame(renderAnimation);
                }
            }
        },

        /**
         *  @function unused
         *      Make the item return at normal state, changing its display.
         */
        unuse:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.on = false;
                this.container.classList.remove("on");
            }
        },

        /**
         *  @function setVolume
         *      The the volume for the given audio item.
         *      It can be the volume of the sample if the item concerns only
         *      one sample or the master volume of a group.
         *
         *  @param {float} volume
         *      Volume value between 0 and 1. 1 equals 100% and 0 is mute.
         *      The sound cannot go beyond 0 and 1.
         *      Nothing happens if volume is not valid (i.e. null, undefined or
         *      not a number).
         *
         *  @warning
         *      Base prototype method that does nothing.
         */
        setVolume:
        {
            configurable: true,
            enumerable: true,
            writable: false,

            value: function(volume)
            {
            }
        },

        /**
         *  @function setContainer
         *      Function to set the container for this audio item, since we will
         *      use either a provided svg polygon (TODO for now, will probably
         *      change) or an api-created standard div for groups.
         *
         *  @param {HTMLElement} container
         *      The element to use as a container for the item. Should be an
         *      SVG polygon or an HTML div, but can be any valid element in
         *      practice.
         *  @param {string} control
         *      Indicates if the item is controlable (volume...) or not.
         *      Possible values are: "CONTROL_ENABLE" (default) or
         *      "CONTROL_DISABLE".
         *  @param {string} drag
         *      Indicates if the item is draggable or not.
         *      Possible values are: "DRAG_ENABLE" (default) or "DRAG_DISABLE".
         */
        setContainer:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function setContainer(container, control, drag)
            {
                const _CODE_LOCATION = "MixEnergetik.UI.AudioItem.setContainer()";

                control = control || "CONTROL_ENABLE";
                drag = drag || "DRAG_ENABLE";

                MixEnergetik.Utilitary.Check.native(control, "string",
                    _CODE_LOCATION, "control");

                this.container = container;
                this.container.classList.add("audio");
                this.container.classList.add("item");
                this.container.classList.add("container");

                let self = this;

                if (control === "CONTROL_ENABLE")
                {
                    let volumeSliderCallback = function volumeSliderCallback(event)
                    {
                        // Update the current slider value (each time you drag the slider handle)
                        self.setVolume(+self.control.volumeSlider.value); // cast
                    };

                    let soloButtonCallback = function soloButtonCallback(event)
                    {
                        const SoloM = MixEnergetik.SoloManager;

                        if (SoloM.soloItem === self)
                        {
                            SoloM.clear();
                        }
                        else
                        {
                            SoloM.setSoloItem(self);
                        }
                    };

                    this.container.appendChild(this.control.button);

                    this.control.parentContainer = this.container;

                    this.control.volumeSlider.addEventListener("input",
                        volumeSliderCallback, false);

                    this.control.soloButton.addEventListener("click",
                        soloButtonCallback, false);

                    /* Source is actually the clickable element */
                    this.source = document.createElement("div");

                    this.container.appendChild(this.source);
                }
                else if (control === "CONTROL_DISABLE")
                {
                    this.source = container;
                }
                else
                {
                    throw new Error("[" + _CODE_LOCATION + "] control parameter"
                        + " is invalid");
                }

                this.source.classList.add("audio");
                this.source.classList.add("item");
                this.source.classList.add("source");

                /* Progression animation */
                if (this.source instanceof HTMLDivElement)
                {
                    this.animation = document.createElement("div");
                    this.animation.classList.add("audio");
                    this.animation.classList.add("item");
                    this.animation.classList.add("progression");
                    this.animation.classList.add("animation");

                    this.source.appendChild(this.animation);
                }

                if (drag === "DRAG_ENABLE")
                {
                    /* Small steps for drag and drop */
                    this.source.draggable = true;

                    /* Drag start is fired on draggable element */
                    this.source.addEventListener("dragstart", function(event)
                    {
                        MixEnergetik.DragAndDropManager.draggedItem = self;
                    }, false);
                }
                else if (drag === "DRAG_DISABLE")
                {
                    /**/
                }
                else
                {
                    throw new Error("[" + _CODE_LOCATION + "] control parameter"
                        + " is invalid");
                }
            }
        }
    }); /* Update MixEnergetik.UI.AudioItem prototype */

    return AudioItem;
})();

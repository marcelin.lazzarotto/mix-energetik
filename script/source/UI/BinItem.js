/*
 *  File:   ChargeItems.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-20
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @constructor BinItem
 *      Builds logic for a bin.
 *
 *  @param {<optional> HTMLElement} container
 *      The container for the bin item. If the parameter is undefined or null,
 *      the container is automatically created as a div element.
 *  @param {<optional> string} type
 *      The type of the bin.
 *      Values are "BIN_SAMPLE" (default) (TODO) and "BIN_GROUP".
 */
MixEnergetik.UI.BinItem = (function()
{
    let BinItem = function BinItem(container, type)
    {
        /* Declaration */
        /**
         *  @property {HTMLElement} container
         *      The bin container.
         */
        this.container = null;

        /**
         *  @property {string} type
         *      The type of the bin.
         *      Values are "BIN_SAMPLE" (default) (TODO) and "BIN_GROUP".
         */
        this.type = "";

        let self = this;

        let dragOverCallback = function dragOverCallback(event)
        {
            MixEnergetik.DragAndDropManager.targetItem = self;
        };

        /* Construction */

        container = container || document.createElement("div");
        type = type || "BIN_SAMPLE";

        MixEnergetik.Utilitary.Check.object(container, HTMLElement,
            "MixEnergetik.UI.ChargeItems.BinItem()", "container",
            "HTMLElement");
        MixEnergetik.Utilitary.Check.native(type, "string",
            "MixEnergetik.UI.ChargeItems.BinItem()", "type");

        this.container = container;
        this.container.classList.add("sample");
        this.container.classList.add("bin");

        this.type = type;

        /* Drag hovers over a valid drop target */
        this.container.addEventListener("dragover", dragOverCallback, false);
    }

    /**
     *  @function dropTargetOn
     *      Activate the drop target indication.
     */
    BinItem.prototype.dropTargetOn = function dropTargetOn()
    {
        this.container.classList.add("dropTarget");
    };
    /**
     *  @function dropTargetOff
     *      Activate the drop target indication.
     */
    BinItem.prototype.dropTargetOff = function dropTargetOff()
    {
        this.container.classList.remove("dropTarget");
    };

    return BinItem;
})();

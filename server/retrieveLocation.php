<?php
/*
 *  File:   retrieveLocation.php
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-16
 */

/**
 *  @function getFileTreeFrom
 *      Retrieve a file tree from $path in a form of array/"object" mix to be
 *      used by json and put it in $fileTree.
 *
 *      $fileTree is of the form:
 *          [
 *              'type' => 'file' || 'directory'
 *              'name' => $fileObjectName
 *              (optional) 'content' => []
 *          ]
 *      Where $fileTree['content'] is of the same form.
 *
 *  @param {string} $path
 *      The path in under which to seek the file tree. Is expected to be a valid
 *      directory name.
 *  @param {array &} $fileTree
 *      The initial base, recursively completed.
 *
 *  @warning
 *      Recursive
 */
function getFileTreeFrom($path, array & $fileTree)
{
    if(!is_dir($path))
    {
        /* We should not be here */
        return;
    }

    $content = array_values(array_diff(scandir($path), array('.', '..')));

    for ($i = 0; $i < count($content); $i++)
    {
        $filename = $content[$i];

        $newpath = $path . '/' . $filename;

        $subTree = array('name' => $filename);

        if (is_dir($newpath))
        {
            $subTree['type'] = 'directory';
            $subTree['content'] = array();

            getFileTreeFrom($newpath, $subTree);
        }
        else
        {
            $subTree['type'] = 'file';
        }

        array_push($fileTree['content'], $subTree);
    }
}

if(!isset($_POST['type']))
{
    echo '';
    return;
}

$type = $_POST['type'];
$phase = NULL;
$base = NULL;

if(isset($_POST['phase']))
{
    $phase = $_POST['phase'];
}

switch ($type)
{
    case 'samples':
        switch($phase)
        {
            case NULL:
                echo 'PHASE_NOT_PROVIDED';
                return;
            case 'base':
                $base = '../resources/samples/base';
                break;
            case 'extraction':
                $base = '../resources/samples/extraction';
                break;
            case 'wander':
                $base = '../resources/samples/wander';
                break;
            case 'vie-mer':
                $base = '../resources/samples/vie-mer';
                break;
            default:
                echo 'PHASE_INVALID';
                return;
        }
        break;
    case 'videos':
        switch ($phase)
        {
            case NULL:
                echo 'PHASE_NOT_PROVIDED';
                return;
            case 'base';
                $base = '../resources/videos/base';
                break;
            case 'extraction';
                $base = '../resources/videos/extraction';
                break;
            case 'wander';
                $base = '../resources/videos/wander';
                break;
            case 'vie-mer';
                $base = '../resources/videos/vie-mer';
                break;
            default:
                echo 'PHASE_INVALID';
                return;
        }
        break;
    case 'images':
        switch ($phase)
        {
            case NULL:
                echo 'PHASE_NOT_PROVIDED';
                return;
            case 'base';
                $base = 'PHASE_INVALID';
                break;
            case 'extraction';
                $base = 'PHASE_INVALID';
                break;
            case 'wander';
                $base = 'PHASE_INVALID';
                break;
            case 'vie-mer';
                $base = '../resources/images/vie-mer';
                break;
            default:
                echo 'PHASE_INVALID';
                return;
        }
        break;
    case 'saves':
        $base = '../resources/saves';

        break;
    default:
        echo 'INVALID_TYPE';
        return;
        // break;
}

$fileTree = array('type' => 'directory',
    'name' => $base,
    'content' => array());

getFileTreeFrom($base, $fileTree);

$result = json_encode($fileTree, 0, 10000);

switch (json_last_error())
{
    case JSON_ERROR_NONE:
        /* echo ' - No errors'; */
        break;
    case JSON_ERROR_DEPTH:
        echo ' - Maximum stack depth exceeded';
        break;
    case JSON_ERROR_STATE_MISMATCH:
        echo ' - Underflow or the modes mismatch';
        break;
    case JSON_ERROR_CTRL_CHAR:
        echo ' - Unexpected control character found';
        break;
    case JSON_ERROR_SYNTAX:
        echo ' - Syntax error, malformed JSON';
        break;
    case JSON_ERROR_UTF8:
        echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
    default:
        echo ' - Unknown error';
        break;
}

echo $result;

?>

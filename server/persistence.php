<?php
/*
 *  File:   persistence.php
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-12
 */

require_once("authentication.php");

$action = null;
$filename = null;

if (empty($_POST['action']))
{
    echo 'PERSISTENCE_MISSING_ACTION';
    return;
}
if (empty($_POST['filename']))
{
    echo 'PERSISTENCE_MISSING_FILENAME';
    return;
}

$action = $_POST['action'];
$filename = $_POST['filename'];

/* Test for auth */
function testAuth()
{
    $login = '';
    $password = '';

    authentication_tmpGetLogins('../resources/security/login.txt', $login,
        $password);
    $res = authentication_test($login, $password);

    if ($res != ME_AUTHENTICATION_SUCCESS)
    {
        return false;
    }

    return true;
}

/* clean string */
$filename = str_replace('/', '_', $filename);
$filename = str_replace('\\', '_', $filename);
$filename = str_replace('"', '_', $filename);
$filename = str_replace('\'', '_', $filename);
$filename = str_replace(':', '_', $filename);
$filename = str_replace('*', '_', $filename);
$filename = str_replace('?', '_', $filename);
$filename = str_replace('<', '_', $filename);
$filename = str_replace('>', '_', $filename);
$filename = str_replace('|', '_', $filename);

$saveLocation = '../resources/saves/' . $filename . '.json';

/* Actual execution */
if ($action === 'save')
{
    if (testAuth() === true)
    {
        $fileLocation = $_FILES['file']['tmp_name'];

        $res = move_uploaded_file($fileLocation, $saveLocation);

        echo 'PERSISTENCE_SAVE_SUCCESS';
    }
    else
    {
        echo 'PERSISTENCE_AUTHENTICATION_FAILURE';

        exit;
    }
}
else if ($action === 'load')
{
    readfile($saveLocation);
}
else if ($action === 'remove')
{
    if (testAuth() === true)
    {
        $res = unlink($saveLocation);

        if ($res)
        {
            echo 'PERSISTENCE_SAVE_REMOVAL_SUCCESS';
        }
        else
        {
            echo 'PERSISTENCE_SAVE_REMOVE_FAILURE';
        }
    }
    else
    {
        echo 'PERSISTENCE_AUTHENTICATION_FAILURE';
    }
}
else
{
    echo 'PERSISTENCE_BAD_ACTION';
}

?>

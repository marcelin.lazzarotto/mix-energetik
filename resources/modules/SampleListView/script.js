/*
 *  File:   SampleListView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module SampleListView
 *      Module dealing with the sample list, so that we can highlight sample
 *      corresponding to sample that play in group.
 */
MixEnergetik.UI.SampleListView = (function()
{
    let SampleListView = {};

    /**
     *  @property {Object} sampleItems
     *      The sample items in the sample list. This is a map of items, so that
     *      there exist only one sample item for a sample (not really necessary,
     *      but it simplifies things) and this sample item can be accessed using
     *      the soundIndex (which is unique) as a key.
     */
    // let SampleListView_sampleItems = {};
    SampleListView.sampleItems = {};

    /**
     *  @property {MixEnergetik.Utilitary.KeyControl} keyControl
     *      The object dealing with key presses.
     */
    SampleListView.keyControl = new MixEnergetik.Utilitary.KeyControl();

    /**
     *  @function initialize
     *      Initialize the sample list view.
     */
    SampleListView.initialize = function initialize()
    {
        /* Meh */
        SampleListView_container = document.getElementById("SampleListView");
        SampleListView_content = document.getElementById("SampleListViewContainer");

        SampleListView.keyControl.setKeys(SampleListView_KEY_CONTROL_KEYS);
    };

    /**
     *  @function addSampleItem
     *      Add a sample item to the sample list and also add it to the view.
     *      Nothing happens if a sample item with the same sound index already
     *      exist (remove it first).
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The SampleItem object to add to our map.
     *
     *  @throws {Error}
     *      If sampleItem is undefined, null or not a SampleItem object.
     */
    SampleListView.addSampleItem = function addSampleItem(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.SampleListView.addSampleItem()",
            "sampleItem",
            "MixEnergetik.UI.SampleItem");

        if (SampleListView.sampleItems[sampleItem.soundData.soundIndex])
        {
            return;
        }

        SampleListView.sampleItems[sampleItem.soundData.soundIndex] = sampleItem;

        SampleListView_content.appendChild(sampleItem.container);

        /* Add to key control */
        SampleListView.keyControl.addItem(sampleItem);
    };

    /**
     *  @function removeSampleItem
     *      Remove a sample item from the map and the view.
     *      Nothing happens if sample item was not in the map.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The item to remove.
     *
     *  @throws {Error}
     *      If sampleItem is undefined, null or not a SampleItem object.
     */
    SampleListView.removeSampleItem = function removeSampleItem(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.SampleListView.removeSampleItem()",
            "sampleItem",
            "MixEnergetik.UI.SampleItem");

        let index = sampleItem.soundData.soundIndex;
        let status = false;

        if ((SampleListView.sampleItems[index] === undefined)
            || (SampleListView.sampleItems[index] === null))
        {
            console.log("[MixEnergetik.UI.SampleListView.removeSampleItem()]"
                + " sampleItem could not be found.");
            return;
        }

        SampleListView_content.removeChild(sampleItem.container);
        status = delete SampleListView.sampleItems[index];

        if (!status)
        {
            console.warn("[MixEnergetik.UI.SampleListView.removeSampleItem()]"
                + " Couldn't delete sampleItem");
        }
    };

    /**
     *  @function setSampleItems
     *      Set the sample list to the given array at once.
     *
     *  @param {MixEnergetik.UI.SampleItem[]} sampleItems
     *      The array of sample items to set the sample list to.
     */
    SampleListView.setSampleItems = function setSampleItems(sampleItems)
    {
        MixEnergetik.Utilitary.Check.array(sampleItems,
            "MixEnergetik.UI.SampleListView.setSample()", "sampleItems");

        MixEnergetik.UI.SampleListView.clear();

        for (let i = 0; i < sampleItems.length; i++)
        {
            MixEnergetik.UI.SampleListView.addSampleItem(sampleItems[i]);
        }
    };

    /**
     *  @function clear
     *      Removes all SampleItem objects from the sample list.
     */
    SampleListView.clear = function clear()
    {
        let sampleItems = MixEnergetik.UI.SampleListView.sampleItems;

        for (let soundIndex in sampleItems)
        {
            const sampleItem = sampleItems[soundIndex];

            if (sampleItem.control.extended)
            {
                /* Remove item control if needed. */
                sampleItem.control.retract();
            }

            /*  Temporary
             *  We need to rewrite sampleItem.play() and sampleItem.stop()
             *  So that those functions are not responsible from removing
             *  sounds from groups.
             *  This if else will be removed then.
             *  -Marcelin, 2019-04-2x
             */
            if (MixEnergetik.VieMer)
            {
                if (sampleItem.on)
                {
                    if (MixEnergetik.GroupManager.isActive())
                    {
                        let preset = MixEnergetik.GroupManager.getActive();
                        let soundIndex = sampleItem.soundData.soundIndex;

                        if (preset.indexOf(soundIndex) <= -1)
                        {
                            MixEnergetik.SampleManager.remove(sampleItem);
                        }
                    }
                    else
                    {
                        MixEnergetik.SampleManager.remove(sampleItem);
                    }
                    sampleItem.unuse();
                }
            }
            else
            {
                sampleItem.stop();
            }

            if (sampleItems.hasOwnProperty(soundIndex))
            {
                MixEnergetik.UI.SampleListView.removeSampleItem(sampleItem);
            }
        }

        SampleListView.keyControl.clear();
    };

    /**
     *  @function extend
     *      Extends the view.
     */
    SampleListView.extend = function extend()
    {
        SampleListView_container.classList.add("extended");
    };

    /**
     *  @function retract
     *      Retracts the view.
     */
    SampleListView.retract = function retract()
    {
        SampleListView_container.classList.remove("extended");
    };

    /**
     *  @function show
     *      Shows the view.
     */
    SampleListView.show = function show()
    {
        SampleListView_container.classList.remove("hidden");
    };

    /**
     *  @function hide
     *      Hides the view.
     */
    SampleListView.hide = function hide()
    {
        SampleListView_container.classList.add("hidden");
    };

    /* Private */

    /**
     *  @property {HTMLElement} container
     *      The view container.
     */
    let SampleListView_container = null;
    /**
    *  @property {HTMLElement} content
    *      The container for all the sample items.
    */
    let SampleListView_content = null;

    /**
     *  @property {object} containers
     *      Map of sample containers. Really bad design but whatever. TODO.
     */
    let SampleListView_contents = {};

    const SampleListView_KEY_CONTROL_KEYS = ["t", "y", "u", "i", "o", "p", "g",
        "h", "j", "k", "l", "m"];

    return SampleListView;
})();

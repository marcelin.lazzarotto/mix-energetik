/*
 *  File:   VideoSelectionView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module VideoSelectionView
 *      The manager dealing with the video.
 */
MixEnergetik.UI.VideoSelectionView = (function()
{
    let VideoSelectionView = {};

    VideoSelectionView.loaded = false;

    /**
     *  @property {object} videoItems
     *      Contains the list of VideoItems object to access them to put samples
     *      there.
     *      Is a map identified from their name.
     */
    VideoSelectionView.videoItems = {};

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      Event manager for the VideoSelectionView object.
     *      Possible events are:
     *      - "fetch" when the view has started fetching for videos.
     *      - "load" when the view has loaded videos.
     */
    VideoSelectionView.event = new MixEnergetik.Utilitary.Event();

    VideoSelectionView.initialize = function initialize()
    {
        VideoSelectionView_container = document.getElementById(
            VideoSelectionView_CONTAINER_ID);
        VideoSelectionView_tagButton = document.getElementById(
            VideoSelectionView_TAG_BUTTON_ID);
        VideoSelectionView_tagsContainer = document.getElementById(
            VideoSelectionView_TAGS_CONTAINER_ID);

        VideoSelectionView_retrieveVideos();

        VideoSelectionView_tagButton.addEventListener("click", function()
        {
            const TagSelV = MixEnergetik.UI.TagSelectionView;

            if (TagSelV.hidden)
            {
                TagSelV.show();
            }
            else
            {
                TagSelV.hide();
            }
        }, false);
    };

    /**
     *  @function addFilter
     *      Add the given filter (a tag name) to the selection, so that only
     *      the videos corresponding to actual filters appears.
     */
    VideoSelectionView.addFilter = function addFilter(tagName)
    {
        MixEnergetik.Utilitary.Check.native(tagName, "string",
            "MixEnergetik.UI.VideoSelectionView.addFilter()", "tagName");

        let videoItems = MixEnergetik.UI.VideoSelectionView.videoItems;

        for (let videoName in videoItems)
        {
            let videoItem = videoItems[videoName];

            if (videoItem.tags.indexOf(tagName) === -1)
            {
                videoItem.container.classList.add("hidden");
            }
        }

        let tagItem = new MixEnergetik.UI.TagItem(tagName, "TAG_UNCLICKABLE");
        VideoSelectionView_tagItems[tagName] = tagItem;
        VideoSelectionView_tagsContainer.appendChild(tagItem.container);
    };

    /**
     *  @function removeFilter
     *      Remove the given filter (a tag name) from the selection, so that
     *      only the videos corresponding to current filters appears.
     */
    VideoSelectionView.removeFilter = function removeFilter(tagName)
    {
        MixEnergetik.Utilitary.Check.native(tagName, "string",
            "MixEnergetik.UI.VideoSelectionView.removeFilter()", "tagName");

        let videoItems = MixEnergetik.UI.VideoSelectionView.videoItems;

        let tagItem = VideoSelectionView_tagItems[tagName];
        delete VideoSelectionView_tagItems[tagName];

        for (let videoName in videoItems)
        {
            let videoItem = videoItems[videoName];

            if (videoItem.tags.indexOf(tagName) === -1)
            {
                let currentTags = VideoSelectionView_tagItems;
                let hasAllCurrentTags = true;

                for (let currentTag in VideoSelectionView_tagItems)
                {
                    if (videoItem.tags.indexOf(currentTag) === -1)
                    {
                        hasAllCurrentTags = false;
                        break;
                    }
                }

                if (hasAllCurrentTags)
                {
                    videoItem.container.classList.remove("hidden");
                }
            }
        }

        VideoSelectionView_tagsContainer.removeChild(tagItem.container);
    };

    /**
     *  @function hide
     *      Hides the view.
     */
    VideoSelectionView.hide = function hide()
    {
        VideoSelectionView_container.classList.add("hidden");
    };

    /**
     *  @function show
     *      Shows the view.
     */
    VideoSelectionView.show = function show()
    {
        VideoSelectionView_container.classList.remove("hidden");
    };

    /**
     *  @constant {string} VideoSelectionView_CONTAINER_ID
     *      The id of the big container present on the webpage.
     */
    const VideoSelectionView_CONTAINER_ID = "VideoSelectionContainer";
    /**
     *  @constant {string} VideoSelectionView_CONTAINER_ID
     *      The id of the big container present on the webpage.
     */
    const VideoSelectionView_TAG_BUTTON_ID = "VideoSelectionTags";
    const VideoSelectionView_TAGS_CONTAINER_ID = "VideoSelectionTagsContainer";

    /**
     *  @property {HTMLVideoElement} container
     *      The video element allowing us to manipulate video.
     */
    let VideoSelectionView_container = null;
    let VideoSelectionView_tagButton = null;
    let VideoSelectionView_tagsContainer = null;

    /**
     *  @property {MixEnergetik.UI.TagItem objectmap} tagItems
     *
     */
    let VideoSelectionView_tagItems = {};

    let util_getDirName = function getDirName(dirObject)
    {
        for (let dirName in dirObject)
        {
            if (dirObject.hasOwnProperty(dirName))
            {
                return dirName;
            }
        }

        return "";
    };

    let VideoSelectionView_retrieveSamplePathes = function retrieveSamplePathes()
    {
        let successHandler = function (fileTree)
        {
            const BASE_PATH = "../resources/samples/wander";
            const TRUE_BASE_PATH = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/samples/wander";
            const videoItems = MixEnergetik.UI.VideoSelectionView.videoItems;

            if ((fileTree.name !== BASE_PATH)
                || (fileTree.type !== "directory"))
            {
                throw new Error("[MixEnergetik.UI.VideoSelectionView_retrieveSamplePathes()]"
                    + " Invalid data returned by"
                    + " MasterManager.retrievePaths()");
            }

            for (let i = 0; i < fileTree.content.length; i++)
            {
                let dir = fileTree.content[i];

                if (dir.type !== "directory")
                {
                    console.warn("[MixEnergetik.UI.VideoSelectionView_retrieveSamplePathes]"
                        + " Expected directory, found a file.");

                    continue;
                }

                if (!videoItems[dir.name])
                {
                    console.warn("[MixEnergetik.UI.VideoSelectionView_retrieveSamplePathes()]"
                        + " Found a sample directory not corresponding to any"
                        + " video.");

                    continue;
                }

                for (let j = 0; j < dir.content.length; j++)
                {
                    let fileObject = dir.content[j];

                    if (fileObject.type !== "file")
                    {
                        console.warn("[MixEnergetik.UI.VideoSelectionView_retrieveSamplePathes()]"
                            + " Expected sample file, found directory.");

                        continue;
                    }

                    let sampleData = {};
                    sampleData.path = TRUE_BASE_PATH + "/" + dir.name + "/"
                        + fileObject.name;
                    sampleData.name = fileObject.name;

                    videoItems[dir.name].sampleDatas.push(sampleData);
                }
            }

            MixEnergetik.UI.VideoSelectionView.event.trigger("load");
        };
        let errorHandler = function (status, errorString)
        {
            MixEnergetik.UI.VideoSelectionView.event.trigger("load");
            throw new Error (errorString + " with status " + status);
        };

        MixEnergetik.MasterManager.retrievePaths("samples", "wander",
            successHandler, errorHandler);
    };

    let VideoSelectionView_retrieveVideos = function retrieveVideoURLs()
    {
        let successHandler = function successHandler(fileTree)
        {
            const BASE_PATH = "../resources/videos/wander";
            const TRUE_BASE_PATH = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/videos/wander";

            if ((fileTree.name !== BASE_PATH)
                || (fileTree.type !== "directory"))
            {
                throw new Error("[VideoSelectionView_retrieveVideos()] Invalid"
                    + " data returned by MasterManager.retrievePaths().");
            }

            for (let i = 0; i < fileTree.content.length; i++)
            {
                let fileObject = fileTree.content[i];

                if (fileObject.type !== "file")
                {
                    console.warn("[VideoSelectionView_retrieveVideos()]"
                        + " Expected video file, found directory.");

                    continue;
                }

                let videoName = fileObject.name.split('.')[0];
                let videoItem = new MixEnergetik.UI.VideoItem(TRUE_BASE_PATH
                    + "/" + fileObject.name, videoName);
                MixEnergetik.UI.VideoSelectionView.videoItems[videoName] = videoItem;

                VideoSelectionView_container.appendChild(videoItem.container);
            }

            VideoSelectionView_retrieveSamplePathes();
        }; /* successHandler() */

        let errorHandler = function errorHandler(status, errorString)
        {
            MixEnergetik.UI.VideoSelectionView.event.trigger("load");

            throw new Error ("[MixEnergetik.UI.VideoSelectionView_retrieveVideos()]"
                + " " + errorString + " with status " + status);
        };

        MixEnergetik.UI.VideoSelectionView.event.trigger("fetch");

        MixEnergetik.MasterManager.retrievePaths("videos", "wander",
            successHandler, errorHandler);
    };

    VideoSelectionView.event.on("load", function()
    {
        const videoItems = MixEnergetik.UI.VideoSelectionView.videoItems;

        VideoSelectionView.loaded = true;

        /* Load the first */
        for (let videoName in videoItems)
        {
            let videoItem = videoItems[videoName];

            MixEnergetik.UI.VideoView.setVideoURL(videoItem.url, videoItem.name);

            break;
        }
    });

    return VideoSelectionView;
})();

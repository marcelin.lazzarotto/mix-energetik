/*
 *  File:   PersistenceView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module PersistenceView
 *      The view dealing with save and load.
 */
MixEnergetik.UI.PersistenceView = (function()
{
    /**
     *  @constructor SaveSelectorItem
     *      An item representing a clickable save object to load a save.
     *
     *  @property {string} name
     *      The name of the save.
     */
    let SaveSelectorItem = function SaveSelectorItem(name)
    {
        /* Definition */
        /**
         *  @property {HTMLDivElement} container
         *      The container of the element.
         */
        this.container = null;

        /**
         *  @property {string} name
         *      The name of the save.
         */
        this.name = "";

        let thisRemoveButton = null;

        let self = this;

        let clickCallback = function clickCallback(event)
        {
            if (event.target === thisRemoveButton)
            {
                return;
            }

            PersistenceView_hideLoad();

            MixEnergetik.PersistenceManager.load(self.name);
        };

        let removeButtonCallback = function removeButtonCallback(event)
        {
            PersistenceView_loadAskConfirm(self);
        };

        /* Construction */

        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.UI.PersistenceView_SaveSelectorItem()", "name");

        this.name = name;

        this.container = document.createElement("div");

        this.container.classList.add("save");
        this.container.classList.add("selector");

        {
            let nameContainer = document.createElement("div");
            nameContainer.classList.add("save");
            nameContainer.classList.add("name");
            nameContainer.innerHTML = this.name;
            this.container.appendChild(nameContainer);
        }

        {
            thisRemoveButton = document.createElement("div");
            thisRemoveButton.classList.add("remove");
            thisRemoveButton.classList.add("button");

            let img = document.createElement("img");
            img.src = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/modules/PersistenceView/Remove.svg";
            img.alt = "Remove";
            img.classList.add("noninteractable");
            thisRemoveButton.appendChild(img);
            this.container.appendChild(thisRemoveButton);
        }

        this.container.addEventListener("click", clickCallback, false);
        thisRemoveButton.addEventListener("click", removeButtonCallback, false);
    };

    let PersistenceView = {};

    /**
     *  @function initialize
     *      Sets the shit up.
     */
    PersistenceView.initialize = function initialize()
    {
        PersistenceView_container = document.getElementById("PersistenceView");

        PersistenceView_defaultMixButton = document.getElementById("PersistenceViewDefaultMixButton");
        PersistenceView_saveButton = document.getElementById("PersistenceViewSaveButton");
        PersistenceView_loadButton = document.getElementById("PersistenceViewLoadButton");

        PersistenceView_saveContainer = document.getElementById("PersistenceViewSave");
        PersistenceView_saveNameInput = document.getElementById("PersistenceViewSaveNameInput");
        PersistenceView_saveConfirmButton = document.getElementById("PersistenceViewSaveConfirmButton");
        PersistenceView_saveCancelButton = document.getElementById("PersistenceViewSaveCancelButton");

        PersistenceView_loadContainer = document.getElementById("PersistenceViewLoad");
        PersistenceView_loadCloseButton = document.getElementById("PersistenceViewLoadCloseButton");

        PersistenceView_loadAskConfirmContainer = document.getElementById("PersistenceViewLoadAskConfirm");
        PersistenceView_loadAskConfirmButton = document.getElementById("PersistenceViewLoadAskConfirmButton");
        PersistenceView_loadAskConfirmCancelButton = document.getElementById("PersistenceViewLoadAskConfirmCancelButton");

        PersistenceView_saveContainer.classList.add("hidden");
        PersistenceView_loadContainer.classList.add("hidden");
        PersistenceView_loadAskConfirmContainer.classList.add("hidden");

        PersistenceView_defaultMixButton.addEventListener("click",
            PersistenceView_defaultMixButtonCallback, false);
        PersistenceView_saveButton.addEventListener("click",
            PersistenceView_saveButtonCallback, false);

        PersistenceView_saveConfirmButton.addEventListener("click",
            PersistenceView_saveConfirmButtonCallback, false);
        PersistenceView_saveCancelButton.addEventListener("click",
            PersistenceView_saveCancelButtonCallback, false);

        PersistenceView_loadButton.addEventListener("click",
            PersistenceView_loadButtonCallback, false);
        PersistenceView_loadCloseButton.addEventListener("click",
            PersistenceView_loadCloseButtonCallback, false);

        PersistenceView_loadAskConfirmButton.addEventListener("click",
            PersistenceView_loadAskConfirmButtonCallback, false);
        PersistenceView_loadAskConfirmCancelButton.addEventListener("click",
            PersistenceView_loadAskConfirmCancelButtonCallback, false);
    };

    /**
     *  @function addSave
     *      Adds a save to the possible loading saves in the list (loading
     *      window).
     *
     *  @param {string} name
     *      The name of the save to add
     */
    PersistenceView.addSave = function addSave(name)
    {
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.UI.PersistenceView.addSave", "name");

        let saveItem = new SaveSelectorItem(name);

        PersistenceView_items[name] = saveItem;

        PersistenceView_loadContainer.appendChild(saveItem.container);
    };

    /**
     *  @function removeSave
     *      Removes a save to the from the list.
     *
     *  @param {string} name
     *      The name of the save to add
     */
    PersistenceView.removeSave = function removeSave(name)
    {
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.UI.PersistenceView.removeSave", "name");

        PersistenceView_loadContainer.removeChild(PersistenceView_items[name].container);
        delete PersistenceView_items[name];
    };

    /**
     *  @function show
     *      Shows the view.
     */
    PersistenceView.show = function show()
    {
        PersistenceView_container.classList.remove("hidden");
    };

    /**
     *  @function hide
     *      Hides the view.
     */
    PersistenceView.hide = function hide()
    {
        PersistenceView_container.classList.add("hidden");
    };

    /* Private */

    let PersistenceView_container = null;

    let PersistenceView_defaultMixButton = null;
    let PersistenceView_saveButton = null;
    let PersistenceView_loadButton = null;

    let PersistenceView_saveContainer = null;
    let PersistenceView_saveNameInput = null;
    let PersistenceView_saveConfirmButton = null;
    let PersistenceView_saveCancelButton = null;

    let PersistenceView_loadContainer = null;
    let PersistenceView_loadCloseButton = null;

    let PersistenceView_loadAskConfirmContainer = null;
    let PersistenceView_loadAskConfirmButton = null;
    let PersistenceView_loadAskConfirmCancelButton = null;

    let PersistenceView_state = "none";

    let PersistenceView_toRemoveItem = null;

    let PersistenceView_items = {};

    let PersistenceView_defaultMixButtonCallback = function defaultMixButtonCallback()
    {
        let name = MixEnergetik.UI.VideoView.getVideoName();

        MixEnergetik.PersistenceManager.loadDefaultMix(name);
    };

    let PersistenceView_showSave = function showSave()
    {
        PersistenceView_saveContainer.classList.remove("hidden");
        PersistenceView_loadContainer.classList.add("hidden");

        PersistenceView_state = "save";
    };

    let PersistenceView_hideSave = function hideSave()
    {
        PersistenceView_saveNameInput.value = "";

        PersistenceView_saveContainer.classList.add("hidden");

        PersistenceView_state = "off";
    };

    let PersistenceView_saveButtonCallback = function saveButtonCallback(event)
    {
        if (PersistenceView_state !== "save")
        {
            PersistenceView_showSave();
        }
        else
        {
            PersistenceView_hideSave();
        }
    };

    let PersistenceView_hideLoad = function hideLoad()
    {
        PersistenceView_loadContainer.classList.add("hidden");

        PersistenceView_state = "off";
    };

    let PersistenceView_loadButtonCallback = function loadButtonCallback(event)
    {
        if (PersistenceView_state !== "load")
        {
            PersistenceView_hideSave();
            PersistenceView_loadContainer.classList.remove("hidden");

            PersistenceView_state = "load";
        }
        else
        {
            PersistenceView_hideLoad();
        }
    };

    let PersistenceView_loadCloseButtonCallback = function loadCloseButtonCallback(event)
    {
        PersistenceView_hideLoad();
    };

    let PersistenceView_saveConfirmButtonCallback = function saveConfirmButtonCallback(event)
    {
        if (PersistenceView_saveNameInput.value === "")
        {
            return;
        }

        let saveName = PersistenceView_saveNameInput.value;

        PersistenceView_hideSave();

        MixEnergetik.PersistenceManager.save(saveName);
    };

    let PersistenceView_saveCancelButtonCallback = function saveCancelButtonCallback(event)
    {
        PersistenceView_hideSave();
    };

    let PersistenceView_loadAskConfirm = function loadAskConfirm(saveItem)
    {
        PersistenceView_toRemoveItem = saveItem;

        PersistenceView_loadAskConfirmContainer.classList.remove("hidden");
    };

    let PersistenceView_loadAskConfirmButtonCallback = function loadAskConfirmButtonCallback()
    {
        if (PersistenceView_toRemoveItem)
        {
            MixEnergetik.PersistenceManager.remove(PersistenceView_toRemoveItem.name);
            PersistenceView_toRemoveItem = null;
        }

        PersistenceView_loadAskConfirmContainer.classList.add("hidden");
    };

    let PersistenceView_loadAskConfirmCancelButtonCallback = function loadAskConfirmCancelButtonCallback()
    {
        PersistenceView_toRemoveItem = null;

        PersistenceView_loadAskConfirmContainer.classList.add("hidden");
    };

    return PersistenceView;
})();

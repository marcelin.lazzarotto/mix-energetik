/*
 *  File:   Preview/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-12-19
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module Preview
 *      Module for the preview UI element. A look into the future.
 */
MixEnergetik.UI.Preview = (function()
{
    let Preview = {};

    /**
     *  @function initialize
     *      Sets all buttons callback.
     */
    Preview.initialize = function initialize()
    {
        Preview_button = document.getElementById("PreviewButton");
        Preview_container = document.getElementById("PreviewContainer");
        Preview_video = document.getElementById("PreviewVideo");

        /* Bad ifelif to know where we are */
        if (MixEnergetik.UI.EnergeticSelectionView)
        {
            /* Base */
            Preview_video.src = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/previews/base.mp4";
        }
        else if (MixEnergetik.UI.GroupDataView)
        {
            /* Extraction */
            Preview_video.src = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/previews/extraction.mp4";
        }
        else /* if (MixEnergetik.UI.SampleListView) */
        {
            /* Wander */
            Preview_video.src = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/previews/wander.mp4";
        }

        Preview_button.addEventListener("click", Preview_toggle, false);

        Preview_container.classList.add("hidden");
    };

    /* Private */

    let Preview_button = null;
    let Preview_container = null;
    let Preview_video = null;

    /**
     *  @function show
     *      Show the credits.
     */
    let Preview_toggle = function toggle()
    {
        Preview_container.classList.toggle("hidden");
        Preview_video.currentTime = 0.0;
    };

    return Preview;
})();

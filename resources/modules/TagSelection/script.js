/*
 *  File:   TagSelectionView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module TagSelectionView
 *      The manager dealing with video filtering by tags.
 */
MixEnergetik.UI.TagSelectionView = (function()
{
    let TagSelectionView = {};

    /**
     *  @property {MixEnergetik.UI.TagItem} selected
     *      2018-07-02: Tags are now locked to only one selected, so keep track
     *      of it.
     */
    TagSelectionView.selected = null;

    /**
     *  @property {HTMLDivElement} container
     *      The div element containing tags.
     */
    TagSelectionView.container = null;

    /**
     *  @property {HTMLDivElement} relatedContainer
     *      The div element containing tags, related to the selected video.
     */
    TagSelectionView.relatedContainer = null;

    /**
     *  @property {MixEnergetik.UI.TagSelectionView_TagItem objectmap} tags
     *      Contains the list of TagItems inside the tag list.
     *      Is a map identified from their name.
     */
    TagSelectionView.tags = {};
    /**
     *  @property {MixEnergetik.UI.TagSelectionView_TagItem objectmap} relatedTags
     *      Contains the list of TagItems inside the related tag list.
     *      Is a map identified from their name.
     */
    TagSelectionView.relatedTags = {};

    /**
     *  @function addTag
     *      Adds a tag to the tag list.
     *      Do nothing if the tag is already there.
     *
     *  @param {string} tagName
     *      The name of the tag to add.
     */
    TagSelectionView.addTag = function addTag(tagName)
    {
        const TagSelV = MixEnergetik.UI.TagSelectionView;

        MixEnergetik.Utilitary.Check.native(tagName, "string",
            "MixEnergetik.UI.TagSelectionView.addTag()", "tagName");

        if ((TagSelV.tags[tagName] !== undefined)
            && (TagSelV.tags[tagName] !== null))
        {
            return;
        }

        TagSelV.tags[tagName] = new MixEnergetik.UI.TagItem(tagName, "TAG_CLICKABLE");
        TagSelV.container.appendChild(TagSelV.tags[tagName].container);
    };

    /**
     *  @function setRelatedTags
     *      Creates all the tags inside the related tags container.
     *
     *  @param {string[]} names
     *      The names of the tags to add.
     */
    TagSelectionView.setRelatedTags = function setRelatedTags(names)
    {
        const TagSelV = MixEnergetik.UI.TagSelectionView;

        MixEnergetik.Utilitary.Check.array(names,
            "MixEnergetik.UI.TagSelectionView.setRelatedTags()", "names");

        /* Clear first */
        TagSelV.relatedTags = {};
        while (TagSelV.relatedContainer.firstChild)
        {
            TagSelV.relatedContainer.removeChild(TagSelV.relatedContainer.firstChild);
        }

        for (let i = 0; i < names.length; i++)
        {
            const tagName = names[i];

            if ((TagSelV.relatedTags[tagName] !== undefined)
                && (TagSelV.relatedTags[tagName] !== null))
            {
                continue;
            }
            if ((TagSelV.tags[tagName] === undefined)
                && (TagSelV.tags[tagName] === null))
            {
                continue;
            }

            const tagItem = new MixEnergetik.UI.TagItem(tagName, "TAG_CLICKABLE");

            TagSelV.relatedTags[tagName] = tagItem;
            TagSelV.relatedContainer.appendChild(tagItem.container);

            if ((TagSelV.selected !== null)
                && (TagSelV.selected.name === tagName))
            {
                tagItem.container.classList.add("selected");
                tagItem.selected = true;
            }
        }
    };

    /**
     *  @property {boolean} hidden
     *      Is the windows hidden.
     */
    TagSelectionView.hidden = true;

    /**
     *  @function show
     *      Show the tag selection window.
     */
    TagSelectionView.show = function show()
    {
        const TagSelV = MixEnergetik.UI.TagSelectionView;

        TagSelV.container.classList.remove("hidden");
        TagSelV.hidden = false;
    };

    /**
     *  @function hide
     *      Closes the tag selection window.
     */
    TagSelectionView.hide = function hide()
    {
        const TagSelV = MixEnergetik.UI.TagSelectionView;

        TagSelV.container.classList.add("hidden");
        TagSelV.hidden = true;
    };

    TagSelectionView.initialize = function initialize()
    {
        const TagSelV = MixEnergetik.UI.TagSelectionView;

        TagSelV.container = document.getElementById(
            TagSelectionView_CONTAINER_ID);
        TagSelV.relatedContainer = document.getElementById(
            TagSelectionView_RELATED_CONTAINER_ID);
        TagSelectionView_closeButton = document.getElementById(
            TagSelectionView_BUTTON_CLOSE_ID);

        TagSelectionView_closeButton.addEventListener("click", function()
        {
            TagSelV.hide();
        }, false);

        TagSelV.hide();
    };

    /**
     *  @function showRelated
     *      Show the related tags.
     */
    TagSelectionView.showRelated = function showRelated()
    {
        MixEnergetik.UI.TagSelectionView.relatedContainer.classList.add("hidden");
    };

    /**
     *  @function hide
     *      Hide the related tags.
     */
    TagSelectionView.hideRelated = function hideRelated()
    {
        MixEnergetik.UI.TagSelectionView.relatedContainer.classList.add("hidden");
    };

    /**
     *  @property {string constant} CONTAINER_ID
     *      The id of the big container present on the webpage.
     */
    const TagSelectionView_CONTAINER_ID = "TagSelection";
    /**
     *  @property {string constant} BUTTON_CLOSE_ID
     *      The id of the big container present on the webpage.
     */
    const TagSelectionView_BUTTON_CLOSE_ID = "TagSelectionCloseButton";
    /**
     *  @property {string constant} RELATED_CONTAINER
     *      The id of the big container present on the webpage.
     */
    const TagSelectionView_RELATED_CONTAINER_ID = "TagSelectionRelated";

    /**
     *  @property {HTMLDivElement} closeButton
     *      The button closing the view.
     */
    let TagSelectionView_closeButton = null;

    return TagSelectionView;
})();
